## RODO

Na podstawie przepisów Rozporządzenia Parlamentu Europejskiego i Rady (UE) 2016/679 z 27 kwietnia 2016 r. w sprawie ochrony osób fizycznych w związku z przetwarzaniem danych osobowych i w sprawie swobodnego przepływu takich danych oraz uchylenia dyrektywy 95/46/WE, (ogólne rozporządzenie o ochronie danych) zwanego dalej RODO informujemy, że:
1. Administratorem Pani/Pana adresu e-mail jest Szkoła Główna Gospodarstwa Wiejskiego w Warszawie, ul. Nowoursynowska 166, 02-787 Warszawa.
2. Pani/Pana adresy mailowe będą przetwarzane w celu konsultowania zgłoszonego działania z zakresu małej retencji.
3. Odbiorcami Pani/Pana adresu e-mail mogą być podmioty lub organy, którym Administrator jest zobowiązany udostępniać dane na podstawie powszechnie obowiązujących przepisów prawa.
4. Przysługują Pani/Panu prawo do żądania usunięcia adresu e-mial i wprowadzonych danych.
5. Przysługuje Pani/Panu wniesienie skargi do organu nadzorczego – Prezesa Urzędu Ochrony Danych Osobowych.
6. Podanie przez Panią/Pana swoich danych osobowych jest dobrowolne.
 
