## Terms of use the portal Decision Support System for N(S)WRM Planning

Terms and Conditions of planuj.retencjawod.sggw.pl and panning.waterretention.sggw.pl
Using the resources of the planuj.retencjawod.sggw.pl and panning.waterretention.sggw.pl website signifies the acceptance of the following Terms and Conditions without limitations or restrictions.

### DEFINITIONS

For the purposes of these Terms and Conditions, the following definitions shall apply:

Terms and Conditions - the following rules, terms and conditions for the use of ww.planuj.retencjawod.sggw.pl and panning.waterretention.sggw.pl;

The Decision Support System hereinafter referred to as "DSS"- created and maintained by the Department of Hydrology, Meteorology and Water Management, Warsaw University of Life Science available at www.planuj.retencjawod.sggw.pl and panning.waterretention.sggw.pl

Administration body - a government administration body, an agency of local government or another entity if it is established by the virtue of law or authorized by agreement to perform public tasks relating to the environment;

Provider - Department of Hydrology, Meteorology and Water Management, Warsaw University of Life Science, who creates and maintains the Spatial information infrastructure DSS;

User - a person, legal person or an organizational unit without a legal personality, which uses the resources of the DSS;


### I. GENERAL INFORMATION

These Terms and Conditions determine the rules, terms and conditions for the use of www.planuj.retencjawod.sggw.pl and panning.waterretention.sggw.pl resources by Users.
The User is required to acquaint oneself with the contents of the Terms and Conditions, before using DSS. The confirmation of reading the Terms and Conditions constitutes the User's understanding and acceptance.
The User and other Administration bodies are obliged to use DSS in a manner consistent with the applicable law, social and moral standards and the provisions of these Terms and Conditions.

### II. TYPES AND SCOPE OF SERVICES

The spatial data services available through the DSS include:

discovery services - making it possible to search for spatial data sets and services on the basis of the content of the corresponding metadata and to display the content of the metadata;
view services - making it possible, as a minimum, to display, navigate, zoom in/out, pan, or overlay viewable spatial data sets and to display legend information and any relevant content of metadata.
download services - retrievable copies of files or their parts and, where practicable, direct access to these collections.
Access to services search and browsing is universal and free.

### III. CONDITIONS / RULES FOR THE USE OF DATA AND SERVICES BY THE USERS    2

The User has the right to use DSS resources only for non-profit purposes and noncommercial use such as education, scientific research and development.
In the case of use of the DSS data User must post or inform listeners about the source by. citation, available in chapter VII of the rules.
The User has no right to reproduce, sell, share or otherwise market or distribute DSS content, in whole or in part, in particular, transmit or make available in computer systems and networks, or any other data communications systems.

### IV. THE TECHNICAL CONDITIONS OF USING DATA AND SERVICES    2

For proper use of DSS resources, the following is required:
* an Internet connection;
* a device allowing access to the Internet, including a program to browse its resources (web browser), which accepts cookie files and Java Script;

When using the DSS resources the following is prohibited:
* the use of viruses, bots, worms or other computer codes, scripts or programs that interrupt, destroy or limit the operation of the DSS or its technical infrastructure, or otherwise allow the unauthorized use or access to the technical infrastructure of the system,
* use of computer codes, scripts or programs to automate the use of the services provided by the DSS.

### V. ADDITIONAL INFORMATION

For further clarification or information relating to the content of these Terms and Conditions, please contact i.kardel@levis.sggw.pl.

### VI. FINAL PROVISIONS

In matters not governed by these Terms and Regulations, regulations shall be governed by applicable laws such as the Act of February 4, 1994 on Copyright and Related Rights (Journal of Laws OJ 2006 No. 90, item 631, as amended) and the Civil Code.
The Provider is not responsible for the lack of access to the DSS for reasons beyond the Provider.
The Provider, for security reasons and for any other reasons beyond the Provider, has the right to temporarily suspend access to the DSS, for the time needed to remove these circumstances.
Subject to limitations imposed by mandatory provisions of law, the Provider is not responsible for any damages arising from the DSS, its use or in connection with the wrongful act, errors, defects, distortions, defects, delays in data transmission, computer virus, line or system failure information or non observance of the Terms and Conditions by the Users.
These Regulations may be changed at any time and in any aspect. The changes take effect following its publication on the DSS website.
DSS, as an Internet site, may contain links to websites operated by third parties.
Links to other Internet resources are used at the User's own risk. The Provider assumes no responsibility for the accuracy, timeliness and completeness of data contained on the pages which may be accessed through the DSS.

This application has been developed through a collaborative framework ( involving the project partners and other stakeholders including the European Union European Regional Development Fund). The application is a working draft and does not necessarily represent the official, formal position of any of the partners. Inputs and outputs might not necessarily reflect the views of the project partners and stakeholders. Neither the European Union nor any other partners are responsible for the use that any third party might make of the information contained in this document. The technical documentation is not legally binding. Any authoritative reading of the law should only be derived from Directive 2000/60/EC itself and other applicable legal texts or principles. Only the Court of Justice of the European Union is competent to authoritatively interpret Union legislation.

### VII. CITATION

Please reference the use of this application in any publications as:
FramWat, 2019, Decision Support System for N(S)WRM Planning carried out within the Framework for improving water balance and nutrient mitigation by applying small water retention measures (FramWat) Interreg Central Europe Project 2014-2020
