## GDPR

According to Regulation on the protection of natural persons with regard to the processing of personal data and on the free movement of such data, and repealing Directive 95/46/EC (Data Protection Directive) with the implementation date of 25th of May 2018, known as GDPR we inform that:
1. The administrator of your e-mail address is Warsaw University of Life Sciences (SGGW) in Warsaw, ul. Nowoursynowska 166, 02-787 Warsaw.
2. Your email addresses will be processed in order to consult the submitted small water retention measure.
3. The recipients of your email address may be entities or bodies to which the Administrator is obliged to disclose data on the basis of generally applicable law.
4. You have the right to request the deletion of your e-mail address and entered data.
5. You are entitled to lodge a complaint to the supervisory body - the President of the Office for Personal Data Protection.
6. Providing your personal data by you is voluntary.
