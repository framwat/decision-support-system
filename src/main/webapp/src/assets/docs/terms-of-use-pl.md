## Warunki korzystania z portalu System Wspomagania Decyzji w zakresie planowania małej retencji

Korzystanie z zasobów strony internetowej planuj.retencjawod.sggw.pl i planning.waterretention.sggw.pl oznacza akceptację niniejszego regulaminu bez ograniczeń i restrykcji.

### DEFINICJE
Na potrzeby niniejszego Regulaminu wskazane poniżej pojęcia będą miały następujące znaczenie:

Regulamin – niniejsze zasady, zakres i warunki korzystania z zasobów witryny internetowej planuj.retencjawod.sggw.pl i planning.waterretention.sggw.pl;

System Wspierania Decyzji, zwany dalej „DSS"– utworzony i utrzymywany przez Katedrę Hydrologii, Meteorologii i Gospodarki Wodnej, Szkoła Główna Gospodarstwa Wiejskiego w Warszawie, dostępny pod adresem internetowym planuj.retencjawod.sggw.pl;

Usługodawca – Katedra Hydrologii, Meteorologii i Gospodarki Wodnej, Szkoła Główna Gospodarstwa Wiejskiego w Warszawie, który tworzy i zarządza infrastrukturą informacji przestrzennej DSS;

Usługobiorca – osoba fizyczna, osoba prawna, organ administracji albo jednostka organizacyjna nieposiadająca osobowości prawnej, która korzysta z zasobów DSS.

### I. INFORMACJE OGÓLNE
Niniejszy Regulamin określa zasady, zakres i warunki korzystania przez Usługobiorców z zasobów witryny internetowej www.planuj.retencjawod.sggw.pl i planning.waterretention.sggw.pl.
Usługobiorca jest zobowiązany do zapoznania się z treścią Regulaminu przed rozpoczęciem korzystania z DSS. Potwierdzenie zapoznania się z treścią Regulaminu jest równoznaczne z oświadczeniem Usługobiorcy, że go rozumie i akceptuje.
Usługobiorca zobowiązany jest do korzystania z DSS w sposób zgodny z obowiązującym prawem, normami społecznymi i obyczajowymi oraz postanowieniami niniejszego Regulaminu.

### II. RODZAJE I ZAKRES ŚWIADCZONYCH USŁUG
Usługi danych przestrzennych udostępnione w DSS polegają na:
usłudze wyszukiwania – umożliwiającej wyszukiwanie zbiorów oraz usług danych przestrzennych na podstawie zawartości odpowiadających im metadanych oraz  wyświetlanie zawartości metadanych;
usłudze przeglądania – umożliwiającej wyświetlanie, nawigowanie, powiększanie i pomniejszanie, przesuwanie lub nakładanie na siebie zobrazowanych zbiorów oraz wyświetlanie objaśnień symboli kartograficznych i zawartości metadanych;
usługa pobierania – umożliwiająca pobieranie kopii zbiorów lub ich części oraz, gdy jest to wykonalne, bezpośredni dostęp do tych zbiorów.
Dostęp do usługi wyszukiwania i przeglądania jest powszechny i nieodpłatny.

### III. WARUNKI/ZASADY KORZYSTANIA Z DANYCH I USŁUG PRZEZ USŁUGOBIORCÓW
Udostępniane zbiory danych przestrzennych prezentowane w DSS można wykorzystać do celów niekomercyjnych takich jak edukacja, prowadzenie badań naukowych oraz prac rozwojowych.
W przypadku wykorzystania danych Usługobiorca powinien zamieścić lub poinformować słuchaczy o źródle wg. cytowania, dostępnego w rozdziale VII regulaminu
Usługobiorca nie ma prawa zwielokrotniać, sprzedawać, udostępniać lub w inny sposób wprowadzać do obrotu lub rozpowszechniać treści DSS, w całości bądź we fragmentach, w szczególności przesyłać lub udostępniać go w systemach i sieciach komputerowych lub jakichkolwiek innych systemach teleinformatycznych.

### IV. WARUNKI TECHNICZNE KORZYSTANIA Z DANYCH I USŁUG
W celu prawidłowego korzystania z zasobów DSS wymagane jest:
* połączenie z siecią Internet;
* urządzenie pozwalające na dostęp do sieci Internet, włącznie z programem służącym do przeglądania jej zasobów  (przeglądarka internetowa), akceptującym pliki typu cookies oraz Java Script lub urządzenie mobilne wykorzystujące jeden z następujących systemów operacyjnych: IOS, Android, Windows Phone

Przy korzystaniu z zasobów DSS zabronione jest:
* wykorzystywanie wirusów, botów, robaków bądź innych kodów komputerowych, skryptów lub programów, które  przerywają, niszczą lub ograniczają działanie DSS lub infrastruktury technicznej albo w inny sposób umożliwiają nieuprawnione korzystanie lub dostęp do infrastruktury technicznej systemu.
* zastosowanie kodów komputerowych, skryptów lub programów do automatyzacji korzystania z usług świadczonych przez DSS.

### V. INFORMACJA DODATKOWA
W celu uzyskania dodatkowych wyjaśnień lub informacji związanych z treścią Regulaminu należy skontaktować się drogą elektroniczną na adres email: i.kardel@levis.sggw.pl.

### VI. POSTANOWIENIA KOŃCOWE
W sprawach opisanych niniejszym Regulaminem, co do zasady, stosuje się przepisy obowiązującego prawa, w tym ustawy o prawie autorskim i prawach pokrewnych oraz Kodeksu cywilnego.
Usługodawca nie ponosi odpowiedzialności za brak dostępu do DSS z przyczyn niezależnych od Usługodawcy. Usługodawca ze względów bezpieczeństwa oraz z powodu jakichkolwiek innych przyczyn niezależnych od Usługodawcy ma prawo czasowo zawiesić dostęp do DSS na okres niezbędny do usunięcia zaistniałych okoliczności.
Z zastrzeżeniem ograniczeń wynikających z obowiązujących przepisów prawa, Usługodawca nie odpowiada za szkody powstałe w związku z DSS, jego użytkowaniem bądź w związku z niewłaściwym działaniem, błędami, brakami, zakłóceniami, defektami, opóźnieniami w transmisji danych, wirusami komputerowymi, awarią linii lub systemu informatycznego czy też nie przestrzeganiem Regulaminu przez Usługobiorców.
Niniejszy Regulamin może być w każdym czasie i w dowolnym zakresie zmieniany. Zmiany obowiązują od momentu opublikowania ich na stronie internetowej DSS.
DSS, jako witryna internetowa może zawierać odnośniki do witryn zarządzanych przez strony trzecie.
Z odnośników do innych zasobów internetowych Usługobiorca korzysta na własne ryzyko. Usługodawca nie ponosi żadnej odpowiedzialności za rzetelność, aktualność oraz kompletność danych zawartych na stronach, do których można uzyskać dostęp poprzez niniejszy DSS.

Niniejsza aplikacja została opracowana w ramach współpracy (z udziałem partnerów projektu i innych zainteresowanych stron, w tym Europejskiego Funduszu Rozwoju Regionalnego Unii Europejskiej). Aplikacja jest projektem roboczym i niekoniecznie reprezentuje oficjalne, formalne stanowisko któregokolwiek z partnerów. Wkłady i wyniki niekoniecznie odzwierciedlają poglądy partnerów projektu i interesariuszy. Ani Unia Europejska, ani inni partnerzy nie ponoszą odpowiedzialności za wykorzystanie przez osoby trzecie informacji zawartych w tym dokumencie. Dokumentacja techniczna nie jest prawnie wiążąca. Wszelkie wiarygodne rozumienie prawa powinno wynikać wyłącznie z samej dyrektywy 2000/60 / WE i innych obowiązujących tekstów lub zasad prawnych. Jedynie Trybunał Sprawiedliwości Unii Europejskiej jest uprawniony do autorytatywnej interpretacji prawodawstwa Unii.

### VII. CYTOWANIE
Korzystając z tej aplikacji odwołaj się do niej w jakichkolwiek publikacjach jako:
FramWat, 2019, Decision Support System for N(S)WRM Planning carried out within the Framework for improving water balance and nutrient mitigation by applying small water retention measures (FramWat) Interreg Central Europe Project 2014-2020
