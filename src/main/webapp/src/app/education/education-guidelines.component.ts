import {Component, OnDestroy, OnInit, ViewEncapsulation} from "@angular/core";

@Component({
  templateUrl: './education-guidelines.component.html',
  styleUrls: ['./education-guidelines.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class EducationGuidelinesComponent implements OnInit, OnDestroy {

    casestudies:any = {
        "CS1": "http://malaretencja.pl/",
        "CS2": "https://envimap.pl/wp-content/uploads/2017/10/Planowanie_ma%C5%82ej_retencji_w_lasach_na_przyk%C5%82adzie_Puszczy_Noteckiej.pdf",
        "CS3": "https://www.gwp.org/globalassets/global/gwp-cee_files/idmp-cee/idmp-case-studies-final-pdf-small.pdf ",
        "CS4": "https://www.gwp.org/globalassets/global/gwp-cee_files/idmp-cee/idmp-nswrm-final-pdf-small.pdf",
        "CS5": "https://www.academia.edu/24766876/Metodyczne_i_praktyczne_aspekty_planowania_ma%C5%82ej_retencji",
        "CS6": "https://ec.europa.eu/environment/water/blueprint/pdf/EUR25552EN_JRC_Blueprint_Optimisation_Study.pdf",
        "CS7": "https://www.researchgate.net/profile/Peter_Burek/publication/263806285_Evaluation_of_the_effectiveness_of_Natural_Water_Retention_Measures_-_Support_to_the_EU_Blueprint_to_Safeguard_Europe's_Waters/links/0a85e53be936c3a66b000000/Evaluation-of-the-effectiveness-of-Natural-Water-Retention-Measures-Support-to-the-EU-Blueprint-to-Safeguard-Europes-Waters.pdf",
    };

    casetudiesKeys = Object.keys(this.casestudies);

  constructor(
  ){}

  ngOnInit() {
  }

  ngOnDestroy() {
  }
}
