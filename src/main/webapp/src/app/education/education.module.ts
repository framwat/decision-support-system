import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from "../shared/shared.module";
import { EducationComponent } from "./education.component";
import {MatIconModule} from "@angular/material/icon";
import {MatGridListModule} from "@angular/material/grid-list";
import {EducationDatabasesComponent} from "./education-databases.component";
import {EducationGuidelinesComponent} from "./education-guidelines.component";
import {EducationMoviesComponent} from "./education-movies.component";
import {EducationProgrammesComponent} from "./education-programmes.component";
import {EducationPublicationsComponent} from "./education-publications.component";
import {AppRoutingModule} from "../app-routing.module";
import {TranslateModule} from "@ngx-translate/core";
import {NgxYoutubePlayerModule} from "ngx-youtube-player";
import {MatCardModule} from "@angular/material/card";

@NgModule({
    imports: [
        AppRoutingModule,
        CommonModule,
        MatCardModule,
        MatIconModule,
        MatGridListModule,
        SharedModule,
        TranslateModule,
        NgxYoutubePlayerModule.forRoot()
    ],
    declarations: [
        EducationComponent,
        EducationDatabasesComponent,
        EducationGuidelinesComponent,
        EducationMoviesComponent,
        EducationProgrammesComponent,
        EducationPublicationsComponent,
    ],
    entryComponents: [
        EducationComponent,
        EducationDatabasesComponent,
        EducationGuidelinesComponent,
        EducationMoviesComponent,
        EducationProgrammesComponent,
        EducationPublicationsComponent,
    ],
    providers: [

    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class EducationModule {}
