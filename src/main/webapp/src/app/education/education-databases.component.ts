import {Component, OnDestroy, OnInit, ViewEncapsulation} from "@angular/core";

@Component({
  templateUrl: './education-databases.component.html',
  styleUrls: ['./education-databases.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class EducationDatabasesComponent implements OnInit, OnDestroy {

  constructor(
  ){}

  ngOnInit() {
  }

  ngOnDestroy() {
  }
}
