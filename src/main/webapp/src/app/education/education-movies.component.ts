import {Component, OnDestroy, OnInit, ViewEncapsulation} from "@angular/core";
import {Movie} from "./education.model";

@Component({
  templateUrl: './education-movies.component.html',
  styleUrls: ['./education-movies.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class EducationMoviesComponent implements OnInit, OnDestroy {

  movies: Movie[];

  constructor(
  ){
    this.movies = [
      new Movie("MOV1", "J6t7tubdnr4"),
      new Movie("MOV2", "j5hg-0MOhK0"),
      new Movie("MOV3", "6JGckMKYeE0"),
      new Movie("MOV4", "XbtRzqQRXU4"),
      new Movie("MOV5", "H4dl2GVkocc"),
      new Movie("MOV6", "KJEf4GQ7sqE"),
      new Movie("MOV7", "22JlYQkOY24"),
      new Movie("MOV8", "mu5dib32tew")
    ];
  }

  ngOnInit() {

  }

  ngOnDestroy() {
  }
}
