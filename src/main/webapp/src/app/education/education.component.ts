import {Component, OnDestroy, OnInit, ViewEncapsulation} from "@angular/core";

@Component({
    templateUrl: './education.component.html',
    styleUrls: ['./education.component.css'],
    encapsulation: ViewEncapsulation.None
})
export class EducationComponent implements OnInit, OnDestroy {

    constructor(
    ){}

    ngOnInit() {
    }

    ngOnDestroy() {
    }
}


