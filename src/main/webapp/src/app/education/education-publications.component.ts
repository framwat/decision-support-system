import {Component, OnDestroy, OnInit, ViewEncapsulation} from "@angular/core";

@Component({
  templateUrl: './education-publications.component.html',
  styleUrls: ['./education-publications.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class EducationPublicationsComponent implements OnInit, OnDestroy {
  publications:any = {
    "PUB1": "https://www.degruyter.com/downloadpdf/j/jwld.2008.12.issue--1/v10025-009-0012-y/v10025-009-0012-y.pdf",
    "PUB2": "https://www.degruyter.com/downloadpdf/j/jwld.2014.20.issue-1/jwld-2014-0005/jwld-2014-0005.pdf",
    "PUB3": "https://www.researchgate.net/profile/Radoslaw_Juszczak/publication/233990795_Assessment_of_water_retention_capacity_of_small_ponds_in_Wyskoc_agricultural-forest_catchment_in_western_Poland/links/0fcfd50dd6f3670aa7000000.pdf",
    "PUB4": "https://www.degruyter.com/downloadpdf/j/jwld.2014.23.issue-1/jwld-2014-0028/jwld-2014-0028.pdf",
    "PUB5": "https://onlinelibrary.wiley.com/doi/full/10.1111/jfr3.12269",
    "PUB6": "https://link.springer.com/article/10.1007/s10113-014-0643-7",
    "PUB7": "https://www.mdpi.com/2073-4441/8/9/371",
    "PUB8": "https://www.researchgate.net/publication/229512742_Multi-scale_modelling_of_land-use_change_and_river_training_effects_on_floods_in_the_Rhine_basin",
    "PUB9": "https://link.springer.com/article/10.1007/s13157-013-0473-2",
      "PUB10": "http://www.sciencedirect.com/science/article/pii/S1470160X16302278",
      "PUB11": "https://docs.google.com/spreadsheets/d/1N9SoYgdJ_lxxkkqLava5xkyI3FQo64WV3M6jQ-QeOtA/edit?usp=sharing",
  };

  publicationsKeys = Object.keys(this.publications);

  constructor(
  ){}

  ngOnInit() {
  }

  ngOnDestroy() {
  }
}
