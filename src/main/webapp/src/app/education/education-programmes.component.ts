import {Component, OnDestroy, OnInit, ViewEncapsulation} from "@angular/core";

@Component({
  templateUrl: './education-programmes.component.html',
  styleUrls: ['./education-programmes.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class EducationProgrammesComponent implements OnInit, OnDestroy {
  programmes:any = {
    "PROG1": "http://www.zielonaakcja.pl/index.php/projekty/w-realizacji/97-projekty/w-realizacji/587-wspoldzialanie-srodowisk-na-rzecz-adaptacyjnosci-do-zmian-klimatycznych-poprzez-mala-retencje-oraz-ochrone-bioroznorodnosci",
    "PROG2": "http://www.zielonaakcja.pl/index.php/oddolne-inicjatywy-dla-zachowania-bioroznorodnosci-poprzez-mala-retencje-i-zadrzewienia",
    "PROG3": "http://www.zielonaakcja.pl/index.php/mala-retencja-duza-sprawa-kampania"
  }

  programmesKeys = Object.keys(this.programmes);

  constructor(
  ){}

  ngOnInit() {
  }

  ngOnDestroy() {
  }
}
