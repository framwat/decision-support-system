import { Component, OnDestroy, OnInit, ViewEncapsulation } from "@angular/core";

@Component({
  templateUrl: './tools.component.html',
  styleUrls: ['./tools.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ToolsComponent implements OnInit, OnDestroy {

    constructor(
    ) {

    }

    ngOnInit() {
    }

    ngOnDestroy() {
    }
}
