import {Component, OnDestroy, OnInit, ViewEncapsulation} from "@angular/core";

@Component({
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.css'],
    encapsulation: ViewEncapsulation.None
})
export class HomeComponent implements OnInit, OnDestroy {

  constructor(
  ){}

  ngOnInit() {
  }

  ngOnDestroy() {
  }
}


