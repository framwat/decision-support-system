import {Component, OnDestroy, OnInit, ViewEncapsulation} from "@angular/core";
import {AdministrationService} from "./administration.service";
import {MatTableDataSource} from "@angular/material/table";
import {SelectionModel} from "@angular/cdk/collections";
import {PlannerMapView, PlannerUser} from "../planner/planner.model";

@Component({
  templateUrl: './administration.component.html',
  styleUrls: ['./administration.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class AdministrationComponent implements OnInit, OnDestroy {

  users: PlannerUser[];
  maps: PlannerMapView[];
  usersDataSource: MatTableDataSource<PlannerUser>;
  mapsDataSource: MatTableDataSource<PlannerMapView>;
  selection = new SelectionModel<PlannerMapView>(true, []);

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.usersDataSource.data.length;
    return numSelected === numRows;
  }

  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.mapsDataSource.data.forEach(row => this.selection.select(row));
  }

  checkboxLabel(row?: PlannerMapView): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'}`;
  }

  constructor(
    private administrationService: AdministrationService
  ){}

  ngOnInit() {
    this.administrationService.getAllUsers().then(
      usersList => {
        this.users = usersList;
        this.usersDataSource = new MatTableDataSource<PlannerUser>(usersList)

        this.administrationService.getAllMaps().then(
          maps => {
            for (let i in maps) {
              let user = this.getUserById(maps[i].uid);

              if (user == null) {
                maps[i].author = "";
              } else {
                maps[i].author = user.name;
              }
            }

            this.maps = maps;
            this.mapsDataSource = new MatTableDataSource<PlannerMapView>(maps);
          }
        )
      }
    );
  }

  ngOnDestroy() {
  }

  downloadMap(id: string): void {
    console.log(id);
  }

  downloadSelectedMaps(): void {

  }

  private getUserById(id: string): PlannerUser {
    for (let i in this.users) {
      if (this.users[i].id == id) {
        return this.users[i];
      }
    }

    return null;
  }
}


