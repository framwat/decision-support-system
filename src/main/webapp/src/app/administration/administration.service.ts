import {Injectable} from "@angular/core";
import {AngularFireDatabase, AngularFireList} from "@angular/fire/database";
import {PlannerMapView, PlannerUser} from "../planner/planner.model";

@Injectable()
export class AdministrationService {
  private mapsBasePath: string = '/maps';
  private usersBasePath: string = '/users';

  private users: PlannerUser[];
  private maps: PlannerMapView[];

  constructor(private db: AngularFireDatabase) {}

  getAllUsers(): Promise<PlannerUser[]> {
    let userList:PlannerUser[] = [];

    return new Promise<PlannerUser[]>((resolve, reject) => {
      this.db.list(this.usersBasePath).query.once("value").then(
        userListSnapshot => {
          let userListObject = userListSnapshot.val();

          for (let i in userListObject) {
            userList.push(userListObject[i]);
          }

          resolve(userList);
        },
        err => reject(err)
      );
    });
  }

  getAllMaps(): Promise<PlannerMapView[]> {
    let maps:PlannerMapView[] = [];

    return new Promise<PlannerMapView[]>((resolve, reject) => {
      this.db.list(this.mapsBasePath).query.once("value").then(
        mapsSnapshot => {
          let mapsObject = mapsSnapshot.val();

          for (let i in mapsObject) {
            for (let j in mapsObject[i]) {
              let map = mapsObject[i][j];
              map.uid = i;
              maps.push(map);
            }
          }

          resolve(maps);
        },
        err => reject(err)
      );
    });
  }
}
