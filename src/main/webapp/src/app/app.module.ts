import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MeasuresCatalogModule } from "./measures-catalog/measures-catalog.module";
import { HttpClient, HttpClientModule } from "@angular/common/http";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { EducationModule } from "./education/education.module";
import { ToolsModule } from "./tools/tools.module";
import { HomeModule } from "./home/home.module";
import { PlannerModule } from "./planner/planner.module";
import { TranslateLoader, TranslateModule } from "@ngx-translate/core";
import { TranslateHttpLoader } from "@ngx-translate/http-loader";
import { SharedModule } from "./shared/shared.module";
import { AngularFireModule } from "@angular/fire";
import { environment } from '../environments/environment';
import { AngularFireAuthModule } from "@angular/fire/auth";
import { AdministrationModule } from "./administration/administration.module";
import {MarkdownModule} from "ngx-markdown";
import {AboutModule} from "./about/about.module";
import {MatButtonModule} from "@angular/material/button";
import {MatToolbarModule} from "@angular/material/toolbar";
import {MatMenuModule} from "@angular/material/menu";
import {LegalAssistantModule} from "./legal-assistant/legal-assistant.module";

export function HttpLoaderFactory(httpClient: HttpClient) {
  return new TranslateHttpLoader(httpClient);
}

@NgModule({
    declarations: [
        AppComponent
    ],
    exports: [

    ],
    imports: [
        AboutModule,
        AdministrationModule,
        AngularFireAuthModule,
        AngularFireModule.initializeApp(environment.firebase),
        AppRoutingModule,
        BrowserModule,
        BrowserAnimationsModule,
        EducationModule,
        HomeModule,
        HttpClientModule,
        LegalAssistantModule,
        MarkdownModule.forRoot(),
        MatToolbarModule,
        MatButtonModule,
        MatMenuModule,
        MeasuresCatalogModule,
        PlannerModule,
        SharedModule,
        ToolsModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: HttpLoaderFactory,
                deps: [HttpClient]
            }
        })
    ],
    providers: [],
    bootstrap: [ AppComponent ],
    schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
})
export class AppModule { }
