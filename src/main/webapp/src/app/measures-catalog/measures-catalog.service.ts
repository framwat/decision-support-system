import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpParams, HttpResponse} from '@angular/common/http';
import { Observable, throwError } from "rxjs";

import { catchError, map } from 'rxjs/operators';
import { createRequestOption } from "../shared/request";
import { environment } from "../../environments/environment";


@Injectable()
export class MeasuresCatalogService {
    constructor(private http: HttpClient) {}
}
