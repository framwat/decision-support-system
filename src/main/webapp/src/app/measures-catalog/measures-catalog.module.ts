import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from "@angular/common";

import { MeasuresCatalogComponent } from './measures-catalog.component';
import { MeasuresCatalogService } from "./measures-catalog.service";
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { SharedModule } from "../shared/shared.module";
import {TranslateModule} from "@ngx-translate/core";
import {MatButtonModule} from "@angular/material/button";
import {MatCheckboxModule} from "@angular/material/checkbox";
import {MatDividerModule} from "@angular/material/divider";
import {MatExpansionModule} from "@angular/material/expansion";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import {MatSelectModule} from "@angular/material/select";
import {MatTabsModule} from "@angular/material/tabs";
import {MatTableModule} from "@angular/material/table";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatCheckboxModule,
    MatDividerModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatTabsModule,
    MatTableModule,
    SharedModule,
    TranslateModule
  ],
    declarations: [ MeasuresCatalogComponent ],
    entryComponents: [ MeasuresCatalogComponent ],
    providers: [ MeasuresCatalogService ],
    schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class MeasuresCatalogModule {}
