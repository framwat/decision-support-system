import {Component, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {Goal, LandScape, LandUse, Measure, Permeability, Scale, Sector, measures} from "./measures-catalog.model";


@Component({
    templateUrl: './measures-catalog.component.html',
    styleUrls: ['./measures-catalog.component.css'],
    encapsulation: ViewEncapsulation.None
})
export class MeasuresCatalogComponent implements OnInit, OnDestroy {
  measures: Measure[];
  filteredSectors: string[] = [];
  filteredMeasureBySector: any = {};
  goals: string[];
  scales: string[];
  landUses: string[]
  sectors: string[];
  permeabilites: string[];
  landscapes: string[];
  selected: any = {
    sector: {},
    goal: {},
    scale: {},
    landUse: {},
    permeability: {},
    landscape: {},
    nearWater: {}
  };

  ngOnInit() {

      this.sectors = Object.values(Sector);

      let sector:string;

      for (sector of Object.values(Sector)) {
          this.selected.sector[sector] = false;
      }

      this.goals = Object.values(Goal);

      let goal: string;

      for (goal of Object.values(Goal)) {
          this.selected.goal[goal] = false;
      }

      this.scales = Object.values(Scale);

      let scale: string;

      for (scale of Object.values(Scale)) {
        this.selected.scale[scale] = false;
      }

      this.landUses = Object.values(LandUse).filter(landuse => landuse != LandUse.ANY_TYPE);

      let landUse: string;

      for (landUse of Object.values(LandUse)) {
        this.selected.landUse[landUse] = false;
      }

      this.permeabilites = Object.values(Permeability).filter(permeability => permeability != Permeability.ANY_TYPE);

      let permeability: string;

      for (permeability of Object.values(Permeability)) {
        this.selected.permeability[permeability] = false;
      }

      this.landscapes = Object.values(LandScape).filter(landscape => landscape != LandScape.ANY_TYPE);

      let landscape: string;

      for (landscape of Object.values(LandScape)) {
        this.selected.landscape[landscape] = false;
      }

      this.measures = measures;
  }

  doFilter(type, value, isChecked) {
    this.selected[type][value] = isChecked;

    let selectedSectors = this.findSelections("sector", Sector);
    let selectedGoals = this.findSelections("goal", Goal);
    let selectedScales = this.findSelections("scale", Scale);
    let selectedLandUses = this.findSelections("landUse", LandUse);
    let selectedPermeability = this.findSelections("permeability", Permeability);
    let selectedLandscapes = this.findSelections("landscape", LandScape);

    let filteredMeasureBySector = [];
    let filteredSectors = [];

    let filteredMeasures = this.measures;

    if (selectedSectors.length > 0) {
      filteredMeasures = filteredMeasures.filter(
        measure => selectedSectors.includes(measure.sector)
      )
    }

    if (selectedGoals.length > 0) {
      filteredMeasures = filteredMeasures.filter(
        measure => this.hasAny(measure.goals, selectedGoals)
      );
    }

    if (selectedScales.length > 0) {
      filteredMeasures = filteredMeasures.filter(
        measure => selectedScales.includes(measure.scale)
      );
    }

    if (selectedLandUses.length > 0) {
      filteredMeasures = filteredMeasures.filter(
        measure => this.hasAny(measure.landUses, selectedLandUses)
      );
    }

    if (selectedPermeability.length > 0) {
      filteredMeasures = filteredMeasures.filter(
        measure => measure.permeability == Permeability.ANY_TYPE || selectedPermeability.includes(measure.permeability)
      );
    }

    if (selectedLandscapes.length > 0) {
      filteredMeasures = filteredMeasures.filter(
        measure => measure.landscape == LandScape.ANY_TYPE || selectedLandscapes.includes(measure.landscape)
      );
    }

    if (this.selected['nearWater']['yes'] || this.selected['nearWater']['no']) {
      filteredMeasures = filteredMeasures.filter(
        measure => (measure.nearWater && this.selected['nearWater']['yes']) || (!measure.nearWater && this.selected['nearWater']['no'])
      )
    }

    filteredMeasures.forEach(function (measure: Measure) {
      if (!filteredMeasureBySector.hasOwnProperty(measure.sector)) {
        filteredMeasureBySector[measure.sector] = [measure];
        filteredSectors.push(measure.sector);
      } else {
        filteredMeasureBySector[measure.sector].push(measure);
      }
    })

    this.filteredSectors = filteredSectors;
    this.filteredMeasureBySector = filteredMeasureBySector;
  }

  private hasAny(array1: any[], array2: any[]): boolean {
    return array1.filter(value => array2.includes(value)).length > 0;
  }

  private findSelections(type: string, values: any): string[] {
      let selected:string[] = [];
      let value:any;

      for (value of Object.values(values)) {
          if (this.selected[type][value]) {
              selected.push(value);
          }
      }

      return selected;
  }

  ngOnDestroy() {
  }
}
