import { Component, Inject, ViewEncapsulation } from "@angular/core";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";

export interface DialogData {
    title: string;
    content: string;
    doneFunction: any;
}

@Component({
    selector: 'dialog',
    templateUrl: 'dialog.component.html',
    styleUrls: [ 'dialog.component.css' ],
    encapsulation: ViewEncapsulation.None,
})
export class DialogComponent {
    constructor(
        public dialogRef: MatDialogRef<DialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: DialogData) {}

    okClick(): void {
        if (this.data.doneFunction != null) {
            this.data.doneFunction();
        }

        this.dialogRef.close();
    }
}
