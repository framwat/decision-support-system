import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
import { CommonModule } from "@angular/common";
import { DialogComponent } from "./dialog.component";
import { HeaderComponent } from "./header.component";
import { YesNoDialogComponent } from "./yesno.dialog.component";
import { LoginComponent } from "./login.component";
import { AuthComponent } from "./auth.component";
import {ProgressDialogComponent} from "./progress-dialog.component";
import {MatProgressBarModule} from "@angular/material/progress-bar";
import {AppRoutingModule} from "../app-routing.module";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatDialogModule} from "@angular/material/dialog";
import {MatInputModule} from "@angular/material/input";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MatCheckboxModule} from "@angular/material/checkbox";
import {TranslateModule} from "@ngx-translate/core";
import {MatButtonModule} from "@angular/material/button";
import {MatMenuModule} from "@angular/material/menu";

@NgModule({
    declarations: [
        AuthComponent,
        DialogComponent,
        HeaderComponent,
        LoginComponent,
        ProgressDialogComponent,
        YesNoDialogComponent,
    ],
    imports: [
        AppRoutingModule,
        CommonModule,
        FormsModule,
        MatButtonModule,
        MatCheckboxModule,
        MatDialogModule,
        MatFormFieldModule,
        MatInputModule,
        MatMenuModule,
        MatProgressBarModule,
        ReactiveFormsModule,
        TranslateModule,
    ],
    exports: [
        HeaderComponent,
        DialogComponent,
        LoginComponent,
        ProgressDialogComponent,
        YesNoDialogComponent,
        AuthComponent
    ],
    entryComponents: [
        AuthComponent,
        DialogComponent,
        LoginComponent,
        ProgressDialogComponent,
        YesNoDialogComponent
    ],
    bootstrap: [],
    schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class SharedModule { }
