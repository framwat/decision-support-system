import { Component, Inject, ViewEncapsulation } from "@angular/core";

import {auth} from "firebase";
import {AngularFireAuth} from "@angular/fire/auth";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

export interface DialogData {
  title: string;
  content: string;
}

@Component({
  selector: 'login',
  templateUrl: 'login.component.html',
  styleUrls: [ 'login.component.css' ],
  encapsulation: ViewEncapsulation.None
})
export class LoginComponent {
    formBuilder = new FormBuilder();
    loginForm: FormGroup;

    error: string;

    constructor(
        public dialogRef: MatDialogRef<LoginComponent>,
        public afAuth: AngularFireAuth,
        @Inject(MAT_DIALOG_DATA) public data: DialogData) {

        this.loginForm = this.formBuilder.group(
            {
                email: ['', [Validators.required, Validators.email]],
                password: ['', [Validators.required, Validators.minLength(7)]],
                gdpr: [false, [Validators.requiredTrue]]
            }
        );
    }

    createNewAccount():void {
        let component = this;

        this.afAuth.auth.createUserWithEmailAndPassword(
            this.loginForm.get("email").value,
            this.loginForm.get("password").value,
        )
            .then(function(ok) {
                component.close();
            })
            .catch(function(error) {
                component.error = error.message;
            });
    }

    logWithEmail(): void {
        let component = this;

        this.afAuth.auth.signInWithEmailAndPassword(
            this.loginForm.get("email").value,
            this.loginForm.get("password").value,
        )
            .then(function(ok) {
                component.close();
            })
            .catch(function(error) {
                component.error = error.message;
            });
    }

    logWithGoogle(): void {
        let component = this;

        this.afAuth.auth.signInWithPopup(new auth.GoogleAuthProvider())
            .then(function(ok) {
                component.close();
            })
            .catch(function(error) {
                component.error = error.message;
            });
    }

    close():void {
        this.dialogRef.close();
    }

    validate(): void {

    }
}
