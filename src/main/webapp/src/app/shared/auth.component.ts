import { Component } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import {PlannerService} from "../planner/planner.service";
import {PlannerUser} from "../planner/planner.model";
import {LoginComponent} from "./login.component";
import {MatDialog} from "@angular/material/dialog";

@Component({
  selector: 'auth',
  template: `
    <div *ngIf="afAuth.user | async as user; else showLogin">
        <button mat-button [matMenuTriggerFor]="userMenu"><span *ngIf="user.displayName">{{ user.displayName }}</span><span *ngIf="!user.displayName">{{ user.email }}</span></button>
      <mat-menu #userMenu="matMenu" class="user-menu">
        <button mat-button routerLink="/administration">Administration panel</button>
        <button mat-button (click)="logout()">Logout</button>
      </mat-menu>
    </div>
    <ng-template #showLogin>
      <button (click)="login()" mat-button>Login</button>
    </ng-template>
  `,
})
export class AuthComponent {
    private isAdmin: boolean = false;

    constructor(
        public afAuth: AngularFireAuth,
        private dialog: MatDialog,
        private plannerService: PlannerService) {

        this.afAuth.user.subscribe(user => {
            if (user != null) {

        this.plannerService.getCurrentUserData().query.once("value").then(
          userDataSnapshot => {
            let userData = userDataSnapshot.val();

            if (userData == null) {
              userData = new PlannerUser(
                user.uid,
                false,
                user.email,
                user.displayName,
                "",
                new Date(),
                new Date()
              );
            } else {
              if (userData.isAdmin == true) {
                this.isAdmin = true;
              }

              userData.lastLoggedAt = new Date();
            }

            this.plannerService.updateCurrentUserData(userData);
          }
        );
      }
    });
  }
  login() {
      this.dialog.open(LoginComponent,
          {
              data: {},
              height: '460px',
              width: '360px',
          });
  }
    logout() {
        this.afAuth.auth.signOut();
    }
}
