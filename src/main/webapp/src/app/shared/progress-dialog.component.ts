import { Component, Inject, ViewEncapsulation } from "@angular/core";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";

export class ProgressDialogData {
  title: string = "";
  content: string = "";
  buttonName: string = "";
  url: string = "";
}

@Component({
  selector: 'progress-dialog',
  templateUrl: 'progress-dialog.component.html',
  styleUrls: [ 'progress-dialog.component.css' ],
  encapsulation: ViewEncapsulation.None
})
export class ProgressDialogComponent {
  public showProgress:boolean = true;

  constructor(
      public dialogRef: MatDialogRef<ProgressDialogComponent>,
      @Inject(MAT_DIALOG_DATA) public data: ProgressDialogData) {}

  closeClick(): void {
    this.dialogRef.close();
  }

  hideProgress():void {
    this.showProgress = false;
  }
}
