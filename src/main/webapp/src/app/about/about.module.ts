import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from "@angular/core";
import {AboutComponent} from "./about.component";
import {MarkdownModule} from "ngx-markdown";
import {CommonModule} from "@angular/common";

@NgModule({
    imports: [
        CommonModule,
        MarkdownModule.forChild(),
    ],
    declarations: [
        AboutComponent,
    ],
    entryComponents: [
        AboutComponent,
    ],
    providers: [

],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AboutModule {}
