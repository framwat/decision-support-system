import {Component, OnInit, ViewEncapsulation} from "@angular/core";
import {ActivatedRoute} from "@angular/router";
import {LangChangeEvent, TranslateService} from "@ngx-translate/core";

@Component({
    templateUrl: './about.component.html',
    styleUrls: ['./about.component.css'],
    encapsulation: ViewEncapsulation.None
})
export class AboutComponent implements OnInit {
    topic: string = "terms-of-use";
    selectedLanguage: string = "";

    constructor(
        private route: ActivatedRoute,
        private translate: TranslateService,
    ) {
        translate.onLangChange.subscribe(
            (lang: LangChangeEvent) => this.selectedLanguage = lang.lang == "pl" ? "pl" : "en"
        )
    }

    ngOnInit() {
        this.route.paramMap.subscribe(params => {
            this.topic = params.get("topic");
        });
    }
}
