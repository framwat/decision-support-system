import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MeasuresCatalogComponent } from './measures-catalog/measures-catalog.component';
import { EducationComponent } from './education/education.component';
import { HomeComponent } from "./home/home.component";
import { ToolsComponent } from "./tools/tools.component";
import { PlannerComponent } from "./planner/planner.component";
import {EducationMoviesComponent} from "./education/education-movies.component";
import {EducationDatabasesComponent} from "./education/education-databases.component";
import {EducationGuidelinesComponent} from "./education/education-guidelines.component";
import {EducationProgrammesComponent} from "./education/education-programmes.component";
import {EducationPublicationsComponent} from "./education/education-publications.component";
import {AdministrationComponent} from "./administration/administration.component";
import {AboutComponent} from "./about/about.component";
import {LegalAssistantComponent} from "./legal-assistant/legal-assistant.component";

const routes: Routes = [
    { path: 'measures-catalog', component: MeasuresCatalogComponent },
    { path: 'education', component: EducationComponent },
    { path: 'education/databases', component: EducationDatabasesComponent },
    { path: 'education/guidelines', component: EducationGuidelinesComponent },
    { path: 'education/movies', component: EducationMoviesComponent },
    { path: 'education/programmes', component: EducationProgrammesComponent },
    { path: 'education/publications', component: EducationPublicationsComponent },
    { path: 'tools', component: ToolsComponent },
    { path: 'home', component: HomeComponent },
    { path: 'planner', component: PlannerComponent },
    { path: 'planner/:userId/:mapId', component: PlannerComponent },
    { path: 'administration', component: AdministrationComponent },
    { path: 'about/:topic', component: AboutComponent },
    { path: 'legal-assistant', component: LegalAssistantComponent },
    { path: '**', redirectTo: 'home' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes,{ useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
