import {Component, Inject, OnInit, ViewEncapsulation} from "@angular/core";

import {Measure, MeasureStatus} from "./planner.model";
import {measures, Sector} from "../measures-catalog/measures-catalog.model";
import {PlannerService} from "./planner.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";

@Component({
    selector: 'measure-form',
    templateUrl: 'measure-form.component.html',
    styleUrls: [ 'measure-form.component.css' ],
    encapsulation: ViewEncapsulation.None
})
export class MeasureFormComponent implements OnInit {
    measureTypes: any;
    sectors: String[];
    isEdited: boolean = false;
    mapId: string;
    measure: Measure;
    form: FormGroup;
    formBuilder = new FormBuilder();
    positiveIntegerConstraintError = 'Value is required and has to be a positive integer.';
    positiveIntegerConstraints = [Validators.pattern(/^[0-9]+$/), Validators.min(0)];

    constructor(
        public dialogRef: MatDialogRef<MeasureFormComponent>,
        private plannerService: PlannerService,
        @Inject(MAT_DIALOG_DATA) public data: any
    ) {
        this.measure = data.measure;
        this.mapId = data.mapId;

        this.form = this.formBuilder.group({
            name: ['', Validators.required],
            type: ['', Validators.required],
            height: ['', this.positiveIntegerConstraints],
            depth: ['', this.positiveIntegerConstraints],
            length: ['', this.positiveIntegerConstraints],
            surface: ['', this.positiveIntegerConstraints],
            description: [''],
            type2: [''],
            type3: [''],
        });
    }

    ngOnInit() {
        let sector;
        let measureTypes = {};
        this.sectors = Object.values(Sector);

        for (sector of this.sectors) {
            measureTypes[sector] = [];
        }

        for (let measure of measures) {
            measureTypes[measure.sector].push(measure.id);
        }

        this.measureTypes = measureTypes;

        this.form.patchValue(this.measure);

        this.dialogRef.backdropClick().subscribe(() => { return false; });
    }

    updateMeasure() {
        this.isEdited = true;
    }

    fieldHasAnyError(fieldName: string): boolean {
        return this.form.get(fieldName).hasError('required')
            || this.form.get(fieldName).hasError('pattern')
            || this.form.get(fieldName).hasError('min');
    }

    saveMeasureForm() {
        Object.assign(this.measure, this.form.getRawValue());

        this.plannerService.updateMeasure(this.measure, this.mapId);

        this.dialogRef.close();
    }
}
