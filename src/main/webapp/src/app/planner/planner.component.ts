import {Component, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {FormBuilder} from '@angular/forms';
import {PlannerService} from './planner.service';
import {
    Control,
    Draw,
    DrawEvents,
    EditToolbar,
    FeatureGroup,
    LatLng, LatLngBounds,
    Layer,
    LeafletEvent,
    LeafletMouseEvent,
    Map,
    polygon,
    Polygon,
    TileLayer,
    tileLayer
} from 'leaflet';
import {AppComponent} from "../app.component";
import {
    BaseLayer,
    CountryDSSLayers,
    CustomWMSLayer,
    DSSDemoArea,
    DSSDemoAreas,
    DSSLayer,
    DSSLayers,
    Measure,
    MeasureStatus,
    PlannerMap,
} from "./planner.model";
import {AngularFireAuth} from "@angular/fire/auth";
import {ReportService} from "./report.service";
import {ProgressDialogComponent} from "../shared/progress-dialog.component";
import {MeasureFormComponent} from "./measure-form.component";
import {DialogComponent} from "../shared/dialog.component";
import {ActivatedRoute} from "@angular/router";
import {AngularFireAction, DatabaseSnapshot} from "@angular/fire/database";
import {CustomLayerFormComponent} from "./custom-layer-form.component";
import {CdkDragDrop, moveItemInArray} from '@angular/cdk/drag-drop';
import {MatDialog} from "@angular/material/dialog";
import {LangChangeEvent, TranslateService} from "@ngx-translate/core";
import {Feature} from "geojson";

@Component({
    selector: 'planner',
    templateUrl: './planner.component.html',
    styleUrls: ['./planner.component.css'],
    encapsulation: ViewEncapsulation.None
})
export class PlannerComponent implements OnInit, OnDestroy {
    baseLayers: BaseLayer[];
    baseTileLayer: TileLayer;
    clickedOnLayer: boolean = false;
    componentMap: Map;
    currentTab: string = 'mapSettings';
    drawControl: Control.Draw;
    drawnItems = new FeatureGroup();
    dssAreas: DSSDemoArea[];
    dssLayers: DSSLayer[] = [];
    editedItems = new FeatureGroup();
    editHandler: any;
    informationMode: boolean = false;
    leafletOptions: any;
    leafletLayers: any;
    leafletZoom: number;
    leafletCenter: LatLng;
    map:PlannerMap = new PlannerMap("", "", "Kamienna", [], new Date());
    measures: Measure[];
    measuresLoaded: boolean = false;
    selectedUserId: string = "";
    selectedMapId: string = "";
    selectedMeasure: Measure;
    selectedFeature;
    selectedLanguage: string = "";
    wmsLayers: any = {};
    progressDialogInstance: ProgressDialogComponent;

    constructor(
        private route: ActivatedRoute,
        private formBuilder: FormBuilder,
        private plannerService: PlannerService,
        private reportService: ReportService,
        private dialog: MatDialog,
        private app: AppComponent,
        public afAuth: AngularFireAuth,
        public translate: TranslateService,
    ) {
        this.formBuilder = new FormBuilder();

        translate.onLangChange.subscribe(
            (lang: LangChangeEvent) => this.selectedLanguage = lang.lang
        );

        console.log(this.afAuth.user)
    }

    ngOnInit() {
        this.route.paramMap.subscribe(params => {
            if (params.has("mapId") && params.has("userId")) {
                this.selectedMapId = params.get("mapId");
                this.selectedUserId = params.get("userId");
            }
        });

        this.app.setFooterBelow();

        this.dssAreas = DSSDemoAreas;

        this.baseLayers = this.plannerService.getBaseLayers();

        this.baseTileLayer = new TileLayer(this.baseLayers[0].urlTemplate);

        this.leafletOptions = {
            layers: [
                this.baseTileLayer,
                this.drawnItems,
                this.editedItems
            ],
            drawControl: false,
            zoomControl: false
        };

        this.leafletZoom = 11;
        this.leafletCenter = new LatLng(52.23, 21.03);

        console.log('init');

        this.drawControl = new Control.Draw({
            edit: {
                featureGroup: this.editedItems,
            }
        });
    }

    onMapMove(event: LeafletEvent) {
        alert(this.componentMap.getBounds().getNorthWest() + ", " + this.componentMap.getBounds().getSouthEast());
    }

    onMapReady(componentMap: Map) {
        this.componentMap = componentMap;

        setTimeout( () => {
            this.componentMap.invalidateSize();}, 100);

        this.afAuth.user.subscribe(user => {
            if (user != null) {
                let tempFeatureGroup: any = {
                    featureGroup: this.editedItems
                };

                this.editHandler = new EditToolbar.Edit(
                    this.componentMap,
                    tempFeatureGroup
                );

                if (this.selectedMapId == "" && this.selectedUserId == "") {
                    this.plannerService.getMapsForAuthenticatedUser().snapshotChanges().subscribe(maps => {
                        let loadedMaps = maps.map(map => map.payload.val());

                        if (loadedMaps.length == 0) {
                            this.map = this.plannerService.createMap(this.map);
                            this.selectedMapId = this.map.id;
                        } else {
                            let plannerMap = loadedMaps[0];
                            let layers: CustomWMSLayer[] = [];

                            this.dssLayers = [];

                            for (let layerId in plannerMap.customLayers) {
                                let customLayer  = plannerMap.customLayers[layerId];

                                layers.push(plannerMap.customLayers[layerId]);

                                let dssLayer = new DSSLayer(
                                    customLayer.wms,
                                    customLayer.name,
                                    customLayer.active,
                                    customLayer.layerName,
                                    0.7,
                                    "default",
                                    true
                                );

                                this.dssLayers.push(dssLayer);
                                this.addLayerToMap(dssLayer);
                            }

                            if (CountryDSSLayers[plannerMap.catchment.trim()] != undefined) {
                                for (let dssLayer of CountryDSSLayers[plannerMap.catchment.trim()]) {
                                    this.dssLayers.push(dssLayer);
                                    this.addLayerToMap(dssLayer);
                                }
                            }

                            for (let dssLayer of DSSLayers) {
                                this.dssLayers.push(dssLayer);
                                this.addLayerToMap(dssLayer);
                            }

                            plannerMap.customLayers = layers;

                            this.map = plannerMap;

                            this.selectedMapId = this.map.id;

                            this.onCatchmentChange(this.map.catchment);
                        }

                        this.loadMeasurements();
                    });

                } else {
                    this.plannerService.getMapByUserIdMapId(this.selectedUserId, this.selectedMapId).then(
                        map => {
                            this.map = map;
                            this.onCatchmentChange(this.map.catchment);
                            this.loadMeasurements();
                        }
                    );
                }
            }
        });
    }

    ngOnDestroy() {
    }

    onCatchmentChange(catchment: String) {
        for (let code in this.dssAreas) {
            let demoArea:DSSDemoArea = this.dssAreas[code] as DSSDemoArea;

            if (demoArea.name == catchment) {
                this.componentMap.setMinZoom(demoArea.zoom);
                this.componentMap.setZoom(demoArea.zoom);
                this.componentMap.setMaxBounds(demoArea.bounds);
            }
        }
    }

    loadMeasurements() {
        this.plannerService.getCurrentUserMeasuresList(this.selectedMapId).snapshotChanges().subscribe(
            measures => this.processLoadedMeasures(measures)
        );
    }

    updateMapSettings() {
        this.plannerService.updateMap(this.map);
    }

    onMapLayerChanged(checked: boolean, layer:DSSLayer) {
        this.wmsLayers[layer.name]._container.style.display = checked ? 'block' : 'none';
    }

    private addLayerToMap(
        dssLayer: DSSLayer
    ) {
        let layer = tileLayer.wms(dssLayer.wms, {
            layers: dssLayer.layerName,
            format: dssLayer.format,
            transparent: true,
            maxZoom: 22,
            minZoom: 0,
            opacity: dssLayer.opacity,
            styles: dssLayer.styles
        }).addTo(this.componentMap);

        this.wmsLayers[dssLayer.name] = layer;
        this.wmsLayers[dssLayer.name]._container.style.display = dssLayer.active ? 'block' : 'none';
    }

    private processLoadedMeasures(measures: AngularFireAction<DatabaseSnapshot<Measure>>[]): void {
        if (this.measuresLoaded == true) {
            return;
        }

        this.measuresLoaded = true;

        let drawnItems = this.drawnItems;
        let planner:PlannerComponent = this;

        this.measures = measures.map(measure => {
            let measureVal = measure.payload.val();

            //here should be validation of read measure
            measureVal.status = MeasureStatus.SUCCESS;
            measureVal.id = measure.payload.key;

            if (measureVal.geojson) {
                let poly = polygon(planner.getCoordinatesFromGeoJSON(measureVal.geojson),
                    PlannerComponent.getDefaultPolygonStyle()
                );

                measureVal.layer = poly;

                drawnItems.addLayer(poly);

                poly.on('click', function(e){
                    planner.setClickedOnFeature(poly);
                });
            }

            return measureVal;
        });
    }

    public updateMeasureList(updatedMeasure:Measure) {
        for (let i in this.measures) {
            if (this.measures[i].id == updatedMeasure.id) {
                this.measures[i] = updatedMeasure;
            }
        }
    }

    addMeasure() {
        if (this.selectedFeature) {
            alert('already selected');

            return;
        }

        this.showMeasuresList();

        new Draw.Polygon(this.componentMap).enable();

        console.log("NEW SELECTED MEASURE");

        this.selectedMeasure = new Measure("New measure", MeasureStatus.EDIT);
        console.log(this.selectedMeasure);

        this.measures.push(this.selectedMeasure);
    }

    onDrawCreated(event: DrawEvents.Created) {
        let geoJson = event.layer.toGeoJSON();

        let newPoly = polygon(this.getCoordinatesFromGeoJSON(geoJson),
            PlannerComponent.getDefaultPolygonStyle()
        );

        this.selectedMeasure.layer = newPoly;
        this.selectedMeasure.geojson = geoJson;
        this.selectedMeasure.id = this.plannerService.createMeasure(this.selectedMeasure, this.selectedMapId);

        this.measures[this.measures.length - 1] = this.selectedMeasure;

        this.editedItems.removeLayer(event.layer);
        this.drawnItems.addLayer(newPoly);

        let planner = this;

        newPoly.on('click', function(e){
            planner.setClickedOnFeature(newPoly);
        })

        this.showEditForm(this.selectedMeasure);
    }

    onClickMap(event: LeafletMouseEvent) {
        if (this.informationMode) {
            this.plannerService.getInformationForPoint(event.latlng.lat, event.latlng.lng)
                .subscribe((resp) => {
                    this.dialog.open(DialogComponent,
                        {
                            data: {
                                title: `${Math.round(event.latlng.lat * 1000) / 1000}, ${Math.round(event.latlng.lng * 1000) / 1000}`,
                                content: `
                                 Location: <b>${resp.administrationNamePath.join(" - ")}</b><br>
                                 Parcel id: <b>${resp.parcelName}</b><br>
                                 Parcel area: <b>${resp.parcelArea} m<sup>2</sup></b><br>
                                  `,
                            },
                            height: '250px',
                            width: '400px',
                        });
                });

            this.informationMode = false;

            return;
        }

        let planner:PlannerComponent = this;
        let clickedOnLayer: boolean = this.clickedOnLayer;

        setTimeout(function() {
            if (!clickedOnLayer) {
                planner.stopEditing();
            }

            planner.unsetClickedOnLayer();
        }, 50);
    }

    private showMeasureByLayer(layer: Layer) {
        for (let measure of this.measures) {
            if (measure.layer == layer) {
                this.selectedMeasure = measure;
                return;
            }
        }

        console.log('measure not find :/');
    }

    private showEditForm(measure: Measure) {
        this.setClickedOnFeature(measure.layer);

        this.dialog.open(MeasureFormComponent,
            {
                data: {measure: measure, mapId: this.selectedMapId},
                height: '400px',
                width: '400px',
                disableClose: true
            }
        ).afterClosed().subscribe(result => {
              measure.status = MeasureStatus.SUCCESS;
        });
    }

    zoomIn() {
        this.componentMap.zoomIn();
    }

    zoomOut() {
        this.componentMap.zoomOut();
    }

    showBaseLayers():void {
        this.currentTab = 'baseLayers';
    }

    showMeasuresList():void {
        this.currentTab = 'measuresList';
    }

    showMapSettings():void {
        this.currentTab = 'mapSettings';
    }

    changeBaseLayer(baseLayer: BaseLayer):void {
        console.log(baseLayer.urlTemplate);

        this.baseTileLayer.setUrl(baseLayer.urlTemplate);
        this.baseTileLayer.redraw()
    }

    private getMeasureStatus() {
        return MeasureStatus;
    }

    private isFeatureForThisMeasure(feature: Feature):boolean {
        return !this.selectedFeature || this.selectedFeature == feature;
    }

    goToMeasurePolygon(event: MouseEvent, measure: Measure): void {
        let minLat = 180;
        let minLon = 180;
        let maxLat = -180;
        let maxLon = -180;

        let coordinates = (measure.geojson.geometry as any).coordinates[0];

        for (let i = 0; i < coordinates.length; i++) {
            let lon = coordinates[i][0];
            let lat = coordinates[i][1];

            minLat = Math.min(lat, minLat);
            minLon = Math.min(lon, minLon);
            maxLat = Math.max(lat, maxLat);
            maxLon = Math.max(lon, maxLon);
        }

        this.componentMap.fitBounds([[minLat, minLon], [maxLat, maxLon]]);
    }

    public unsetClickedOnLayer() {
        this.clickedOnLayer = false;
    }

    public setClickedOnFeature(selectedFeature: Polygon) {
        this.clickedOnLayer = true;

        if (selectedFeature != null) {
            if (selectedFeature == this.selectedFeature) {
                return;
            }

            this.stopEditing();
        }

        this.showMeasuresList();
        this.showMeasureByLayer(selectedFeature);
        this.selectedMeasure.status = MeasureStatus.EDIT;
        (selectedFeature.options as any).original = selectedFeature.options;
        (selectedFeature.options as any).previousOptions = selectedFeature.options;
        (selectedFeature.options as any).editing = {};
        this.selectedFeature = selectedFeature;

        this.editedItems.addLayer(selectedFeature);
        this.editHandler.enable()
    }

    public stopEditing() {
        if (this.selectedFeature) {
            let planner:PlannerComponent = this;

            this.editHandler.disable();
            this.editedItems.removeLayer(this.selectedFeature);

            let geoJson = this.selectedFeature.toGeoJSON();
            let newPoly = polygon(
                this.getCoordinatesFromGeoJSON(geoJson),
                PlannerComponent.getDefaultPolygonStyle()
            );

            this.selectedMeasure.layer = newPoly;
            this.selectedMeasure.geojson = geoJson;
            this.selectedMeasure.status = MeasureStatus.SUCCESS;
            this.updateMeasure(this.selectedMeasure);

            this.drawnItems.addLayer(newPoly);

            newPoly.on('click', function(e){
                planner.setClickedOnFeature(newPoly);
            });

            this.selectedFeature = null;
            this.selectedMeasure = null;
        }
    }

    updateMeasure(updatedMeasure:Measure) {
        this.updateMeasureList(updatedMeasure);

        this.plannerService.updateMeasure(updatedMeasure, this.selectedMapId);
    }

    addCustomLayer() {
        this.dialog.open(CustomLayerFormComponent,
            {
                data: {
                    customWMSLayer: new CustomWMSLayer("", "", "", ""),
                    mapId: this.selectedMapId
                },
                height: '370px',
                width: '400px',
            }
            );
    }

    public getCoordinatesFromGeoJSON(geoJson: any) {
        let coordinates = (geoJson.geometry as any).coordinates[0];
        return [coordinates.map(function(point){return [point[1], point[0]];})];
    }

    public removeMeasure(measureToRemove: Measure) {
        this.measures.splice(this.measures.indexOf(measureToRemove), 1);
        this.drawnItems.removeLayer(measureToRemove.layer);
        this.plannerService.removeMeasure(measureToRemove, this.selectedMapId);
    }

    public static getDefaultPolygonStyle() {
        return {
            weight: 1,
            fillOpacity: 0.7,
            color: '#778CC8',
            dashArray: '3'
        };
    }

    public generateReport():void {
        this.progressDialogInstance = this.dialog.open(ProgressDialogComponent, {
            data: { title: `Report`, content: `Generating new report. Please wait.`, buttonName: `Download` },
            height: '200px',
            width: '350px'
        }).componentInstance;

        this.reportService.generateReport(this.measures, this.selectedLanguage).subscribe(
            reportStatus => { this.checkReport(reportStatus.uuid); },
                error => { console.error(error); }
                );
    }

    private checkReport(uuid: string) {
        this.reportService.getReportStatus(uuid).subscribe(
            reportStatus => {
                if (reportStatus.status == "DONE") {
                    this.progressDialogInstance.data.url = this.reportService.getReportUrl(uuid);
                    this.progressDialogInstance.hideProgress();
                } else if (reportStatus.status == "ERROR") {
                    this.progressDialogInstance.data.title = "Error";
                    this.progressDialogInstance.data.content = "Calculation cannot be done because of error. More information with error code: " + uuid + ".";
                    this.progressDialogInstance.hideProgress();
                } else {
                    setTimeout(() => { this.checkReport(uuid); }, 5000);
                }
            }
            );
    }

    public findInformation():void {
        this.informationMode = !this.informationMode;

        if (this.informationMode) {
        }

        //change cursor
    }

    dropLayerListElement(event: CdkDragDrop<string[]>) {
        moveItemInArray(this.dssLayers, this.dssLayers.length - event.previousIndex - 1, this.dssLayers.length - event.currentIndex - 1);

        let i = 0;
        for (let layer of this.dssLayers) {
            (<TileLayer.WMS>this.wmsLayers[layer.name]).setZIndex(i++);
        }
    }
}
