import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {ReportStatus} from "./report.model";
import {environment} from "../../environments/environment";
import {Measure} from "./planner.model";

@Injectable()
export class ReportService {

    private resourceUrl = `${environment.url}/report`;

    constructor(private http: HttpClient) { }

    generateReport(measures:Measure[], locale:String):Observable<ReportStatus> {
        let request = {};

        for (let measure of measures) {
            request[measure.id] = measure;
            delete request[measure.id].layer;
        }

        return this.http.post<ReportStatus>(this.resourceUrl + "?locale=" + locale, request);
    }

    getReportStatus(uuid: string): Observable<ReportStatus> {
        return this.http.get<ReportStatus>(this.resourceUrl + '/status/' + uuid);
    }

    getReportUrl(uuid: string) {
        return this.resourceUrl + '/download/' + uuid;
    }

    getReportFile(uuid: string) {
        window.location.href = this.getReportUrl(uuid);
    }
}
