import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList, AngularFireObject } from "@angular/fire/database";
import {BaseLayer, CustomWMSLayer, MapInformation, Measure, PlannerMap, PlannerUser} from "./planner.model";
import { AngularFireAuth } from "@angular/fire/auth";
import {HttpClient, HttpResponse} from "@angular/common/http";
import {Observable} from "rxjs";
import {environment} from "../../environments/environment";

@Injectable()
export class PlannerService {
    private measuresBasePath: string = '/measures';
    private mapsBasePath: string = '/maps';
    private usersBasePath: string = '/users';

    private resourceUrl = `${environment.url}`;

    measures: AngularFireList<Measure> = null;
    map: AngularFireObject<PlannerMap> = null;
    maps: AngularFireList<PlannerMap> = null;

    constructor(private db: AngularFireDatabase, private afAuth: AngularFireAuth, private http: HttpClient) {}

    getBaseLayers():BaseLayer[] {
        return [
            new BaseLayer(
            'Open Street Maps',
              'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
              'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors'
            ),
            new BaseLayer(
              'Stamen terrain map',
              'http://{s}.tile.stamen.com/terrain/{z}/{x}/{y}.png',
              'Map tiles by <a href="http://stamen.com">Stamen Design</a>, <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a> &mdash; Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>'),
        ]
    }

    getInformationForPoint(lat:number, lng: number): Observable<MapInformation> {
        return this.http.get<MapInformation>(`${this.resourceUrl}/information/${lat}/${lng}`);
    }

    getMapsForAuthenticatedUser(): AngularFireList<PlannerMap> {
        this.maps = this.db.list(this.mapsBasePath + "/" + this.afAuth.auth.currentUser.uid);

        return this.maps;
    }

    getMapByUserIdMapId(userId: string, mapId: string): Promise<PlannerMap> {
        return new Promise<PlannerMap>(
          (resolved, rejected) => {
            this.db.object(this.mapsBasePath + "/" + userId + "/" + mapId).query.once("value").then(
              mapSnapshot => {
                resolved(mapSnapshot.val());
              }
            );
          }
        );
    }

    getCurrentUserData(): AngularFireObject<PlannerUser> {
        console.log('getCurrentUserData', this.usersBasePath + "/" + this.afAuth.auth.currentUser.uid);

        return this.db.object(this.usersBasePath + "/" + this.afAuth.auth.currentUser.uid);
    }

    updateCurrentUserData(plannerUser: PlannerUser) {
        this.updateDBObject(this.usersBasePath + "/" + this.afAuth.auth.currentUser.uid, plannerUser);
    }

    createMap(map: PlannerMap): PlannerMap {
        const newKey = this.db.database.ref().child(this.mapsBasePath + "/" + this.afAuth.auth.currentUser.uid).push().key;

        map.id = newKey;

        this.updateDBObject(this.mapsBasePath + "/" + this.afAuth.auth.currentUser.uid + "/" + newKey, map);

        return map;
    }

    updateMap(map: PlannerMap): void {
        this.updateDBObject(this.mapsBasePath + "/" + this.afAuth.auth.currentUser.uid + "/" + map.id, map);
    }

    getCurrentUserMeasuresList(mapId: string): AngularFireList<Measure> {
        return this.getMeasuresListByUid(this.afAuth.auth.currentUser.uid, mapId);
    }

    getMeasuresListByUid(userId: string, mapId: string): AngularFireList<Measure> {
        //this.db.database.ref().once('value', function(snapshot) {
        //  console.log("createMap", snapshot.hasChild("measures"));
        //});

        console.log('getMeasuresListByUid', this.measuresBasePath + "/" + userId + "/" + mapId);

        this.measures = this.db.list(this.measuresBasePath + "/" + userId + "/" + mapId);

        return this.measures;
    }

    createMeasure(measure: Measure, mapId: string): string  {
        let newMeasure:Measure = Object.assign({}, measure);
        delete newMeasure.layer;
        delete newMeasure.id;
        delete newMeasure.status;
        newMeasure.type = null;

        let path = this.measuresBasePath + "/" + this.afAuth.auth.currentUser.uid + "/" + mapId;

        let newKey = this.db.database.ref().child(path).push().key;

        newMeasure.id = newKey;

        for (let key in newMeasure) {
            if (newMeasure[key] === undefined) {
                newMeasure[key] = null;
            }
        }

        this.db.database.ref(path + "/" + newKey).set(newMeasure).catch(
            error => console.log(error)
        );

        return newKey;
    }

    updateMeasure(measure: Measure, mapId: string): void {
        console.log('updateMeasure', measure, mapId);

        let newMeasure:Measure = Object.assign({}, measure);
        delete newMeasure.layer;
        delete newMeasure.id;
        delete newMeasure.status;

        for (let key in newMeasure) {
            if (newMeasure[key] === undefined) {
                newMeasure[key] = null;
            }
        }

        this.updateDBObject(this.measuresBasePath + "/" + this.afAuth.auth.currentUser.uid + "/" + mapId + "/" + measure.id, newMeasure);
    }

    public removeMeasure(measure: Measure, mapId: string): void {
        const refPath = this.measuresBasePath + "/" + this.afAuth.auth.currentUser.uid + "/" + mapId + "/" + measure.id;

        this.db.database.ref(refPath).remove().catch(error => console.log(error));
    }

    createCustomLayer(customLayer: CustomWMSLayer, mapId: string): string  {
        let newCustomLayer:CustomWMSLayer = Object.assign({}, customLayer);

        let path = this.mapsBasePath + "/" + this.afAuth.auth.currentUser.uid + "/" + mapId + "/customLayers";

        let newKey = this.db.database.ref().child(path).push().key;

        newCustomLayer.id = newKey;

        for (let key in newCustomLayer) {
            if (newCustomLayer[key] === undefined) {
                newCustomLayer[key] = null;
            }
        }

        console.log("saving custom layer");
        console.log(path + "/" + newKey);

        this.db.database.ref(path + "/" + newKey).set(newCustomLayer).catch(
            error => console.log(error)
        );

        return newKey;
    }

    private updateDBObject(path: string, object: any) {
        let updates = {};
        updates[path] = object;

        this.db.database.ref()
            .update(updates)
            .catch(error => console.log(error))
    }
}
