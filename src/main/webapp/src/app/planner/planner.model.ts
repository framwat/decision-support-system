import {Feature} from "geojson";
import {LatLngBounds} from "leaflet";
import {environment} from "../../environments/environment";

export class BaseLayer {
    constructor(
        public name: string,
        public urlTemplate: string,
        public attribution: string
    ) {
    }
}

export class Country {
    constructor(
        public name: string,
    ) {

    }
}

export class Measure {
    constructor(
        public name: string,
        public status: MeasureStatus,
        public description?: string,
        public length?: number,
        public height?: number,
        public depth?: number,
        public surface?: number,
        public type?: string,
        public type2?: string,
        public type3?: string,
        public id?: string,
        public geojson?: Feature,
        public layer?: any
    ) {

    }
}

export enum MeasureStatus {
    EDIT, SUCCESS, ERROR
}

export class MapInformation {
    constructor(
        public administrationNamePath: string[],
        public parcelName: string,
        public parcelArea: number) {

    }
}

export class PlannerMap {
    constructor(
        public id: string,
        public name: string,
        public catchment: string,
        public customLayers: CustomWMSLayer[],
        public createdAt: Date
    ) {
    }
}

export class PlannerMapView {
  constructor(
    public id: string,
    public uid: string,
    public author: string,
    public name: string,
    public country: string,
    public createdAt: Date
  ) {
  }
}

export class PlannerUser {
    constructor(
        public id: string,
        public isAdmin: boolean,
        public email: string,
        public name: string,
        public organization: string,
        public createdAt: Date,
        public lastLoggedAt: Date
    ) {
    }
}

export class DSSLayer {
    constructor(
        public wms: string,
        public name: string,
        public active:boolean,
        public layerName: string,
        public opacity: number,
        public styles: string,
        public isCustom: boolean = false,
        public format: string = "image/vnd.jpeg-png8"
    ) {
    }
}

export class CustomWMSLayer {
    constructor(
        public id: string,
        public wms: string,
        public layerName: string,
        public name: string,
        public active: boolean = true,
        public legend?: string
    ){}
}

export var CountryDSSLayers = {
    "Kamienna": [
        new DSSLayer(
            `${environment.url}/proxy/http/mapy.geoportal.gov.pl/wss/service/img/guest/ORTO/MapServer/WMSServer`,
            "Ortophotomap PL",
            false,
            "Raster",
            1,
            "default"
        ),
        new DSSLayer(
            `${environment.url}/proxy/http/mapy.geoportal.gov.pl/wss/service/PZGIK/NMT/GRID1/WMS/ShadedRelief`,
            "DEM Geoportal",
            false,
            "Raster",
            0.8,
            "default"
        ),
    ],
    "Nagykunsági": [
        new DSSLayer(
            `${environment.url}/proxy/https/geoportal.vizugy.hu/arcgis/services/KOTIVIZIG/FRAMWAT/MapServer/WMSServer`,
            "Ortophotomap HU",
            true,
            "1",
            1,
            "default"
        ),
    ],
    "Slaná": [
        new DSSLayer(
            `${environment.url}/proxy/https/zbgisws.skgeodesy.sk/zbgis_ortofoto_wms/service.svc/get`,
            "Ortophotomap SK",
            true,
            "1",
            1,
            "default"
        ),
    ],
    "Kamniska Bistrica": [
        new DSSLayer(
            `${environment.url}/proxy/https/prostor4.gov.si/ows2-m-pub/SI.GURS.ZPDZ/wms`,
            "Ortophotomap SL",
            true,
            "DOF050",
            1,
            "dof",
            false,
            "image/png"
        )
    ],
    "All": [],
};

export var DSSLayers = [
  new DSSLayer(
    `${environment.url}/proxy/http/levis-framwat.sggw.pl:8888/geoserver/dss/wms`,
    "TWI",
    false,
    "dss:twi",
    0.8,
    ""
  ),
  new DSSLayer(
    `${environment.url}/proxy/http/levis-framwat.sggw.pl:8888/geoserver/dss/wms`,
    "LANDSLOPE",
    false,
    "dss:landSlope",
    0.8,
    ""
  ),
  new DSSLayer(
    `${environment.url}/proxy/http/levis-framwat.sggw.pl:8888/geoserver/dss/wms`,
    "DEM",
    false,
    "dss:dem",
    0.8,
    ""
  ),
  new DSSLayer(
    `${environment.url}/proxy/http/levis-framwat.sggw.pl:8888/geoserver/dss/wms`,
    "LANDUSES",
    false,
    "dss:landuses",
    0.7,
    ""
  ),
  new DSSLayer(
    `${environment.url}/proxy/http/levis-framwat.sggw.pl:8888/geoserver/dss/wms`,
    "POTENTIAL_FWR_GENERAL",
    false,
    "dss:potentialforwaterretention",
    0.7,
    "potentialForWaterRetention cGeneral"
  ),
  new DSSLayer(
    `${environment.url}/proxy/http/levis-framwat.sggw.pl:8888/geoserver/dss/wms`,
    "POTENTIAL_FWR_SEDIMENT",
    false,
    "dss:potentialforwaterretention",
    0.7,
    "potentialForWaterRetention cSediment"
  ),
  new DSSLayer(
    `${environment.url}/proxy/http/levis-framwat.sggw.pl:8888/geoserver/dss/wms`,
    "POTENTIAL_FWR_QUALITY",
    false,
    "dss:potentialforwaterretention",
    0.7,
    "potentialForWaterRetention cWaterQuality"
  ),
  new DSSLayer(
    `${environment.url}/proxy/http/levis-framwat.sggw.pl:8888/geoserver/dss/wms`,
    "POTENTIAL_FWR_FLOOD",
    false,
    "dss:potentialforwaterretention",
    0.7,
    "potentialForWaterRetention cFlood"
  ),
  new DSSLayer(
    `${environment.url}/proxy/http/levis-framwat.sggw.pl:8888/geoserver/dss/wms`,
    "POTENTIAL_FWR_DROUGHT",
    false,
    "dss:potentialforwaterretention",
    0.7,
    "potentialForWaterRetention cDrought"
  ),
  new DSSLayer(
    `${environment.url}/proxy/http/levis-framwat.sggw.pl:8888/geoserver/dss/wms`,
    "PROTECTED_SURFACE",
    false,
    "dss:pssites",
    0.7,
    ""
  ),
  new DSSLayer(
    `${environment.url}/proxy/http/levis-framwat.sggw.pl:8888/geoserver/dss/wms`,
    "PROTECTED_POINT",
    false,
    "dss:pssitep",
    1,
    ""
  ),
  //hypSurfaceWaterS - true
  new DSSLayer(
    `${environment.url}/proxy/http/levis-framwat.sggw.pl:8888/geoserver/dss/wms`,
    "CATASTRAL_PARCELS",
    false,
    "dss:cpparcels",
    1,
    ""
  ),
  new DSSLayer(
    `${environment.url}/proxy/http/levis-framwat.sggw.pl:8888/geoserver/dss/wms`,
    "LAKE",
    true,
    "dss:hypsurfacewaters",
    1,
    ""
  ),
  new DSSLayer(
    `${environment.url}/proxy/http/levis-framwat.sggw.pl:8888/geoserver/dss/wms`,
    "RIVER",
    true,
    "dss:hypsurfacewaterl",
    1,
    ""
  ),
  new DSSLayer(
    `${environment.url}/proxy/http/levis-framwat.sggw.pl:8888/geoserver/dss/wms`,
    "ADM_UNITS",
    true,
    "dss:auadmunits",
    1,
    ""
  ),
];

export class DSSDemoArea {
    constructor(
        public name: string,
        public bounds: LatLngBounds,
        public zoom: number
    ) {

    }
};

export var DSSDemoAreas = [
  new DSSDemoArea("Kamienna", new LatLngBounds([50.706895, 20.45929], [51.297135, 22.024841]), 10),
  new DSSDemoArea("Slaná", new LatLngBounds([48.153261, 19.364777], [48.775198, 20.930328]), 10),
  new DSSDemoArea("Nagykunsági ", new LatLngBounds([46.560749, 19.135437], [47.835283, 22.266541]), 9),
  new DSSDemoArea("Kamniska Bistrica", new LatLngBounds([46.038923, 14.261627], [46.363515, 15.044403]), 10),
  new DSSDemoArea("All", new LatLngBounds([36, -15], [58, 38]), 7),
];
