import {Component, Inject, OnInit, ViewEncapsulation} from "@angular/core";

import {CustomWMSLayer} from "./planner.model";
import { FormControl, FormBuilder, FormGroup, Validators } from "@angular/forms";
import {HttpClient} from "@angular/common/http";
import {WMSService} from "./wms.service";
import {PlannerService} from "./planner.service";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";

@Component({
  selector: 'custom-layer-form',
  templateUrl: 'custom-layer-form.component.html',
  styleUrls: [ 'custom-layer-form.component.css' ],
  encapsulation: ViewEncapsulation.None
})
export class CustomLayerFormComponent implements OnInit {
    isEdited: boolean = false;
    customWMSLayer: CustomWMSLayer;
    form: FormGroup;
    showWMSLayers: boolean = false;
    validURL: boolean = false;
    loadedWMSLayers: CustomWMSLayer[];
    mapId: string;

    constructor(
        public dialogRef: MatDialogRef<CustomLayerFormComponent>,
        private formBuilder: FormBuilder,
        private http: HttpClient,
        private wmsService: WMSService,
        private plannerService: PlannerService,
        @Inject(MAT_DIALOG_DATA) public data: any
    ) {
        this.customWMSLayer = data.customWMSLayer;
        this.mapId = data.mapId;

        this.form = formBuilder.group({
            name: new FormControl(null, Validators.required),
            wmsURL: new FormControl(null, [Validators.required, Validators.pattern(/^(http[s]?:\/\/){0,1}(www\.){0,1}[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,5}[\.]{0,1}/)]),
            wmsLayerName: new FormControl(null, Validators.required),
        });
    }

    ngOnInit() {
        this.dialogRef.backdropClick().subscribe(() => { return false; });
    }

    tryConnection() {
        var url = this.form.get('wmsURL').value;

        this.wmsService.getCapabilities(url).then(
            layers => {
                this.showWMSLayers = true;
                this.loadedWMSLayers = layers;
            }
        );
    }

    saveCustomWMSLayer() {
        let layerName = this.form.get('wmsLayerName').value;
        let selectedLayer: CustomWMSLayer;

        for (let layer of this.loadedWMSLayers) {
            if (layer.layerName == layerName) {
                selectedLayer = layer;
                break;
            }
        }

        this.plannerService.createCustomLayer(
            new CustomWMSLayer(
                "",
                this.form.get('wmsURL').value,
                this.form.get('wmsLayerName').value,
                this.form.get('name').value,
                true,
                selectedLayer.legend,
            ),
            this.mapId
        );

        this.dialogRef.close();
    }

    urlChanged() {
        this.validURL = this.form.get('wmsURL').valid;
    }
}
