import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from "@angular/common";

import { LeafletDrawModule } from "@asymmetrik/ngx-leaflet-draw";
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { PlannerComponent } from './planner.component';
import { PlannerService } from "./planner.service";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from "../shared/shared.module";
import { TranslateModule } from "@ngx-translate/core";
import { AngularFireDatabaseModule } from "@angular/fire/database";
import { HttpClientModule } from "@angular/common/http";
import { ReportService } from "./report.service";
import { MeasureFormComponent } from "./measure-form.component";
import { MatDialogModule } from "@angular/material/dialog";
import { CustomLayerFormComponent } from "./custom-layer-form.component";
import { WMSService } from "./wms.service";
import { DragDropModule } from "@angular/cdk/drag-drop";
import {MatButtonModule} from "@angular/material/button";
import {MatDatepickerModule} from "@angular/material/datepicker";
import {MatDividerModule} from "@angular/material/divider";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatIconModule} from "@angular/material/icon";
import {MatInputModule} from "@angular/material/input";
import {MatNativeDateModule} from "@angular/material/core";
import {MatSelectModule} from "@angular/material/select";
import {MatSlideToggleModule} from "@angular/material/slide-toggle";
import {MatTabsModule} from "@angular/material/tabs";
import {MatTableModule} from "@angular/material/table";
import {MatTooltipModule} from "@angular/material/tooltip";

@NgModule({
    imports: [
        AngularFireDatabaseModule,
        CommonModule,
        DragDropModule,
        FormsModule,
        HttpClientModule,
        LeafletModule.forRoot(),
        LeafletDrawModule.forRoot(),
        MatButtonModule,
        MatDatepickerModule,
        MatDialogModule,
        MatDividerModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatNativeDateModule,
        MatSelectModule,
        MatSlideToggleModule,
        MatTabsModule,
        MatTableModule,
        MatTooltipModule,
        ReactiveFormsModule,
        SharedModule,
        TranslateModule
    ],
        declarations: [ PlannerComponent, MeasureFormComponent, CustomLayerFormComponent ],
        entryComponents: [ PlannerComponent, MeasureFormComponent, CustomLayerFormComponent ],
        providers: [ PlannerService, ReportService, WMSService ],
        schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
    })
export class PlannerModule {}
