import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import * as X2JS from "x2js";
import {CustomWMSLayer} from "./planner.model";
import {environment} from "../../environments/environment";

@Injectable()
export class WMSService {
    constructor(private http: HttpClient) {}

    public getCapabilities(url: string): Promise<CustomWMSLayer[]> {
        var x2js = new X2JS();

        const urlArray = url.split("/");
        const proto = urlArray.shift().split(":").join("");
        urlArray.shift();

        url = environment.url + "/proxy/" + proto + "/" + urlArray.join("/");

        console.log("url to load: ", url);

        return new Promise<CustomWMSLayer[]>((resolve, reject) => {
            this.http.get( url+ "?SERVICE=WMS&VERSION=1.1.1&REQUEST=GetCapabilities", { responseType: 'text' }).subscribe(
                data => {
                    let wmsData: any = x2js.xml2js(data);
                    let wmsLayers: CustomWMSLayer[] = [];

                    for (let layer of wmsData.WMT_MS_Capabilities.Capability.Layer.Layer) {
                        let legendUrl = "";

                        if (layer.Style != undefined && layer.Style.LegendURL != undefined && layer.Style.LegendURL.OnlineResource != undefined) {
                            legendUrl = layer.Style.LegendURL.OnlineResource["_xlink:href"];
                        }

                        wmsLayers.push(new CustomWMSLayer("", url, layer.Name, layer.Title, true, legendUrl));
                    }

                    resolve(wmsLayers);
                },
                error => {
                    reject(error);
                }
            );
        });
    }
}

class WMSXML {
  WMT_MS_Capabilities: WMT_MS_Capabilities;
}

class WMSCapability {
  Request: any;
  Layer: WMSLayers;
  SRS: string[];
}

class WMT_MS_Capabilities {
    Service: WMS_Service;
    Capability: WMSCapability;
    _version: string;
}

class WMSLayer {
    Name: string;
    Title: string;
}

class WMSLayers {
    Layer:WMSLayer[];
}

class WMS_Service {
    Abstract: string;
    AccessConstraints: string;
    ContactInformation: any;
    Fees: string;
    Name: string;
    OnlineResource: any;
    Title: string;
}
