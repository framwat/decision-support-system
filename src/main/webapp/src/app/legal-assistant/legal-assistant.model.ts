
export class LegalAct {
  constructor(id: string, name: string) {
  }
}

export class LegalDocument {
  constructor(
      public id: string,
      public name: string,
      public advice: string,
      public legalInstitutionIds: string[]) {
  }
}

export class LegalInstitution {
  constructor(public id: string, public name: string) {
  }
}

export const legalActs: LegalAct[] = [
  new LegalAct("PL01", "Ustawa  z dnia 20 lipca 2017 r. Prawo wodne"),
  new LegalAct("PL02", "Ustawa z dnia 16 kwietnia 2004 r. o ochronie przyrody"),
  new LegalAct("PL03", "Ustawa z dnia 27 kwietnia 2001 r. Prawo ochrony środowiska"),
  new LegalAct("PL04", "Ustawa z dnia 3 października 2008 r. o udostępnianiu  informacji o środowisku u jego ochronie, udziale społeczeństwa w ochronie środowiska oraz ocenie oddziaływania na środowisko"),
  new LegalAct("PL05", "Ustawa z dnia 8 lipca 2010 r. o szczególnych zasadach przygotowywania do realizacji inwestycji w zakresie budowli przeciwpowodziowych"),
  new LegalAct("PL06", "Ustawa z dnia 3 lutego 1995 r. o ochronie gruntów rolnych i leśnych"),
  new LegalAct("PL07", "Ustawa z dnia 7 lipca 1994 r. Prawo budowlane"),
  new LegalAct("PL08", "Ustawa z dnia 14 czerwca 1960 r. Kodeks Postępowania Administracyjnego"),
  new LegalAct("PL09", "Ustawa z dnia 10 kwietnia 2003 r. o szczególnych zasadach przygotowania i realizacji inwestycji w zakresie dróg publicznych"),
  new LegalAct("PL10", "Ustawa z dnia 28 września 1991 r. o lasach"),
  new LegalAct("PL11", "Rozporządzenie Rady Ministrów z dnia 9 listopada 2010 r. w sprawie przedsięwzięć mogących znacząco oddziaływać na środowisko"),
  new LegalAct("PL12", "Rozporządzenie Rady Ministrów z dnia 10 września 2019 r. w sprawie przedsięwzięć mogących znacząco oddziaływać na środowisko"),
];

export const legalInstitutions: LegalInstitution[] = [
  new LegalInstitution("PL01", "starostwo powiatowe"),
  new LegalInstitution("PL02", "urząd miasta lub gminy"),
  new LegalInstitution("PL03", "urząd wojewódzki"),
  new LegalInstitution("PL04", "urząd marszałkowski"),
  new LegalInstitution("PL05", "wojewoda"),
  new LegalInstitution("PL06", "powiatowy inspektorat nadzoru budowlanego"),
  new LegalInstitution("PL07", "wojewódzki inspektorat nadzoru budowlanego"),
  new LegalInstitution("PL08", "Minister Gospodarki Morskiej i Żeglugi Śródlądowej"),
  new LegalInstitution("PL09", "Generalna Dyrekcja Ochrony Środowiska"),
  new LegalInstitution("PL10", "regionalna dyrekcja ochrony środowiska"),
  new LegalInstitution("PL11", "regionalna dyrekcja lasów państwowych"),
  new LegalInstitution("PL12", "regionalny zarząd gospodarki wodnej"),
  new LegalInstitution("PL13", "zarząd zlewni (właściwy miejscowo Dyrektor Zarządu Zlewni)"),
  new LegalInstitution("PL14", "nadzór wodny (właściwy miejscowo Kierownik Nadzoru Wodnego)"),
  new LegalInstitution("PL15", "właściwy miejscowo RZGW Wód Polskich (dyrektor regionalnego zarządu gospodarki wodnej Wód Polskich)"),
  new LegalInstitution("PL16", "kierownik nadzoru wodnego Wód Polskich"),
  new LegalInstitution("PL17", "osoba uprawniona do sporządzenia raportu oddziaływania przedsięwziącia na środowisko"),
  new LegalInstitution("PL18", "osoba uprawniona do sporządzenia raportu oddziaływania przedsięwziącia na obszar Natura 2000"),
  new LegalInstitution("PL19", "regionalny dyrektor ochrony środowiska"),
  new LegalInstitution("PL20", "dyrektor właściwego urzędu morskiego"),
  new LegalInstitution("PL21", "Minister właściwy do spraw środowiska"),
  new LegalInstitution("PL22", "Generalny Dyrektor Ochrony Środowiska"),
];

export const legalDocuments: LegalDocument[] = [
    new LegalDocument("PL02", "Pozwolenie na budowę", `Pozwolenia na budowę wymagają przedsięwzięcia, które wymagają przeprowadzenia oceny oddziaływania na środowisko oraz przedsięwzięcia wymagające przeprowadzenia oceny oddziaływania na obszar Natura 2000.<br>
Przeprowadzenia oceny oddziaływania przedsięwzięcia na środowisko wymaga realizacja następujących planowanych przedsięwzięć mogących znacząco oddziaływać na środowisko:<br>
1) planowanego przedsięwzięcia mogącego zawsze znacząco oddziaływać na środowisko;<br>
2) planowanego przedsięwzięcia mogącego potencjalnie znacząco oddziaływać na środowisko, jeżeli organ stwierdził obowiązek przeprowadzenia oceny oddziaływania przedsięwzięcia na środowisko.<br>&nbsp;<br>
Do przedsięwzięć mogących zawsze znacząco oddziaływać na środowisko zalicza się m.in. następujące rodzaje przedsięwzięć:<br>
1. zapory lub inne urządzenia przeznaczone do zatrzymywania i stałego retencjonowania (gromadzenia) nie mniej niż 10 mln m3 nowej lub dodatkowej masy wody;<br>
2. urządzenia do przerzutu wody wyłącznie w celu zwiększania zasobów wodnych innych cieków naturalnych, kanałów, jezior oraz innych zbiorników wodnych, w ilościach nie mniejszych niż 100 mln m3 na rok (z wyłączeniem przesyłu wody przeznaczonej do spożycia przez ludzi za pośrednictwem systemów zaopatrzenia w wodę);<br>&nbsp;<br>
Pozwolenia  na budowę nie wymaga m.in. budowa:<br>
1. przepustów o średnicy do 100 cm;<br>
2. obiektów budowlanych piętrzących wodę i upustowych o wysokości piętrzenia poniżej 1 m poza rzekami żeglownymi oraz poza obszarem parków narodowych, rezerwatów przyrody i parków krajobrazowych oraz ich otulin;<br>
3. przydomowych basenów i oczek wodnych o powierzchni do 50 m2;<br>&nbsp;<br>
Pozwolenia na budowę nie wymaga także m.in. wykonywanie robót budowlanych polegających na wykonywaniu i przebudowie urządzeń melioracji wodnych szczegółowych.`,["PL01", "PL03"]),
    new LegalDocument("PL03", "Zgłoszenie budowy lub innych robót budowlanych", `Zgłoszenia budowy lub innych robót budowlanych bez projektu budowlanego wymagają m.in.:<br>
1. budowa lub przebudowa obiektów budowlanych piętrzących wodę i upustowych o wysokości piętrzenia poniżej 1 m poza rzekami żeglownymi oraz poza obszarem parków narodowych, rezerwatów przyrody i parków krajobrazowych oraz ich otulin;<br>
2. wykonywanie i przebudowa urządzeń melioracji wodnych szczegółowych;<br>&nbsp;<br>
Przez zgłoszenie budowy obiektu budowlanego należy również rozumieć jego rozbudowę, nadbudowę, odbudowę.<br>&nbsp;<br>
Zgłoszenie należy złożyć do starostwa albo urzędu miasta na prawach powiatu."`, ["PL01", "PL02", "PL03"]),
    new LegalDocument("PL04", "Pozwolenie na użytkowanie", `"Pozwolenie na użytkowanie wymagane jest, jeśli dla obiektu wydano pozwolenie na budowę oraz zalicza się on m.in. do kategorii XXVIII i XXX – oraz niektórych obiektów budowlanych należących m.in. do kategorii XXIV oraz XXVII. Informacja o tym, czy inwestycja wymaga pozwolenia na użytkowanie powinna znajdować się w pozwoleniu na budowę. Jednakże w przypadku, gdy urząd nie wpisał takiej informacji do pozwolenia na budowę, nie oznacza, że inwestor jest zwolniony z obowiązku uzyskania pozwolenia na użytkowanie – jeśli wynika to z przepisów.<br>
Pozwolenia na użytkowanie wymagają m.in. następujące obiekty budowlane:<br>
• kategoria XXIV – obiekty gospodarki wodnej, jak: zbiorniki wodne i nadpoziomowe;<br>
• kategoria XXVII – budowle hydrotechniczne piętrzące, upustowe i regulacyjne, jak: zapory, progi i stopnie wodne, bramy przeciwpowodziowe, śluzy wałowe, syfony, kanały, śluzy żeglowne (w przypadku zamiaru przystąpienia do użytkowania jazów, wałów przeciwpowodziowych, opasek, ostróg brzegowych i rowów melioracyjnych należy złożyć zawiadomienie o zakończeniu budowy).<br>&nbsp;<br>
W przypadku, gdy pozwolenie na budowę wydał urząd wojewódzki, to wniosek o pozwolenie na użytkowanie należy złożyć do wojewódzkiego inspektoratu nadzoru budowlanego."`, ["PL06", "PL07"]),
    new LegalDocument("PL05", "Zawiadomienie o zakończeniu budowy", ``,["PL06", "PL07"]),
    new LegalDocument("PL06", "Pozwolenie wodnoprawne", `Pozwolenie wodnoprawne (jeżeli ustawa nie stanowi inaczej) jest wymagane na:<br>
        1. prowadzenie przez wody powierzchniowe płynące oraz przez wały przeciwpowodziowe obiektów mostowych, rurociągów, przewodów w rurociągach osłonowych lub przepustów;<br>&nbsp;<br>
        Pozwolenie wodnoprawne jest wymagane również na lokalizowanie na obszarach szczególnego zagrożenia powodzią nowych przedsięwzięć mogących znacząco oddziaływać na środowisko lub nowych obiektów budowlanych.<br>&nbsp;<br>
        Wniosek należy złożyć w siedzibie nadzoru wodnego. Wniosek można również złożyć w siedzibie zarządu zlewni lub regionalnego zarządu gospodarki wodnej, w którym będzie on rozpatrywany.<br>&nbsp;<br>
        Jeśli wnioskodawcą są Wody Polskie to wniosek składa się do Ministra Gospodarki Morskiej i Żeglugi Śródlądowej.`, ["PL08", "PL13", "PL14", "PL15"]),
    new LegalDocument("PL07", "Zgłoszenie wodnoprawne", `Zgłoszenia wodnoprawnego wymaga:<br>
        1. przebudowa rowu polegająca na wykonaniu przepustu lub innego przekroju zamkniętego na długości nie większej niż 10 m;<br>
        2. przebudowa lub odbudowa urządzeń odwadniających zlokalizowanych w pasie drogowym dróg publicznych.<br>&nbsp;<br>
        Zgłoszenie wodnoprawne należy złożyć do nadzoru wodnego (właściwego miejscowo kierownika nadzoru wodnego Wód Polskich).<br>
        Jeśli wnioskodawcą są Wody Polskie to zgłoszenie składa się do Ministra Gospodarki Morskiej i Żeglugi Śródlądowej.`,["PL08", "PL14"]),
    new LegalDocument("PL08", "Decyzja o środowiskowych uwarunkowaniach", `Uzyskanie decyzji o środowiskowych uwarunkowaniach jest wymagane dla planowanych przedsięwzięć mogących zawsze znacząco oddziaływać na środowisko (także w przypadku ich rozbudowy, przebudowy lub montażu zrealizowanego lub realizowanego przedsięwzięcia).<br>&nbsp;<br>
Do przedsięwzięć mogących zawsze znacząco oddziaływać na środowisko zalicza się m.in. następujące rodzaje przedsięwzięć:<br>
1. zapory lub inne urządzenia przeznaczone do zatrzymywania i stałego retencjonowania (gromadzenia) nie mniej niż 10 mln m3 nowej lub dodatkowej masy wody;<br>
2. urządzenia do przerzutu wody wyłącznie w celu zwiększania zasobów wodnych innych cieków naturalnych, kanałów, jezior oraz innych zbiorników wodnych, w ilościach nie mniejszych niż 100 mln m3 na rok (z wyłączeniem przesyłu wody przeznaczonej do spożycia przez ludzi za pośrednictwem systemów zaopatrzenia w wodę);<br>&nbsp<br>
W przypadku, gdy wnioskodawcą jest podmiot prywatny wniosek należy złożyć do urzędu miasta lub gminy. W przypadku, gdy wnioskodawcą jest jednostka samorządu terytorialnego lub podmioty od niej zależne wniosek składa się do regionalnej dyrekcji ochrony środowiska. Urząd, do którego zostanie złożony wniosek przeprowadzi ocenę oddziaływania przedsięwzięcia na środowisko.<br>&nbsp;<br>
Do wniosku dotyczącego przedsięwzięć mogących zawsze znacząco oddziaływać na środowisko należy dołączyć raport o oddziaływaniu przedsięwzięcia na środowisko, sporządzony przez osobę uprawnioną do sporządzenia raportu oddziaływania przedsięwzięcia na środowisko. Można też wystąpić do urzędu o ustalenie zakresu raportu o oddziaływaniu na środowisko - w takim przypadku do wniosku należy dołączyć kartę informacyjną przedsięwzięcia wraz z wnioskiem o ustalenie zakresu raportu.<br>&nbsp;<br>
Do przedsięwzięć mogących potencjalnie znacząco oddziaływać na środowisko zalicza się m.in. następujące rodzaje przedsięwzięć:<br>
1. budowle piętrzące inne niż zapory lub inne urządzenia przeznaczone do zatrzymywania i stałego retencjonowania (gromadzenia) nie mniej niż 10 mln m3 nowej lub dodatkowej masy wody oraz budowle piętrzące o wysokości piętrzenia wody nie mniejszej niż 5 m:<br>
1.1. na obszarach objętych formami ochrony przyrody, o których mowa w art. 6 ust. 1 pkt. 1-5, 8 i 9 ustawy z dnia 16 kwietnia 2004 r. o ochronie przyrody (tj. parkach narodowych, rezerwatach przyrody, parkach krajobrazowych, obszarach chronionego krajobrazu, obszarach Natura 2000, użytkach ekologicznych, zespołach przyrodniczo-krajobrazowych), lub w otulinach form ochrony przyrody, o których mowa w art. 6 ust. 1 pkt 1-3 tej ustawy (tj. parkach narodowych, rezerwatach przyrody, parkach krajobrazowych), z wyłączeniem budowli piętrzących o wysokości piętrzenia wody mniejszej niż 1 m realizowanych na podstawie planu ochrony, planu zadań ochronnych lub zadań ochronnych ustanowionych dla danej formy ochrony przyrody,<br>
1.2. jeżeli piętrzenie dotyczy cieków naturalnych, na których nie ma budowli piętrzących,<br>
1.3. jeżeli w promieniu mniejszym niż 5 km na tym samym cieku lub cieku z nim połączonym znajduje się inna budowla piętrząca,<br>
1.4. o wysokości piętrzenia wody nie mniejszej niż 1 m;<br>
2. kanały – rozumiane jako sztuczne koryta prowadzące wody w sposób ciągły lub okresowy, o szerokości dna co najmniej 1,5 m przy ich ujściu lub ujęciu;<br>&nbsp;<br>
W przypadku, gdy wnioskodawcą jest podmiot prywatny wniosek należy złożyć do urzędu miasta lub gminy. W przypadku, gdy wnioskodawcą jest jednostka samorządu terytorialnego lub podmioty od niej zależne wniosek składa się do regionalnej dyrekcji ochrony środowiska. Urząd, do którego zostanie złożony wniosek zdecyduje czy przeprowadzi ocenę oddziaływania przedsięwzięcia na środowisko. W przypadku, gdy urząd, do którego został złożony wniosek zdecyduje, że przeprowadzi ocenę oddziaływania przedsięwzięcia na środowisko należy złożyć raport o oddziaływaniu przedsięwzięcia na środowisko, sporządzony przez osobę uprawnioną do sporządzenia raportu oddziaływania przedsięwzięcia na środowisko.`, ["PL02", "PL10"]),
    new LegalDocument("PL09", "Decyzja ustalająca warunki prowadzenia robót na terenach cennych przyrodniczo", ``,["PL19"]),
    new LegalDocument("PL10", "Decyzja o pozwoleniu na realizację inwestycji w zakresie budowli przeciwpowodziowych", `W przypadku, gdy inwestycja stanowi budowlę przeciwpowodziową decyzję o pozwoleniu na budowę zastępuje decyzja o pozwoleniu na realizację inwestycji w zakresie budowli przeciwpowodziowych. Przez budowle przeciwpowodziowe rozumie się: kanały ulgi, kierownice w ujściach rzek do morza, poldery przeciwpowodziowe, sztuczne zbiorniki przeciwpowodziowe, suche zbiorniki przeciwpowodziowe, wały przeciwpowodziowe, budowle regulacyjne, wrota przeciwpowodziowe i przeciwsztormowe, falochrony, budowle ochrony brzegów morskich, stopnie wodne - wraz z obiektami związanymi z nimi technicznie i funkcjonalnie lub nieruchomościami przeznaczonymi na potrzeby ochrony przed powodzią.`,["PL05"]),
    new LegalDocument("PL11", "Zezwolenie na usunięcie drzewa lub krzewu", `Przedsiębiorca, spółdzielnia mieszkaniowa, wspólnota mieszkaniowa, osoba fizyczna, której urząd wcześniej odmówił zgody na usunięcie drzewa lub krzewów po dokonaniu zgłoszenia, chcąc dokonać usunięcia drzewa lub krzewu obowiązany jest uzyskać zezwolenie na usunięcie drzewu lub krzewu.<br>
Obwód pnia drzewa należy zmierzyć na wysokości 5 cm od ziemi - jeżeli będzie to mniej niż 50 cm to zezwolenie nie jest potrzebne – niezależnie od gatunku drzewa, który chcesz usunąć. Niektóre gatunki drzew można usunąć bez zezwolenia – nawet gdy na wysokości 5 cm od ziemi mają większy obwód pnia. Są to:<br>
• kasztanowiec zwyczajny, robinia akacjowa oraz platan klonolistny mogą mieć do 65 cm obwodu,<br>
• topola, wierzba, klon jesionolistny oraz klon srebrzysty mogą mieć do 80 cm obwodu.<br>
Chcąc usunąć krzew albo krzewy rosnące w skupisku należy obliczyć powierzchnię, którą one zajmują – w przypadku, gdy wynosi ona mniej niż 25 m2 – zezwolenie na ich usunięcie nie jest potrzebne.<br>
Nie wymaga uzyskiwania zezwolenia na usunięcie drzewa lub krzewów usunięcie m.in.:<br>
• drzew lub krzewów owocowych – chyba, że znajdują się one terenie nieruchomości wpisanej do rejestru zabytków lub na terenach zieleni,<br>
• krzewów na terenach pokrytych roślinnością pełniącą funkcje ozdobne, urządzoną pod względem rozmieszczenia i doboru gatunków posadzonych roślin (z wyjątkiem, gdy krzewy te znajdują się w pasie drogowym drogi publicznej, na terenie nieruchomości wpisanej do rejestru zabytków oraz na terenach zieleni - wtedy należy uzyskać zezwolenie na usunięcie drzewa lub krzewu),<br>
• drzew lub krzewów w celu przywrócenia gruntów nieużytkowanych do użytkowania rolniczego,<br>
• drzew lub krzewów należących do gatunków obcych, np. bożodrzew gruczołowaty,<br>
• drzew lub krzewów na plantacjach lub w lasach – w rozumieniu ustawy z dnia 28 września 1991 r. o lasach,<br>
• drzew lub krzewów z obszaru parku narodowego lub rezerwatu przyrody nieobjętego ochroną krajobrazową,<br>
• drzew lub krzewów złamanych lub przewróconych,<br>
• drzew lub krzewów usuwanych w ramach zadań wynikających z planu ochrony lub zadań ochronnych parku narodowego lub rezerwatu przyrody, planu ochrony parku krajobrazowego, albo planu zadań ochronnych lub planu ochrony dla obszaru Natura 2000.<br>
Ponadto nie wymaga odrębnego zezwolenia na usunięcie drzewa lub krzewu, jeżeli:<br>
• starostwo albo urząd miasta na prawach powiatu nakazał usunięcie drzew lub krzewów z obszarów położonych między linią brzegu a wałem przeciwpowodziowym lub naturalnym wysokim brzegiem, w który wbudowano trasę wału przeciwpowodziowego, z wału przeciwpowodziowego i terenu w odległości mniejszej niż 3 m od stopy wału,<br>
• drzewa lub krzewy usuwane są na podstawie decyzji wydanej w celu utrzymania urządzeń melioracji wodnych szczegółowych (np. rowów, drenów,  rurociągów o średnicy poniżej 0,6 m, stacji pomp do nawodnień ciśnieniowych, ziemnych stawów rybnych, grobli na obszarach nawadnianych, systemów nawodnień grawitacyjnych i ciśnieniowych).`, ["PL02"]),
    new LegalDocument("PL12", "Zgłoszenie zamiaru usunięcia drzewa", `"Właściciel działki, który jest osobą fizyczną, chcąc usunąć drzewo lub krzewy na cele niezwiązane z prowadzeniem działalności gospodarczej obowiązany jest dokonać zgłoszenia do właściwego organu, zamiaru usunięcia drzewa, jeżeli obwód pnia drzewa mierzonego na wysokości 5 cm przekracza:<br>
1) 80 cm - w przypadku topoli, wierzb, klonu jesionolistnego oraz klonu srebrzystego, <br>
2) 65 cm - w przypadku kasztanowca zwyczajnego, robinii akacjowej oraz platanu klonolistnego,<br>
3) 50 cm - w przypadku pozostałych gatunków drzew."`,["PL02"]),
    new LegalDocument("PL13", "Zezwolenie na obszarze parku narodowego na odstępstwa od zakazów", ``,["PL21"]),
    new LegalDocument("PL14", "Zezwolenie na obszarze rezerwatu przyrody na odstępstwa od zakazów", ``,["PL22"]),
    new LegalDocument("PL15", "Zezwolenie na realizację planu lub działań, mogących znacząco negatywnie oddziaływać na cele ochrony obszaru Natura 2000", ``,["PL19", "PL20"]),
];

export enum OwnerType {
  PRIVATE = "prywatny",
  PUBLIC = "publiczny",
}

export class MeasureLegal {
  constructor(
    public id: string,
    public ownerType: OwnerType[],
  ) {
  }
}

export const measuresLegal: MeasureLegal[] = [
  {
    id: "A01",
    ownerType: [OwnerType.PRIVATE]
  },
  {
    id: "A02",
    ownerType: [OwnerType.PUBLIC]
  },
  {
    id: "A03",
    ownerType: [OwnerType.PRIVATE]
  },
  {
    id: "A04",
    ownerType: [OwnerType.PRIVATE]
  },
  {
    id: "A05",
    ownerType: [OwnerType.PRIVATE]
  },
  {
    id: "A06",
    ownerType: [OwnerType.PRIVATE]
  },
  {
    id: "A07",
    ownerType: [OwnerType.PRIVATE]
  },
  {
    id: "A08",
    ownerType: [OwnerType.PRIVATE]
  },
  {
    id: "A09",
    ownerType: [OwnerType.PRIVATE]
  },
  {
    id: "A10",
    ownerType: [OwnerType.PRIVATE]
  },
  {
    id: "A11",
    ownerType: [OwnerType.PRIVATE]
  },
  {
    id: "A12",
    ownerType: [OwnerType.PRIVATE]
  },
  {
    id: "A13",
    ownerType: [OwnerType.PRIVATE]
  },
  {
    id: "A14",
    ownerType: [OwnerType.PRIVATE]
  },
  {
    id: "A15",
    ownerType: [OwnerType.PRIVATE]
  },
  {
    id: "F01",
    ownerType: [OwnerType.PUBLIC]
  },
  {
    id: "F02",
    ownerType: [OwnerType.PUBLIC]
  },
  {
    id: "F03",
    ownerType: [OwnerType.PRIVATE, OwnerType.PUBLIC]
  },
  {
    id: "F04",
    ownerType: [OwnerType.PRIVATE, OwnerType.PUBLIC]
  },
  {
    id: "F05",
    ownerType: [OwnerType.PRIVATE, OwnerType.PUBLIC]
  },
  {
    id: "F06",
    ownerType: [OwnerType.PUBLIC]
  },
  {
    id: "F07",
    ownerType: [OwnerType.PUBLIC]
  },
  {
    id: "F08",
    ownerType: [OwnerType.PUBLIC]
  },
  {
    id: "F09",
    ownerType: [OwnerType.PUBLIC]
  },
  {
    id: "F10",
    ownerType: [OwnerType.PUBLIC]
  },
  {
    id: "F13",
    ownerType: [OwnerType.PUBLIC]
  },
  {
    id: "F14",
    ownerType: [OwnerType.PUBLIC]
  },
  {
    id: "N01",
    ownerType: [OwnerType.PRIVATE, OwnerType.PUBLIC]
  },
  {
    id: "N02",
    ownerType: [OwnerType.PRIVATE, OwnerType.PUBLIC]
  },
  {
    id: "N03",
    ownerType: [OwnerType.PRIVATE, OwnerType.PUBLIC]
  },
  {
    id: "N04",
    ownerType: [OwnerType.PRIVATE, OwnerType.PUBLIC]
  },
  {
    id: "N05",
    ownerType: [OwnerType.PUBLIC]
  },
  {
    id: "N06",
    ownerType: [OwnerType.PUBLIC]
  },
  {
    id: "N07",
    ownerType: [OwnerType.PUBLIC]
  },
  {
    id: "N08",
    ownerType: [OwnerType.PUBLIC]
  },
  {
    id: "N09",
    ownerType: [OwnerType.PUBLIC]
  },
  {
    id: "N10",
    ownerType: [OwnerType.PUBLIC]
  },
  {
    id: "N11",
    ownerType: [OwnerType.PUBLIC]
  },
  {
    id: "N12",
    ownerType: [OwnerType.PUBLIC]
  },
  {
    id: "N13",
    ownerType: [OwnerType.PRIVATE, OwnerType.PUBLIC]
  },
  {
    id: "N14",
    ownerType: [OwnerType.PUBLIC]
  },
  {
    id: "D01",
    ownerType: [OwnerType.PRIVATE, OwnerType.PUBLIC]
  },
  {
    id: "D02",
    ownerType: [OwnerType.PRIVATE, OwnerType.PUBLIC]
  },
  {
    id: "D03",
    ownerType: [OwnerType.PRIVATE, OwnerType.PUBLIC]
  },
  {
    id: "D04",
    ownerType: [OwnerType.PRIVATE, OwnerType.PUBLIC]
  },
  {
    id: "D05",
    ownerType: [OwnerType.PRIVATE, OwnerType.PUBLIC]
  },
  {
    id: "D06",
    ownerType: [OwnerType.PRIVATE, OwnerType.PUBLIC]
  },
  {
    id: "T01",
    ownerType: [OwnerType.PRIVATE, OwnerType.PUBLIC]
  },
  {
    id: "T02",
    ownerType: [OwnerType.PUBLIC]
  },
  {
    id: "T03",
    ownerType: [OwnerType.PUBLIC]
  },
];

export const LegalSuggestion = {
    SUG01: `W przypadku przedsięwzięć realizowanych na obszarach cennych i chronionych (tj. parkach narodowych, rezerwatach przyrody, parkach krajobrazowych, obszarach chronionego krajobrazu, obszarach Natura 2000, stanowiskach dokumentacyjnych, użytkach ekologicznych, zespołach przyrodniczo-krajobrazowych), w obrębach ochronnych wyznaczonych na podstawie ustawy o rybactwie śródlądowym, a także w obrębie cieków naturalnych, konieczne jest zgłoszenie ich regionalnemu dyrektorowi ochrony środowiska. W szczególności dotyczy to działań polegających na utrzymywaniu wód, melioracjach wodnych czy robót ziemnych mogących zmieniać stosunki wodne lub wodno-glebowe.<br>&nbsp;<br>
    Regionalny dyrektor ochrony środowiska może nałożyć obowiązek uzyskania decyzji ustalającej warunki prowadzenia robót, jeżeli planowane działania mogą potencjalnie naruszać przepisy dotyczące form ochrony przyrody, obrębów ochronnych i/lub spowodować pogorszenie stanu środowiska naturalnego, w tym znacząco negatywnie oddziaływać na cele ochrony obszarów chronionych, siedliska przyrodnicze, chronione gatunki roślin, grzybów, zwierząt lub ich siedliska.<br>&nbsp;<br>
    Jeśli zgłoszone działania będą potencjalnie znacząco oddziaływać na obszar Natura 2000, wówczas regionalny dyrektor ochrony nałoży obowiązek przeprowadzania oceny oddziaływania, a tym samym nałoży obowiązek przedłożenia raportu o oddziaływaniu przedsięwzięcia na obszar Natura 2000. Ocena winna być przeprowadzona przed wydaniem decyzji ustalającej warunki prowadzenia robót.`,
    SUG02: `W parkach narodowych m.in. zabrania się:<br>
        1. budowy lub przebudowy obiektów budowlanych i urządzeń technicznych, z wyjątkiem obiektów i urządzeń służących celom parku narodowego albo rezerwatu przyrody;<br>
        2. zmiany stosunków wodnych, regulacji rzek i potoków, jeżeli zmiany te nie służą ochronie przyrody;<br>
        3. niszczenia gleby lub zmiany przeznaczenia i użytkowania gruntów;<br>
        4. wykonywania prac ziemnych trwale zniekształcających rzeźbę terenu.<br>&nbsp;<br>
        Minister właściwy do spraw środowiska, po zasięgnięciu opinii dyrektora parku narodowego, może zezwolić na obszarze parku narodowego na odstępstwa od zakazów, jeżeli jest to uzasadnione:<br>
        1. potrzebą ochrony przyrody, wykonywaniem badań naukowych, celami edukacyjnymi, kulturowymi, turystycznymi, rekreacyjnymi lub sportowymi lub celami kultu religijnego i nie spowoduje to negatywnego oddziaływania na przyrodę parku narodowego lub<br>
        2. potrzebą realizacji inwestycji liniowych celu publicznego, w przypadku braku rozwiązań alternatywnych i po zagwarantowaniu kompensacji przyrodniczej w rozumieniu art. 3 pkt 8 ustawy z dnia 27 kwietnia 2001 r. - Prawo ochrony środowiska.`,
    SUG03: `W rezerwatach przyrody m.in. zabrania się:<br>
        1. budowy lub przebudowy obiektów budowlanych i urządzeń technicznych, z wyjątkiem obiektów i urządzeń służących celom parku narodowego albo rezerwatu przyrody;<br>
        2. zmiany stosunków wodnych, regulacji rzek i potoków, jeżeli zmiany te nie służą ochronie przyrody;<br>
        3. niszczenia gleby lub zmiany przeznaczenia i użytkowania gruntów;<br>
        4. wykonywania prac ziemnych trwale zniekształcających rzeźbę terenu.<br>&nbsp;<br>
        Generalny Dyrektor Ochrony Środowiska, po zasięgnięciu opinii regionalnego dyrektora ochrony środowiska, może zezwolić na obszarze rezerwatu przyrody na odstępstwa od zakazów, jeżeli jest to uzasadnione potrzebą:<br>
        1. ochrony przyrody lub<br>
        2. realizacji inwestycji liniowych celu publicznego, w przypadku braku rozwiązań alternatywnych i po zagwarantowaniu kompensacji przyrodniczej w rozumieniu art. 3 pkt 8 ustawy z dnia 27 kwietnia 2001 r. - Prawo ochrony środowiska.`,
    SUG04: `W parku krajobrazowym mogą być wprowadzone m.in. następujące zakazy:<br>
        1. realizacji przedsięwzięć mogących znacząco oddziaływać na środowisko w rozumieniu przepisów ustawy z dnia 3 października 2008 r. o udostępnianiu informacji o środowisku i jego ochronie, udziale społeczeństwa w ochronie środowiska oraz o ocenach oddziaływania na środowisko (nie dotyczy realizacji przedsięwzięć mogących znacząco oddziaływać na środowisko, dla których sporządzenie raportu o oddziaływaniu na środowisko nie jest obowiązkowe i przeprowadzona procedura oceny oddziaływania na środowisko wykazała brak niekorzystnego wpływu na przyrodę i krajobraz parku krajobrazowego);<br>
        2. likwidowania i niszczenia zadrzewień śródpolnych, przydrożnych i nadwodnych, jeżeli nie wynikają z potrzeby ochrony przeciwpowodziowej lub zapewnienia bezpieczeństwa ruchu drogowego lub wodnego lub budowy, odbudowy, utrzymania, remontów lub naprawy urządzeń wodnych;<br>
        3. wykonywania prac ziemnych trwale zniekształcających rzeźbę terenu, z wyjątkiem prac związanych z zabezpieczeniem przeciwsztormowym, przeciwpowodziowym lub przeciwosuwiskowym lub budową, odbudową, utrzymaniem, remontem lub naprawą urządzeń wodnych;<br>
        4. dokonywania zmian stosunków wodnych, jeżeli zmiany te nie służą ochronie przyrody lub racjonalnej gospodarce rolnej, leśnej, wodnej lub rybackiej;<br>
        5. budowania nowych obiektów budowlanych w pasie szerokości 100 m od:<br>
        a) linii brzegów rzek, jezior i innych naturalnych zbiorników wodnych,<br>
        b) zasięgu lustra wody w sztucznych zbiornikach wodnych usytuowanych na wodach płynących przy normalnym poziomie piętrzenia określonym w pozwoleniu wodnoprawnym;<br>
        6. likwidowania, zasypywania i przekształcania zbiorników wodnych, starorzeczy oraz obszarów wodno-błotnych.`,
    SUG05: `Na obszarze chronionego krajobrazu mogą być wprowadzone m.in. następujące zakazy:<br>
        1. realizacji przedsięwzięć mogących znacząco oddziaływać na środowisko w rozumieniu przepisów ustawy z dnia 3 października 2008 r. o udostępnianiu informacji o środowisku i jego ochronie, udziale społeczeństwa w ochronie środowiska oraz o ocenach oddziaływania na środowisko (nie dotyczy realizacji przedsięwzięć mogących znacząco oddziaływać na środowisko, dla których przeprowadzona ocena oddziaływania na środowisko wykazała brak negatywnego wpływu na ochronę przyrody i ochronę krajobrazu obszaru chronionego krajobrazu);<br>
        2. likwidowania i niszczenia zadrzewień śródpolnych, przydrożnych i nadwodnych, jeżeli nie wynikają one z potrzeby ochrony przeciwpowodziowej i zapewnienia bezpieczeństwa ruchu drogowego lub wodnego lub budowy, odbudowy, utrzymania, remontów lub naprawy urządzeń wodnych;<br>
        3. wykonywania prac ziemnych trwale zniekształcających rzeźbę terenu, z wyjątkiem prac związanych z zabezpieczeniem przeciwsztormowym, przeciwpowodziowym lub przeciwosuwiskowym lub utrzymaniem, budową, odbudową, naprawą lub remontem urządzeń wodnych;<br>
        4. dokonywania zmian stosunków wodnych, jeżeli służą innym celom niż ochrona przyrody lub zrównoważone wykorzystanie użytków rolnych i leśnych oraz racjonalna gospodarka wodna lub rybacka;<br>
        5. likwidowania naturalnych zbiorników wodnych, starorzeczy i obszarów wodno-błotnych;<br>
        6. budowania nowych obiektów budowlanych w pasie szerokości 100 m od:<br>
        a) linii brzegów rzek, jezior i innych naturalnych zbiorników wodnych,<br>
        b) zasięgu lustra wody w sztucznych zbiornikach wodnych usytuowanych na wodach płynących przy normalnym poziomie piętrzenia określonym w pozwoleniu wodnoprawnym;<br>
        7. lokalizowania obiektów budowlanych w pasie szerokości 200 m od linii brzegów klifowych oraz w pasie technicznym brzegu morskiego.`,
    SUG06: `Zabrania się podejmowania działań mogących, osobno lub w połączeniu z innymi działaniami, znacząco negatywnie oddziaływać na cele ochrony obszaru Natura 2000, w tym w szczególności:<br>
        1. pogorszyć stan siedlisk przyrodniczych lub siedlisk gatunków roślin i zwierząt, dla których ochrony wyznaczono obszar Natura 2000 lub<br>
        2. wpłynąć negatywnie na gatunki, dla których ochrony został wyznaczony obszar Natura 2000, lub<br>
        3. pogorszyć integralność obszaru Natura 2000 lub jego powiązania z innymi obszarami.<br>&nbsp;<br>
        Jeżeli przemawiają za tym konieczne wymogi nadrzędnego interesu publicznego, w tym wymogi o charakterze społecznym lub gospodarczym, i wobec braku rozwiązań alternatywnych, właściwy miejscowo regionalny dyrektor ochrony środowiska, a na obszarach morskich - dyrektor właściwego urzędu morskiego, może zezwolić na realizację planu lub działań, mogących znacząco negatywnie oddziaływać na cele ochrony obszaru Natura 2000, zapewniając wykonanie kompensacji przyrodniczej niezbędnej do zapewnienia spójności i właściwego funkcjonowania sieci obszarów Natura 2000.`,
    SUG07: `W stosunku do pomnika przyrody, stanowiska dokumentacyjnego, użytku ekologicznego lub zespołu przyrodniczo-krajobrazowego mogą być wprowadzone m.in. następujące zakazy:<br>
        1. niszczenia, uszkadzania lub przekształcania obiektu lub obszaru;<br>
        2. wykonywania prac ziemnych trwale zniekształcających rzeźbę terenu, z wyjątkiem prac związanych z zabezpieczeniem przeciwsztormowym lub przeciwpowodziowym albo budową, odbudową, utrzymywaniem, remontem lub naprawą urządzeń wodnych;<br>
        3. uszkadzania i zanieczyszczania gleby;<br>
        4. dokonywania zmian stosunków wodnych, jeżeli zmiany te nie służą ochronie przyrody albo racjonalnej gospodarce rolnej, leśnej, wodnej lub rybackiej;<br>
        5. likwidowania, zasypywania i przekształcania naturalnych zbiorników wodnych, starorzeczy oraz obszarów wodno-błotnych;<br>
        6. zmiany sposobu użytkowania ziemi.<br>&nbsp;<br>
        Powyższe zakazy nie dotyczą:<br>
        1. prac wykonywanych na potrzeby ochrony przyrody po uzgodnieniu z organem ustanawiającym daną formę ochrony przyrody;<br>
        2. realizacji inwestycji celu publicznego w przypadku braku rozwiązań alternatywnych, po uzgodnieniu z organem ustanawiającym daną formę ochrony przyrody.`,
    SUG08: `Uzyskanie decyzji o środowiskowych uwarunkowaniach jest wymagane dla planowanych przedsięwzięć mogących potencjalnie znacząco oddziaływać na środowisko. Do przedsięwzięć mogących potencjalnie znacząco oddziaływać na środowisko zalicza się m.in. następujące rodzaje przedsięwzięć:<br>
        1) zalesienia:<br>
        - pastwisk lub łąk na obszarach narażonych na niebezpieczeństwo powodzi w rozumieniu art. 16 pkt 33 ustawy z dnia 20 lipca 2017 r. - Prawo wodne, a jeżeli została sporządzona mapa zagrożenia powodziowego - na obszarach, o których mowa w art. 169 ust. 2 pkt 2 i 3 ustawy z dnia 20 lipca 2017 r. - Prawo wodne,<br>
        - nieużytków na glebach bagiennych,<br>
        - nieużytków lub innych niż orne użytków rolnych, znajdujących się na obszarach objętych formami ochrony przyrody, o których mowa w art. 6 ust. 1 pkt 1-5, 8 i 9 ustawy z dnia 16 kwietnia 2004 r. o ochronie przyrody, lub w otulinach form ochrony przyrody, o których mowa w art. 6 ust. 1 pkt 1-3 tej ustawy;<br>
        2) zalesienia o powierzchni powyżej 20 ha inne niż wymienione powyżej.<br>&nbsp;<br>
        W przypadku, gdy wnioskodawcą jest podmiot prywatny wniosek należy złożyć do urzędu miasta lub gminy.<br>&nbsp;<br>
        Urząd, do którego zostanie złożony wniosek zdecyduje czy przeprowadzi ocenę oddziaływania przedsięwzięcia na środowisko.<br>&nbsp;<br>
        W przypadku, gdy urząd, do którego został złożony wniosek zdecyduje, że przeprowadzi ocenę oddziaływania przedsięwzięcia na środowisko należy złożyć raport o oddziaływaniu przedsięwzięcia na środowisko, sporządzony przez osobę uprawnioną do sporządzenia raportu oddziaływania przedsięwzięcia na środowisko.`,

};
