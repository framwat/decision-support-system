import {Component, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {Measure, measures, Sector} from "./../measures-catalog/measures-catalog.model";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {
    LegalDocument,
    legalDocuments,
    LegalInstitution, legalInstitutions,
    LegalSuggestion,
    measuresLegal,
    OwnerType
} from "./legal-assistant.model";


@Component({
  templateUrl: './legal-assistant.component.html',
  styleUrls: ['./legal-assistant.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class LegalAssistantComponent implements OnInit {
    measures: Measure[];
    filteredSectors: string[] = [];
    filteredMeasureBySector: any = {};
    measureTypes: any;
    sectors: String[];
    form: FormGroup;
    formBuilder = new FormBuilder();
    hasOwnerPrivate = false;
    hasOwnerPublic = false;

    selectedDocuments: LegalDocument[];
    suggestions: string[];
    permission: Permission;

    constructor() {
        let sector;
        let measureTypes = {};
        this.sectors = Object.values(Sector);

        for (sector of this.sectors) {
            measureTypes[sector] = [];
        }

        for (let measure of measures) {
            measureTypes[measure.sector].push(measure.id);
        }

        this.measureTypes = measureTypes;

        this.form = this.formBuilder.group({
            measure: ['', Validators.required],
            owner: ['', Validators.required],
            protectedArea: ['', Validators.required],
        });

        this.form.valueChanges.subscribe(
            values => {
                let measure = this.form.get('measure').value;

                if (measure) {
                    for (let measureLegal of measuresLegal) {
                        if (measureLegal.id == measure) {
                            this.hasOwnerPrivate = measureLegal.ownerType.includes(OwnerType.PRIVATE);
                            this.hasOwnerPublic = measureLegal.ownerType.includes(OwnerType.PUBLIC);

                            break;
                        }
                    }
                    console.log('selected measure', measure);
                }

                if (!this.form.valid) {
                    this.selectedDocuments = null;
                    this.permission = null;
                    this.suggestions = [];

                    return;
                }

                let response = this.getAdvice(values);

                if (!response) {
                    return;
                }

                let suggestions: string[] = [];

                for (let id of response.suggestionIds) {
                    suggestions.push(LegalSuggestion[id]);
                }

                this.suggestions = suggestions;

                let documents: LegalDocument[] = [];

                if (response.permission == Permission.YES) {
                    for (let id of response.requiredDocumentIds) {
                        documents.push(this.getLegalDocumentById(id));
                    }
                }

                this.selectedDocuments = documents;

                this.permission = response.permission;
            }
        );
    }

    ngOnInit() {
        this.measures = measures;
    }

    getAdvice(query: LegalQuery): LegalResponse {
        if (query.protectedArea != "niechroniony") {
            if (query.protectedArea == "park narodowy" && query.measure != "D03") {
                return new LegalResponse(["PL13"], Permission.YES, ["SUG02"]);
            }

            if (query.protectedArea == "tereny cenne przyrodniczo (ogólnie)") {
                return new LegalResponse( ["PL09"], Permission.YES, ["SUG01"]);
            }

            if (query.protectedArea  == "rezerwat przyrody") {
                return new LegalResponse(["PL14"], Permission.YES, ["SUG03"]);
            }

            if (query.protectedArea  == "park krajobrazowy") {
                return new LegalResponse(null, Permission.NO, ["SUG04"]);
            }

            if (query.protectedArea  == "obszar chronionego krajobrazu") {
                return new LegalResponse(null, Permission.NO, ["SUG05"]);
            }

            if (query.protectedArea  == "natura 2000") {
                return new LegalResponse(["PL15"], Permission.YES, ["SUG06"]);
            }

            if (query.protectedArea  == "pomnik przyrody") {
                if (query.measure == "F05" && query.owner == "prywatny") {
                    return new LegalResponse(null, Permission.NA, ["SUG07"]);
                }

                return new LegalResponse(null, Permission.NO, ["SUG07"]);
            }

        } else {
            if (query.measure.startsWith("A") || ["F01", "F02", "F03", "F04", "F06", "F07", "F10", "F14"].includes(query.measure)) {
                return new LegalResponse([], Permission.YES, []);
            }

            if (query.measure == "F05") {
                return new LegalResponse(["PL08"], Permission.YES, ["SUG08"]);
            }

            if (query.measure == "F08") {
                return new LegalResponse(["PL06", "PL07"], Permission.YES, []);
            }

            if (query.measure == "F09") {
                return new LegalResponse(["PL04", "PL02", "PL06", "PL07", "PL08", "PL11"], Permission.YES, []);
            }

            if (query.measure == "F13") {
                return new LegalResponse(["PL04", "PL02", "PL03", "PL06", "PL07", "PL08", "PL11"], Permission.YES, []);
            }

            if (["N01", "N02", "N09", "N13", "D01", "D02", "D04", "D05", "D07", "T01", "T02", "T03"].includes(query.measure)) {
                if (query.owner == "prywatny") {
                    return new LegalResponse(["PL04", "PL10", "PL02", "PL03", "PL06", "PL07", "PL08", "PL11", "PL12"], Permission.YES, []);
                } else {
                    return new LegalResponse(["PL04", "PL10", "PL02", "PL03", "PL06", "PL07", "PL08", "PL11"], Permission.YES, []);
                }
            }

            if (["N03", "N04", "N05", "N06", "N07", "N08", "N12", "N14"].includes(query.measure)) {
                if (query.owner == "prywatny") {
                    return new LegalResponse(["PL04", "PL10", "PL03", "PL06", "PL07", "PL11", "PL12"], Permission.YES, []);
                } else {
                    return new LegalResponse(["PL04", "PL10", "PL03", "PL06", "PL07", "PL11"], Permission.YES, []);
                }
            }

            if (["N10", "N11"].includes(query.measure)) {
                return new LegalResponse(["PL04", "PL10", "PL03", "PL06", "PL07", "PL08", "PL11"], Permission.YES, []);
            }

            if (query.measure == "D03") {
                return new LegalResponse([], Permission.NA,[]);
            }

        }
    }

    private getLegalDocumentById(id: string): LegalDocument {
        return legalDocuments.filter(document => document.id == id).shift();
    }

    private getLegalInstitutionById(id: string): LegalInstitution {
        return legalInstitutions.filter(institution => institution.id == id).shift();
    }
}

export class LegalQuery {
    constructor(
        public measure: string,
        public owner: string,
        public protectedArea: string,
    ) {
    }
}

export class LegalResponse {
    constructor(
        public requiredDocumentIds: string[] | null,
        public permission: Permission,
        public suggestionIds: string[],
    ) {
    }
}

export enum Permission {
    YES, NO, NA
}
