import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {TranslateService} from "@ngx-translate/core";
import {MatDialog} from "@angular/material/dialog";
import {DialogComponent} from "./shared/dialog.component";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class AppComponent implements OnInit {
    languages:any;
    footerBelow:Boolean = false;

    constructor(
        public translate: TranslateService,
        private dialog: MatDialog,
    ) {
        translate.addLangs(['de', 'en', 'hr', 'hu', 'pl', 'sk', 'sl']);
        translate.setDefaultLang('en');

        if (localStorage.getItem("language") == null) {
            localStorage.setItem("language", translate.getBrowserLang());
        }

        const selectedLanguage = localStorage.getItem("language");

        this.languages = {
            de: "Deutsch",
            en: "English",
            hr: "Hrvatski",
            hu: "Magyar",
            pl: "Polski",
            sk: "Slovenčina",
            sl: "Slovenščina",
        }

        translate.use(selectedLanguage.match(/de|en|pl|hr|hu|pl|sl|sk/) ? selectedLanguage : 'en');
    }

    ngOnInit() {
        if (!localStorage.getItem("termsOfUse")) {
            this.dialog.open(DialogComponent,
                {
                    data: {
                        title: `Warunki korzystania z serwisu`,
                        content: `
                                 Udostępnione w serwisie planuj.retencjawod.sggw.pl dane mają charakter demonstracyjny i nie mogą być wykorzystywane do pracy komercyjnych ani być materiałem dowodowym w sprawach sądowych.<br>&nbsp;<br>Akceptacja warunków dotyczy również <a href="/#/about/terms-of-use" target="_blank">warunków korzystania</a>.
                                  `,
                        doneFunction: this.acceptConditions,
                    },
                    height: '320px',
                    width: '600px',
                });
        }
    }

    acceptConditions():void {
        localStorage.setItem("termsOfUse", "true");
    }

    changeLanguage(lang: string): void {
        localStorage.setItem("language", lang);

        this.translate.use(lang);
    }

    public setFooterBelow() {
        this.footerBelow = true;
    }
}
