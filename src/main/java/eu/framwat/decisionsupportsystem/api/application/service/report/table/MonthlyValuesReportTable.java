package eu.framwat.decisionsupportsystem.api.application.service.report.table;

import be.quodlibet.boxable.page.PageProvider;
import eu.framwat.decisionsupportsystem.api.application.model.Measure;
import eu.framwat.decisionsupportsystem.api.application.service.report.ReportTable;
import lombok.EqualsAndHashCode;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.font.PDType0Font;

import java.io.IOException;
import java.util.*;

@EqualsAndHashCode(callSuper = true)
public class MonthlyValuesReportTable extends ReportTable {
    private final static List<String> headers = Arrays.asList("nswrm_name_column", "nswrm_type_column", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX", "X", "XI", "XII", "annual_column");
    private final static List<Float> colWidths = Arrays.asList(17f, 17f, 5f, 5f, 5f, 5f, 5f, 5f, 5f, 5f, 5f, 5f, 5f, 5f, 6f);

    public MonthlyValuesReportTable(ResourceBundle resourceBundle, float yStart, PDDocument document, PDPage page, PDType0Font font, PageProvider pageProvider) throws IOException {
        super(resourceBundle, yStart, document, page, font, pageProvider);
    }

    public void setData(Map<String, Measure> measures, Map<String, Collection<Double>> monthlyValues, boolean isSum) throws IOException {
        List<List<String>> data = new ArrayList<>();

        data.add(new ArrayList<>(headers));

        for (Map.Entry<String, Measure> measure : measures.entrySet()) {
            Collection<Double> values = monthlyValues.get(measure.getKey());

            List<String> row = new ArrayList<>();
            row.add(measure.getValue().getName());
            row.add(measure.getValue().getType());

            Double yearly = 0.;

            for (Double value : values) {
                if (value == null) {
                    row.add("");

                    continue;
                }

                yearly += value;
                row.add(String.format("%.1f", value));
            }

            row.add(String.format("%.1f", isSum ? yearly : yearly / 12));
            data.add(row);
        }

        this.prepareData(data, colWidths);
    }
}
