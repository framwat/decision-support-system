package eu.framwat.decisionsupportsystem.api.application.model;

public interface ZonalStatisticView {
    Integer getCount();
    Double getSum();
    Double getMean();
    Double getStddev();
    Double getMin();
    Double getMax();
}
