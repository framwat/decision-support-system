package eu.framwat.decisionsupportsystem.api.application.model;

public interface ProtectedSitePointView {
    String getProtectionClass();

    String getLocalName();
}
