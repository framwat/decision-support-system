package eu.framwat.decisionsupportsystem.api.application.repository;

import eu.framwat.decisionsupportsystem.api.application.model.MonthlyRunOff;
import eu.framwat.decisionsupportsystem.api.application.model.ZonalStatisticView;
import org.locationtech.jts.geom.Geometry;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface MonthlyRunOffRepository extends JpaRepository<MonthlyRunOff, Integer> {
    String TABLE_NAME = "monthrunoff";

    @Query(value = "SELECT (ST_SummaryStatsAgg(intersection.rast, 1, TRUE)).*" +
            "         FROM (" +
            "                  SELECT ST_Intersection(\"" + TABLE_NAME + "\".\"rast\", ?, " +
            "                                         ST_AsRaster(" +
            "                                                 ST_Transform(" +
            "                                                         ST_SetSRID(?, 4326)," +
            "                                                         3857)," +
            "                                                 \"" + TABLE_NAME + "\".\"rast\"), 1" +
            "                             ) AS rast" +
            "                  FROM " + TABLE_NAME +
            "              ) as intersection", nativeQuery = true)
    ZonalStatisticView getZonalStatistic(Integer band, Geometry g);

    @Query(value = "SELECT zone.val FROM (SELECT ST_Value(" +
            TABLE_NAME + ".rast," +
            "?," +
            "ST_Centroid(ST_Transform(ST_SetSRID(?, 4326), 3857))) AS val FROM " + TABLE_NAME + ") AS zone WHERE val IS NOT NULL", nativeQuery = true)
    Double getValueForGeometryCentroid(Integer band, Geometry g);
}
