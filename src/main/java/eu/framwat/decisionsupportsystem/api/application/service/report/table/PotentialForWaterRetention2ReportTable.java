package eu.framwat.decisionsupportsystem.api.application.service.report.table;

import be.quodlibet.boxable.page.PageProvider;
import eu.framwat.decisionsupportsystem.api.application.model.Measure;
import eu.framwat.decisionsupportsystem.api.application.model.PotentialForWaterRetention;
import eu.framwat.decisionsupportsystem.api.application.service.report.ReportTable;
import lombok.EqualsAndHashCode;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.font.PDType0Font;

import java.io.IOException;
import java.util.*;

@EqualsAndHashCode(callSuper = true)
public class PotentialForWaterRetention2ReportTable extends ReportTable {
    private final static List<String> headers = Arrays.asList(
            "nswrm_name_column",
            "nswrm_type_column",
            "MeanderRatio",
            "NonForestedRatio",
            "Pre_Var_a",
            "Pre_Var_m",
            "PrecFreqLow75",
            "ReclaimedRatio",
            "SRI",
            "SWR",
            "UrbanRatio",
            "WetlandRatio",
            "ArableRatio",
            "BFI",
            "CWB",
            "EcoAreaRatio",
            "EcoNumRatio",
            "SoilErodibility"
    );
    private final static List<Float> colWidths = Arrays.asList(
            20f, 32f,
            3f, 3f, 3f, 3f, 3f, 3f, 3f, 3f,
            3f, 3f, 3f, 3f, 3f, 3f, 3f, 3f
    );

    public PotentialForWaterRetention2ReportTable(ResourceBundle resourceBundle, float yStart, PDDocument document, PDPage page, PDType0Font font, PageProvider pageProvider) throws IOException {
        super(resourceBundle, yStart, document, page, font, pageProvider);
    }

    public void setData(Map<String, Measure> measures, Map<String, Collection<PotentialForWaterRetention>> potentialCollection) throws IOException {
        List<List<String>> data = new ArrayList<>();

        data.add(new ArrayList<>(headers));

        for (Map.Entry<String, Measure> measure : measures.entrySet()) {
            for (PotentialForWaterRetention potential : potentialCollection.get(measure.getKey())) {
                data.add(new ArrayList<>(
                        Arrays.asList(
                                measure.getValue().getName(),
                                measure.getValue().getType(),
                                formatField(potential.getCnonforestedratio()),
                                formatField(potential.getCpre_var_a()),
                                formatField(potential.getCpre_var_m()),
                                formatField(potential.getCprecfreqlow75()),
                                formatField(potential.getCreclaimedratio()),
                                formatField(potential.getCsri()),
                                formatField(potential.getCswr()),
                                formatField(potential.getCurbanratio()),
                                formatField(potential.getCwetlandratio()),
                                formatField(potential.getCarableratio()),
                                formatField(potential.getCbfi()),
                                formatField(potential.getCcwb()),
                                formatField(potential.getCarableratio()),
                                formatField(potential.getCeconumratio()),
                                formatField(potential.getCsoilerodibility()),
                                formatField(potential.getCmeanderratio())
                        )));
            }
        }

        this.prepareData(data, colWidths);
    }

    private String formatField(Double value) {
        return value == null ? "" : String.format("%.0f", value);
    }
}
