package eu.framwat.decisionsupportsystem.api.application.repository;

import eu.framwat.decisionsupportsystem.api.application.model.AdministrationUnit;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.Point;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Optional;

@Repository
public interface AdministrationUnitsRepository extends JpaRepository<AdministrationUnit, Integer> {
    @Query(value = "SELECT a FROM AdministrationUnit AS a WHERE ST_Intersects(a.geometry, ST_Transform(ST_SetSRID(?1, 4326), 3857)) = true")
    Collection<AdministrationUnit> findByGeomIntersection(Geometry geometry);

    @Query(value = "SELECT a FROM AdministrationUnit AS a WHERE ST_Within(ST_Transform(ST_SetSRID(?1, 4326), 3857), a.geometry) = true")
    Optional<AdministrationUnit> findByPoint(Point point);
}
