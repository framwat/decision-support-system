package eu.framwat.decisionsupportsystem.api.application.controller;

import lombok.Builder;
import lombok.Getter;

import java.util.UUID;

@Builder
@Getter
public class ReportStatusResponse {
    private UUID uuid;
    private String status;
}
