package eu.framwat.decisionsupportsystem.api.application.service;

import eu.framwat.decisionsupportsystem.api.application.model.*;
import eu.framwat.decisionsupportsystem.api.application.repository.*;
import eu.framwat.decisionsupportsystem.api.application.service.report.Report;
import eu.framwat.decisionsupportsystem.api.application.service.report.ReportData;
import eu.framwat.decisionsupportsystem.api.application.service.report.ReportNotFoundException;
import eu.framwat.decisionsupportsystem.api.application.service.report.ReportNotReadyException;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.time.Duration;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.*;

@Service
@RequiredArgsConstructor
@Slf4j
public class ReportGenerator {

    public static final String REPORT_DIRECTORY = "tmp/reports";
    public static final Integer REPORT_TIME_OUT = 10*60;

    private final AdministrationUnitsRepository admUnitsRepository;

    private final LanduseRepository landuseRepository;

    private final ParcelRepository parcelRepository;

    private final PotentialWaterRetentionRepository potentialWaterRetentionRepository;

    private final ProtectedSitesAreasRepository protectedSitesAreasRepository;

    private final ProtectedSitesPointsRepository protectedSitesPointsRepository;

    private final MonthlyMeanAirTemperatureRepository monthlyMeanAirTemperatureRepository;

    private final MonthlyPrecipitationRepository monthlyPrecipitationRepository;

    private final MonthlyRunOffRepository monthlyRunOffRepository;

    private Map<UUID, ReportStatus> statusMap = new HashMap<>();

    @Async
    public void generate(UUID uuid, Map<String, Measure> measures, String locale) throws IOException {
        statusMap.put(uuid, new ReportStatus());

        try {
            Map<String, Collection<AdministrationUnit>> administrationUnits = new HashMap<>();
            Map<String, Collection<ParcelView>> parcels = new HashMap<>();
            Map<String, Collection<ProtectedSiteAreaView>> protectedSiteAreas = new HashMap<>();
            Map<String, Collection<ProtectedSitePointView>> protectedSitePoints = new HashMap<>();
            Map<String, Collection<LandUseView>> landuses = new HashMap<>();
            Map<String, Collection<Double>> monthlyPrecipitation = new HashMap<>();
            Map<String, Collection<Double>> monthlyAirTemp = new HashMap<>();
            Map<String, Collection<Double>> monthlyRunOff = new HashMap<>();
            Map<String, Collection<PotentialForWaterRetention>> potentialForWaterRetentions = new HashMap<>();
            ZonalStatisticView zonalStatistic;

            for (Map.Entry<String, Measure> entry : measures.entrySet()) {
                administrationUnits.put(
                        entry.getKey(),
                        admUnitsRepository.findByGeomIntersection(entry.getValue().getGeometry())
                );

                parcels.put(
                        entry.getKey(),
                        parcelRepository.findByGeomIntersection(entry.getValue().getGeometry(), entry.getValue().getGeometry())
                );

                List<Double> precipitation = new ArrayList<>();

                for (int i = 1; i < 13; i++) {
                    zonalStatistic = monthlyPrecipitationRepository.getZonalStatistic(i, entry.getValue().getGeometry());

                    if (zonalStatistic.getMean() != null) {
                        precipitation.add(zonalStatistic.getMean());
                    } else {
                        precipitation.add(monthlyPrecipitationRepository.getValueForGeometryCentroid(i, entry.getValue().getGeometry()));
                    }
                }

                monthlyPrecipitation.put(
                        entry.getKey(),
                        precipitation
                );

                List<Double> airTemp = new ArrayList<>();

                for (int i = 1; i < 13; i++) {
                    zonalStatistic = monthlyMeanAirTemperatureRepository.getZonalStatistic(i, entry.getValue().getGeometry());

                    if (zonalStatistic.getMean() != null) {
                        airTemp.add(zonalStatistic.getMean());
                    } else {
                        airTemp.add(monthlyMeanAirTemperatureRepository.getValueForGeometryCentroid(i, entry.getValue().getGeometry()));
                    }
                }

                monthlyAirTemp.put(
                        entry.getKey(),
                        airTemp
                );

                List<Double> runOff = new ArrayList<>();

                for (int i = 1; i < 13; i++) {
                    zonalStatistic = monthlyRunOffRepository.getZonalStatistic(i, entry.getValue().getGeometry());

                    if (zonalStatistic.getMean() != null) {
                        runOff.add(zonalStatistic.getMean());
                    } else {
                        runOff.add(monthlyRunOffRepository.getValueForGeometryCentroid(i, entry.getValue().getGeometry()));
                    }
                }

                monthlyRunOff.put(
                        entry.getKey(),
                        runOff
                );

                protectedSiteAreas.put(
                        entry.getKey(),
                        protectedSitesAreasRepository.findByGeomIntersection(entry.getValue().getGeometry(), entry.getValue().getGeometry())
                );

                protectedSitePoints.put(
                        entry.getKey(),
                        protectedSitesPointsRepository.findByGeomIntersection(entry.getValue().getGeometry())
                );

                landuses.put(
                        entry.getKey(),
                        landuseRepository.findByGeomIntersection(entry.getValue().getGeometry(), entry.getValue().getGeometry())
                );

                potentialForWaterRetentions.put(
                        entry.getKey(),
                        potentialWaterRetentionRepository.findByGeomIntersection(entry.getValue().getGeometry())
                );
            }

            Report report = new Report();
            report.setData(
                    ReportData.builder()
                            .measures(measures)
                            .administrationUnits(administrationUnits)
                            .parcels(parcels)
                            .monthlyAirTemp(monthlyAirTemp)
                            .monthlyPrecipitation(monthlyPrecipitation)
                            .monthlyRunOff(monthlyRunOff)
                            .protectedSiteAreas(protectedSiteAreas)
                            .protectedSitePoints(protectedSitePoints)
                            .landuses(landuses)
                            .potentialForWaterRetentions(potentialForWaterRetentions)
                            .build()
            );

            report.draw(locale);
            report.save(getFileName(uuid));
            report.close();

            statusMap.get(uuid).setStatus(ReportStatus.Status.DONE);
        } catch (Exception e) {
            statusMap.get(uuid).setStatus(ReportStatus.Status.ERROR);
            log.error("Cannot generate report: " + uuid, e);
        }
    }

    public File getFileByUUID(UUID uuid) {
        ReportStatus reportStatus = getStatusByUUID(uuid);

        if (reportStatus.status != ReportStatus.Status.DONE) {
            throw new ReportNotReadyException();
        }

        return new File(getFileName(uuid));
    }

    public void removeFileByUUID(UUID uuid) {
        if (getFileByUUID(uuid).delete()) {
            statusMap.remove(uuid);
        }
    }

    public ReportStatus getStatusByUUID(UUID uuid) {
        ReportStatus reportStatus = statusMap.get(uuid);

        if (reportStatus == null) {
            throw new ReportNotFoundException();
        }

        return reportStatus;
    }

    private String getFileName(UUID uuid) {
        return REPORT_DIRECTORY + "/" + uuid.toString() + ".pdf";
    }

    @Getter
    public static class ReportStatus {
        @Setter
        private Status status;
        private OffsetDateTime creationDate;

        private ReportStatus() {
            this.status = Status.IN_PROGRESS;
            this.creationDate = OffsetDateTime.now(ZoneOffset.UTC);
        }

        public enum Status {
            NEW, IN_PROGRESS, DONE, ERROR
        }
    }

    public Set<UUID> removeOldReports() {
        OffsetDateTime now = OffsetDateTime.now(ZoneOffset.UTC);
        Set<UUID> oldReports = new HashSet<>();

        for (Map.Entry<UUID, ReportStatus> entry : statusMap.entrySet()) {
            if (Duration.between(now, entry.getValue().getCreationDate()).toSeconds() > REPORT_TIME_OUT) {
                oldReports.add(entry.getKey());
            }
        }

        return oldReports;
    }
}