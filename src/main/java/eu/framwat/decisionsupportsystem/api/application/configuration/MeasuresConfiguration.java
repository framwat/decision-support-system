package eu.framwat.decisionsupportsystem.api.application.configuration;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

@Configuration
@ConfigurationProperties(prefix = "measures")
@Data
public class MeasuresConfiguration {
    private Map<String, String> measures = new HashMap<>();
}
