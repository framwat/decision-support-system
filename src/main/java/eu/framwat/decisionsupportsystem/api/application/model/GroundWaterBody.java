package eu.framwat.decisionsupportsystem.api.application.model;

import org.locationtech.jts.geom.Geometry;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "wfdgroundwaterbodys")
public class GroundWaterBody {
    @Id
    @Column(name = "objectid")
    private Integer id;

    @Column(name = "localname")
    private String name;

    @Column(name = "id_localid")
    private String localId;

    @Column(name = "id_namespace")
    private String namespace;

    @Column(name = "id_versionid")
    private String versionId;

    @Column(name = "eugroundwaterbodycode")
    private String bodyCode;

    @Column(name = "cyear")
    private String year;

    @Column(name = "gwsignificantpressureother")
    private String significantPressure;

    @Column(name = "gwsignificantimpactother")
    private String signigicantImpact;

    @Column(name = "gwatriskquantitative")
    private String atRiskQuantitative;

    @Column(name = "gweoriskquantitative")
    private String eoRiskQuantitative;

    @Column(name = "gwquantitativestatusvalue")
    private String quantitativeStatusValue;

    @Column(name = "gwatriskchemical")
    private String atRiskChemical;

    @Column(name = "gweoriskchemical")
    private String eoRiskChemical;

    @Column(name = "gwchemicalstatusvalue")
    private String chemicalStatusValue;

    @Column(name = "shape_length")
    private Double shapeLength;

    @Column(name = "shape_area")
    private Double shapeArea;

    @Column(name = "shape")
    private Geometry geometry;
}
