package eu.framwat.decisionsupportsystem.api.application.scheduler;

import eu.framwat.decisionsupportsystem.api.application.service.ReportGenerator;
import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.File;
import java.util.UUID;

@Component
@RequiredArgsConstructor
public class ReportScheduler {

    private final ReportGenerator reportGenerator;

    @PostConstruct
    @Async
    public void removePreviousReports() {
        File directory = new File(ReportGenerator.REPORT_DIRECTORY);

        if(!directory.exists()) {
            directory.mkdirs();
        } else {
            for (final File fileEntry : directory.listFiles()) {
                fileEntry.delete();
            }
        }
    }

    @Async
    @Scheduled(fixedRateString = "${report.purge-time}")
    public void removeOldReports() {
        for (UUID uuid : reportGenerator.removeOldReports()) {
            reportGenerator.removeFileByUUID(uuid);
        }
    }
}
