package eu.framwat.decisionsupportsystem.api.application.repository;

import eu.framwat.decisionsupportsystem.api.application.model.ProtectedSitePoint;
import eu.framwat.decisionsupportsystem.api.application.model.ProtectedSitePointView;
import org.locationtech.jts.geom.Geometry;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface ProtectedSitesPointsRepository extends JpaRepository<ProtectedSitePoint, Integer> {
    String TABLE_NAME = "pssites";

    @Query(value = "SELECT a.localname AS localName, a.protclass AS protectionClass FROM " + TABLE_NAME + " AS a WHERE ST_Intersects(a.shape, ST_Transform(ST_SetSRID(?, 4326), 3857)) = true", nativeQuery = true)
    Collection<ProtectedSitePointView> findByGeomIntersection(Geometry geometry);
}
