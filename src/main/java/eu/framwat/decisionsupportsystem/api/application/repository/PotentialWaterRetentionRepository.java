package eu.framwat.decisionsupportsystem.api.application.repository;

import eu.framwat.decisionsupportsystem.api.application.model.PotentialForWaterRetention;
import org.locationtech.jts.geom.Geometry;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface PotentialWaterRetentionRepository extends JpaRepository<PotentialForWaterRetention, Integer> {
    @Query(value = "SELECT a FROM PotentialForWaterRetention AS a WHERE ST_Intersects(a.geometry, ST_Transform(ST_SetSRID(?1, 4326), 3857)) = true")
    Collection<PotentialForWaterRetention> findByGeomIntersection(Geometry geometry);
}
