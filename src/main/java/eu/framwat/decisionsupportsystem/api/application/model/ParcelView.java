package eu.framwat.decisionsupportsystem.api.application.model;

public interface ParcelView {
    String getName();

    Double getMetricParcelArea();

    Double getShapeParcelArea();

    Double getShapeIntersectionArea();
}
