package eu.framwat.decisionsupportsystem.api.application.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import org.locationtech.jts.geom.Geometry;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "cpparcels")
@EqualsAndHashCode
@Getter
public class Parcel {
    @Id
    @Column(name = "objectid")
    private Integer id;

    @Column(name = "id_localid")
    private String localId;

    @Column(name = "areavalue")
    private Double area;

    @Column(name = "areavalue_uom")
    private String areaUnit;

    @Column(name = "shape")
    private Geometry geometry;
}
