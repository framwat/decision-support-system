package eu.framwat.decisionsupportsystem.api.application.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import org.locationtech.jts.geom.Geometry;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "landuses")
@Getter
@EqualsAndHashCode
public class Landuse {
    @Id
    @Column(name = "objectid")
    private Integer id;

    @Column(name = "enname")
    private String name;

    @Column(name = "shape")
    private Geometry geometry;
}
