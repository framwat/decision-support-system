package eu.framwat.decisionsupportsystem.api.application.controller;

import eu.framwat.decisionsupportsystem.api.application.service.PointInformationQuery;
import lombok.RequiredArgsConstructor;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.PrecisionModel;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.NoSuchElementException;

@RestController
@RequiredArgsConstructor
@CrossOrigin(origins = "*", maxAge = 3600)
public class InformationController {

    private final PointInformationQuery informationFetcher;

    @GetMapping(path = "/information/{lat}/{lng}")
    public ResponseEntity<MapInformationResponse> getInformationByLatLng(
            @PathVariable("lat") @Valid float lat,
            @PathVariable("lng") @Valid float lng
    ) {

        GeometryFactory gf = new GeometryFactory(new PrecisionModel(), 4326);

        try {
            return ResponseEntity.ok(informationFetcher.getInformationByPoint(gf.createPoint(new Coordinate(lng, lat))));
        } catch (NoSuchElementException e) {
            return ResponseEntity.notFound().build();
        }
    }
}
