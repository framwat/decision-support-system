package eu.framwat.decisionsupportsystem.api.application.model;

public interface LandUseView {
    String getName();
    Double getShapeIntersectionArea();
}
