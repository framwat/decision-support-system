package eu.framwat.decisionsupportsystem.api.application.repository;

import eu.framwat.decisionsupportsystem.api.application.model.Landslope;
import eu.framwat.decisionsupportsystem.api.application.model.ZonalStatisticView;
import org.locationtech.jts.geom.Geometry;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface LandslopeRepository extends JpaRepository<Landslope, Integer> {
    String TABLE_NAME = "landslope";

    @Query(value = "SELECT (ST_SummaryStatsAgg(intersection.rast, 1, TRUE)).*" +
            "         FROM (" +
            "                  SELECT ST_Intersection(\"" + TABLE_NAME + "\".\"rast\"," +
            "                                         ST_AsRaster(" +
            "                                                 ST_Transform(" +
            "                                                         ST_SetSRID(?1, 4326)," +
            "                                                         3857)," +
            "                                                 \"" + TABLE_NAME + "\".\"rast\")" +
            "                             ) AS rast" +
            "                  FROM " + TABLE_NAME +
            "              ) as intersection", nativeQuery = true)
    ZonalStatisticView getZonalStatistic(Geometry g);
}
