package eu.framwat.decisionsupportsystem.api.application.repository;

import eu.framwat.decisionsupportsystem.api.application.model.Parcel;
import eu.framwat.decisionsupportsystem.api.application.model.ParcelView;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.Point;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Optional;

@Repository
public interface ParcelRepository extends JpaRepository<Parcel, Integer> {
    String TABLE_NAME = "cpparcels";

    @Query(value = "SELECT a.id_localid AS name, a.areavalue AS metricParcelArea, a.shape_area AS shapeParcelArea, ST_Area(ST_INTERSECTION(a.shape, ST_Transform(ST_SetSRID(?, 4326), 3857))) AS shapeIntersectionArea FROM " + TABLE_NAME + " AS a WHERE ST_Intersects(a.shape, ST_Transform(ST_SetSRID(?, 4326), 3857)) = true", nativeQuery = true)
    Collection<ParcelView> findByGeomIntersection(Geometry geom1, Geometry geom2);

    @Query(value = "SELECT a FROM Parcel AS a WHERE ST_Within(ST_Transform(ST_SetSRID(?1, 4326), 3857), a.geometry) = true")
    Optional<Parcel> findByPoint(Point point);
}
