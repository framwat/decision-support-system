package eu.framwat.decisionsupportsystem.api.application.controller;

import eu.framwat.decisionsupportsystem.api.application.configuration.MeasuresConfiguration;
import eu.framwat.decisionsupportsystem.api.application.model.Measure;
import lombok.RequiredArgsConstructor;
import org.locationtech.jts.geom.Geometry;
import org.springframework.stereotype.Component;
import org.wololo.geojson.Feature;
import org.wololo.jts2geojson.GeoJSONReader;

import java.util.*;

@Component
@RequiredArgsConstructor
public class MeasureMapper {

    private final GeoJSONReader reader = new GeoJSONReader();

    private final MeasuresConfiguration measuresConfiguration;

    public Map<String, Measure> map(Map<String, MeasureRequest> measureRequests) {
        Map<String, Measure> measures = new HashMap<>();

        for (Map.Entry<String, MeasureRequest> entry : measureRequests.entrySet()) {
            measures.put(
                    entry.getKey(),
                    Measure.builder()
                            .depth(entry.getValue().getDepth())
                            .height(entry.getValue().getHeight())
                            .length(entry.getValue().getLength())
                            .name(entry.getValue().getName())
                            .surface(entry.getValue().getSurface())
                            .type(getMeasureTypeName(entry.getValue().getType()))
                            .geometry(mapGeojsonFeature(entry.getValue().getGeojson()))
                            .build()
            );
        }

        return measures;
    }

    private String getMeasureTypeName(String type) {
        return measuresConfiguration.getMeasures().get(type);
    }

    private Geometry mapGeojsonFeature(Feature feature) {
        Geometry geometry = reader.read(feature.getGeometry());
        geometry.setSRID(4326);

        return geometry;
    }
}
