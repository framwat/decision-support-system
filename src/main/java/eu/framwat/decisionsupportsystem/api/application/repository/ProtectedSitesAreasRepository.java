package eu.framwat.decisionsupportsystem.api.application.repository;

import eu.framwat.decisionsupportsystem.api.application.model.ProtectedSiteArea;
import eu.framwat.decisionsupportsystem.api.application.model.ProtectedSiteAreaView;
import org.locationtech.jts.geom.Geometry;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface ProtectedSitesAreasRepository extends JpaRepository<ProtectedSiteArea, Integer> {
    String TABLE_NAME = "pssites";

    @Query(value = "SELECT a.localname AS localName, a.protclass AS protectionClass, ST_Area(ST_INTERSECTION(a.shape, ST_Transform(ST_SetSRID(?, 4326), 3857))) AS shapeIntersectionArea FROM " + TABLE_NAME + " AS a WHERE ST_Intersects(a.shape, ST_Transform(ST_SetSRID(?, 4326), 3857)) = true", nativeQuery = true)
    Collection<ProtectedSiteAreaView> findByGeomIntersection(Geometry geom1, Geometry geom2);
}
