package eu.framwat.decisionsupportsystem.api.application.repository;

import eu.framwat.decisionsupportsystem.api.application.model.GroundWaterBody;
import org.locationtech.jts.geom.Geometry;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface GroundWaterBodiesRepository extends JpaRepository<GroundWaterBody, Integer> {
    @Query(value = "SELECT a FROM GroundWaterBody AS a WHERE ST_Intersects(a.geometry, ST_Transform(ST_SetSRID(?1, 4326), 3857)) = true")
    Collection<GroundWaterBody> findByGeomIntersection(Geometry geometry);
}
