package eu.framwat.decisionsupportsystem.api.application.repository;

import eu.framwat.decisionsupportsystem.api.application.model.SurfaceWaterBodyReservoir;
import org.locationtech.jts.geom.Geometry;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface SurfaceWaterBodyReservoirsRepository extends JpaRepository<SurfaceWaterBodyReservoir, Integer> {
    @Query(value = "SELECT a FROM SurfaceWaterBodyReservoir AS a WHERE ST_Intersects(a.geometry, ST_Transform(ST_SetSRID(?1, 4326), 3857)) = true") //id = 68
    Collection<SurfaceWaterBodyReservoir> findByGeomIntersection(Geometry geometry);
}
