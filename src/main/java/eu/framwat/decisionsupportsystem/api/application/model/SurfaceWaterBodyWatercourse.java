package eu.framwat.decisionsupportsystem.api.application.model;

import org.locationtech.jts.geom.Geometry;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "wfdsurfacewaterbodyl")
public class SurfaceWaterBodyWatercourse {
    @Id
    @Column(name = "objectid")
    private Integer id;

    @Column(name = "localname")
    private String name;

    @Column(name = "id_localid")
    private String localId;

    @Column(name = "id_namespace")
    private String namespace;

    @Column(name = "id_versionid")
    private String versionId;

    @Column(name = "id_typespace")
    private String typespace;

    @Column(name = "eusurfacewaterbodycode")
    private String bodyCode;

    @Column(name = "cyear")
    private String year;

    @Column(name = "surfacewaterbodytypecode")
    private String typeCode;

    @Column(name = "swecologicalstatusorpotentialvalue")
    private String geologicalStatusPotentialValue;

    @Column(name = "swchemicalstatusvalue")
    private String chemicalStatusValue;

    @Column(name = "swchemicalmonitoringresults")
    private String chemicalMonitoringReults;

    @Column(name = "swsignificantpressuretype")
    private String significantPressureType;

    @Column(name = "swsignificantimpacttype")
    private String significantImpactType;

    @Column(name = "swprioritysubstancecode")
    private String prioritySubstanceCode;

    @Column(name = "shape_length")
    private Double shapeLength;

    @Column(name = "shape")
    private Geometry geometry;
}
