package eu.framwat.decisionsupportsystem.api.application.model;

public interface ProtectedSiteAreaView {
    String getProtectionClass();

    String getLocalName();

    Double getShapeIntersectionArea();
}
