package eu.framwat.decisionsupportsystem.api.application.model;

import lombok.Getter;
import org.locationtech.jts.geom.Geometry;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "potentialforwaterretention")
@Getter
public class PotentialForWaterRetention {
    @Id
    @Column(name = "objectid")
    private Integer id;

    private Double carableratio;
    private Double cbfi;
    private Double ccwb;
    private Double cdrainaged;
    private Double cecoarabuf20mratio;
    private Double cecoarearatio;
    private Double cecobadrhs;
    private Double cecocombined;
    private Double ceconumratio;
    private Double cfloodriskarearatio;
    private Double cflowmaxavgratio;
    private Double cflowminavgratio;
    private Double cflowminmaxratio;
    private Double cflowvarratio_m;
    private Double cforestratio;
    private Double cgraniteratio;
    private Double cgrr;
    private Double clakecatchratio;
    private Double clakeratio;
    private Double clandslope;
    private Double cmeanderratio;
    private Double cnonforestedratio;
    private Double corchvegratio;
    private Double cpre_var_a;
    private Double cpre_var_m;
    private Double cprecfreqlow75;
    private Double crainfallerodibility;
    private Double creclaimedratio;
    private Double criverslope;
    private Double csoilerodibility;
    private Double csri;
    private Double cswr;
    private Double ctwi;
    private Double curbanratio;
    private Double cwateryieldavgflow;
    private Double cwateryieldminflow;
    private Double cwetlandratio;
    private Double vdrought;
    private Double vflood;
    //private Double vsediment;
    //private Double vwaterquality;
    //private Double vgeneral;
    private Double cdrought;
    private Double cflood;
    private Double csediment;
    private Double cwaterquality;
    private Double cgeneral;

    @Column(name = "shape_length")
    private Double shapeLength;

    @Column(name = "shape_area")
    private Double shapeArea;

    @Column(name = "shape")
    private Geometry geometry;
}
