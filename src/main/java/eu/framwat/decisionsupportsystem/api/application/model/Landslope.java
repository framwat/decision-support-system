package eu.framwat.decisionsupportsystem.api.application.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "landslope")
public class Landslope {
    @Id
    @Column(name = "rid")
    private Integer id;
}