package eu.framwat.decisionsupportsystem.api.application.model;

import lombok.EqualsAndHashCode;
import org.locationtech.jts.geom.Geometry;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "auadmunits")
@EqualsAndHashCode
public class AdministrationUnit {
    @Id
    @Column(name = "objectid")
    private Integer id;

    @Column(name = "id_country")
    private String countryCode;

    @Column(name = "name1storder")
    private String level1Name;

    @Column(name = "name2ndorder")
    private String level2Name;

    @Column(name = "name3rdorder")
    private String level3Name;

    @Column(name = "name4thorder")
    private String level4Name;

    @Column(name = "name5thorder")
    private String level5Name;

    @Column(name = "name6thorder")
    private String level6Name;

    @Column(name = "shape")
    private Geometry geometry;

    public String getLevel1Name() {
        if (level1Name != null) {
            return level1Name;
        }

        return "";
    }

    public String getLevel2Name() {
        if (level2Name != null) {
            return level2Name;
        }

        return "";
    }

    public String getLevel3Name() {
        if (level3Name != null) {
            return level3Name;
        }

        return "";
    }

    public String getLevel4Name() {
        if (level4Name != null) {
            return level4Name;
        }

        return "";
    }

    public String getLevel5Name() {
        if (level1Name != null) {
            return level5Name;
        }

        return "";
    }

    public String getLevel6Name() {
        if (level6Name != null) {
            return level6Name;
        }

        return "";
    }
}
