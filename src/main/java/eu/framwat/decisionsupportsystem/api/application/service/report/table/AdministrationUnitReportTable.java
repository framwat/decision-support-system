package eu.framwat.decisionsupportsystem.api.application.service.report.table;

import be.quodlibet.boxable.page.PageProvider;
import eu.framwat.decisionsupportsystem.api.application.model.AdministrationUnit;
import eu.framwat.decisionsupportsystem.api.application.model.Measure;
import eu.framwat.decisionsupportsystem.api.application.service.report.ReportTable;
import lombok.EqualsAndHashCode;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.font.PDType0Font;

import java.io.IOException;
import java.util.*;

@EqualsAndHashCode(callSuper = true)
public class AdministrationUnitReportTable extends ReportTable {
    private final static List<String> headers = Arrays.asList("nswrm_name_column", "nswrm_type_column", "Państwo", "Województwo", "Gmina", "Miejscowość (obręb)");
    private final static List<Float> colWidths = Arrays.asList(16.66f, 16.66f, 16.66f, 16.66f, 16.66f, 16.66f);

    public AdministrationUnitReportTable(ResourceBundle resourceBundle, float yStart, PDDocument document, PDPage page, PDType0Font font, PageProvider pageProvider) throws IOException {
        super(resourceBundle, yStart, document, page, font, pageProvider);
    }

    public void setData(Map<String, Measure> measures, Map<String, Collection<AdministrationUnit>> administrationUnits) throws IOException {
        List<List<String>> data = new ArrayList<>();

        data.add(new ArrayList<>(headers));

        for (Map.Entry<String, Measure> measure : measures.entrySet()) {
            for (AdministrationUnit administrationUnit : administrationUnits.get(measure.getKey())) {
                data.add(new ArrayList<>(
                        Arrays.asList(measure.getValue().getName(), measure.getValue().getType(), administrationUnit.getLevel1Name(), administrationUnit.getLevel2Name(), administrationUnit.getLevel3Name(), administrationUnit.getLevel4Name())));
            }
        }

        this.prepareData(data, colWidths);
    }
}
