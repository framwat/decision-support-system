package eu.framwat.decisionsupportsystem.api.application.repository;

import eu.framwat.decisionsupportsystem.api.application.model.LandUseView;
import eu.framwat.decisionsupportsystem.api.application.model.Landuse;
import org.locationtech.jts.geom.Geometry;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface LanduseRepository extends JpaRepository<Landuse, Integer> {
    String TABLE_NAME = "landuses";

    @Query(value = "SELECT a.enname AS name, ST_Area(ST_INTERSECTION(a.shape, ST_Transform(ST_SetSRID(?, 4326), 3857))) AS shapeIntersectionArea FROM " + TABLE_NAME + " AS a WHERE ST_Intersects(a.shape, ST_Transform(ST_SetSRID(?, 4326), 3857)) = true", nativeQuery = true)
    Collection<LandUseView> findByGeomIntersection(Geometry geom1, Geometry geom2);
}
