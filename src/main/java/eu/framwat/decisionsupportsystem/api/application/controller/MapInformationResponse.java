package eu.framwat.decisionsupportsystem.api.application.controller;

import lombok.Builder;
import lombok.Getter;

import java.util.List;

@Builder
@Getter
public class MapInformationResponse {
    public List<String> administrationNamePath;
    public String parcelName;
    public Double parcelArea;
}
