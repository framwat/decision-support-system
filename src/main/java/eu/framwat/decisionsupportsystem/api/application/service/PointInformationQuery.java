package eu.framwat.decisionsupportsystem.api.application.service;

import eu.framwat.decisionsupportsystem.api.application.controller.MapInformationResponse;
import eu.framwat.decisionsupportsystem.api.application.model.AdministrationUnit;
import eu.framwat.decisionsupportsystem.api.application.model.Parcel;
import eu.framwat.decisionsupportsystem.api.application.repository.AdministrationUnitsRepository;
import eu.framwat.decisionsupportsystem.api.application.repository.ParcelRepository;
import lombok.RequiredArgsConstructor;
import org.locationtech.jts.geom.Point;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Component
@RequiredArgsConstructor
@CrossOrigin(origins = "*", maxAge = 3600)
public class PointInformationQuery {
    private final AdministrationUnitsRepository administrationUnitsRepository;
    private final ParcelRepository parcelRepository;

    public MapInformationResponse getInformationByPoint(Point point) {
        Optional<Parcel> optionalParcel = parcelRepository.findByPoint(point);

        if (optionalParcel.isEmpty()) {
            throw new NoSuchElementException();
        }

        Optional<AdministrationUnit> optionalAdministrationUnit = administrationUnitsRepository.findByPoint(point);

        if (optionalAdministrationUnit.isEmpty()) {
            throw new NoSuchElementException();
        }

        AdministrationUnit administrationUnit = optionalAdministrationUnit.get();

        List<String> namePath = new ArrayList();

        if (administrationUnit.getLevel1Name() != null && !administrationUnit.getLevel1Name().isEmpty()) {
            namePath.add(administrationUnit.getLevel1Name());
        }

        if (administrationUnit.getLevel2Name() != null && !administrationUnit.getLevel2Name().isEmpty()) {
            namePath.add(administrationUnit.getLevel2Name());
        }

        if (administrationUnit.getLevel3Name() != null && !administrationUnit.getLevel3Name().isEmpty()) {
            namePath.add(administrationUnit.getLevel3Name());
        }

        if (administrationUnit.getLevel4Name() != null && !administrationUnit.getLevel4Name().isEmpty()) {
            namePath.add(administrationUnit.getLevel4Name());
        }

        if (administrationUnit.getLevel5Name() != null && !administrationUnit.getLevel5Name().isEmpty()) {
            namePath.add(administrationUnit.getLevel5Name());
        }

        if (administrationUnit.getLevel6Name() != null && !administrationUnit.getLevel6Name().isEmpty()) {
            namePath.add(administrationUnit.getLevel6Name());
        }

        return MapInformationResponse.builder()
                .administrationNamePath(namePath)
                .parcelArea(optionalParcel.get().getArea())
                .parcelName(optionalParcel.get().getLocalId())
                .build();
    }
}
