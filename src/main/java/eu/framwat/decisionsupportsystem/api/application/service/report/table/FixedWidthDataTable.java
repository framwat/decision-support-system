package eu.framwat.decisionsupportsystem.api.application.service.report.table;

import be.quodlibet.boxable.BaseTable;
import be.quodlibet.boxable.Cell;
import be.quodlibet.boxable.HorizontalAlignment;
import be.quodlibet.boxable.Row;
import be.quodlibet.boxable.Table;
import be.quodlibet.boxable.VerticalAlignment;
import be.quodlibet.boxable.line.LineStyle;
import java.awt.Color;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.font.PDType0Font;
import org.apache.pdfbox.pdmodel.font.PDType1Font;

/**
 * Write CSV documents directly to PDF Tables
 *
 * @author Dries Horions {@code <dries@quodlibet.be>}
 */
public class FixedWidthDataTable {
    public static final Boolean HASHEADER = true;
    public static final Boolean NOHEADER = false;
    private Table table;
    private final Cell headerCellTemplate;
    private final Cell dataCellTemplateEven;
    private final Cell dataCellTemplateOdd;
    private final Cell firstColumnCellTemplate;
    private final Cell lastColumnCellTemplate;
    private final Cell defaultCellTemplate;

    /**
     * <p>
     * Create a CSVTable object to be able to add CSV document to a Table. A
     * page needs to be passed to the constructor so the Template Cells can be
     * created.
     * </p>
     *
     * @param table {@link Table}
     * @param page {@link PDPage}
     * @throws IOException  If there is an error releasing resources
     */
    public FixedWidthDataTable(Table table, PDPage page) throws IOException {
        this.table = table;
        // Create a dummy pdf document, page and table to create template cells
        PDDocument ddoc = new PDDocument();
        PDPage dpage = new PDPage();
        dpage.setMediaBox(page.getMediaBox());
        dpage.setRotation(page.getRotation());
        ddoc.addPage(dpage);
        BaseTable dummyTable = new BaseTable(10f, 10f, 10f, table.getWidth(), 10f, ddoc, dpage, false, false);
        Row dr = dummyTable.createRow(0f);
        headerCellTemplate = dr.createCell(10f, "A", HorizontalAlignment.CENTER, VerticalAlignment.MIDDLE);
        dataCellTemplateEven = dr.createCell(10f, "A", HorizontalAlignment.LEFT, VerticalAlignment.MIDDLE);
        dataCellTemplateOdd = dr.createCell(10f, "A", HorizontalAlignment.LEFT, VerticalAlignment.MIDDLE);
        firstColumnCellTemplate = dr.createCell(10f, "A", HorizontalAlignment.LEFT, VerticalAlignment.MIDDLE);
        lastColumnCellTemplate = dr.createCell(10f, "A", HorizontalAlignment.LEFT, VerticalAlignment.MIDDLE);
        defaultCellTemplate = dr.createCell(10f, "A", HorizontalAlignment.LEFT, VerticalAlignment.MIDDLE);
        setDefaultStyles();
        ddoc.close();
    }

    /**
     * <p>
     * Default cell styles for all cells. By default, only the header cell has a
     * different style than the rest of the table.
     * </p>
     */
    private void setDefaultStyles() {
        LineStyle thinline = new LineStyle(Color.BLACK, 0.75f);
        // Header style
        headerCellTemplate.setFillColor(new Color(137, 218, 245));
        headerCellTemplate.setTextColor(Color.BLACK);
        headerCellTemplate.setFont(PDType1Font.HELVETICA_BOLD);
        headerCellTemplate.setBorderStyle(thinline);

        // Normal cell style, all rows and columns are the same by default
        defaultCellTemplate.setFillColor(new Color(242, 242, 242));
        defaultCellTemplate.setTextColor(Color.BLACK);
        defaultCellTemplate.setFont(PDType1Font.HELVETICA);
        defaultCellTemplate.setBorderStyle(thinline);

        dataCellTemplateEven.copyCellStyle(defaultCellTemplate);
        dataCellTemplateOdd.copyCellStyle(defaultCellTemplate);
        firstColumnCellTemplate.copyCellStyle(defaultCellTemplate);
        lastColumnCellTemplate.copyCellStyle(defaultCellTemplate);
    }

    public void setFont(PDType0Font font) {
        headerCellTemplate.setFont(font);
        dataCellTemplateEven.setFont(font);
        dataCellTemplateOdd.setFont(font);
        firstColumnCellTemplate.setFont(font);
        lastColumnCellTemplate.setFont(font);
    }

    /**
     * <p>
     * Add a List of Lists to the Table
     * </p>
     *
     * @param data {@link Table}'s data
     * @param hasHeader boolean if {@link Table} has header
     * @throws IOException parsing error
     */
    public void drawDataTable(List<List<String>> data, Boolean hasHeader, List<Float> colWidths) throws IOException {
        Boolean isHeader = hasHeader;
        Boolean odd = true;

        Integer colNum = data.get(0).size();
        Boolean hasFirstColumnTemplate = firstColumnCellTemplate.hasSameStyle(defaultCellTemplate);
        Boolean hasLastColumnTemplate = lastColumnCellTemplate.hasSameStyle(defaultCellTemplate);

        Iterator<List<String>> iterator = data.iterator();

        if (isHeader && iterator.hasNext()) {
            List<String> header = iterator.next();

            // Add Header Row
            Row h = table.createRow(headerCellTemplate.getCellHeight());
            for (int i = 0; i < colNum; i++) {
                String cellValue = header.get(i);
                Cell c = h.createCell(colWidths.get(i), cellValue, headerCellTemplate.getAlign(),
                        headerCellTemplate.getValign());
                // Apply style of header cell to this cell
                c.copyCellStyle(headerCellTemplate);
                c.setText(cellValue);
            }
            table.addHeaderRow(h);
        }

        while (iterator.hasNext()) {
            List<String> row = iterator.next();

            Row r = table.createRow(dataCellTemplateEven.getCellHeight());

            for (int i = 0; i < colNum; i++) {
                Cell template = dataCellTemplateEven;
                if (odd) {
                    template = dataCellTemplateOdd;
                }
                if (i == 0 & hasFirstColumnTemplate) {
                    template = firstColumnCellTemplate;
                }
                if (i == colNum & hasLastColumnTemplate) {
                    template = lastColumnCellTemplate;
                }
                String cellValue = row.get(i);

                Cell c = r.createCell(colWidths.get(i), cellValue, template.getAlign(), template.getValign());
                c.copyCellStyle(template);
                c.setText(cellValue);
            }
            odd = !odd;
        }
    }
}
