package eu.framwat.decisionsupportsystem.api.application.service.report.table;

import be.quodlibet.boxable.page.PageProvider;
import eu.framwat.decisionsupportsystem.api.application.model.LandUseView;
import eu.framwat.decisionsupportsystem.api.application.model.Measure;
import eu.framwat.decisionsupportsystem.api.application.service.report.ReportTable;
import lombok.EqualsAndHashCode;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.font.PDType0Font;

import java.io.IOException;
import java.util.*;

@EqualsAndHashCode(callSuper = true)
public class LandUseReportTable extends ReportTable {
    private final static List<String> headers = Arrays.asList("nswrm_name_column", "nswrm_type_column", "characteristic_column", "area_column");
    private final static List<Float> colWidths = Arrays.asList(20f, 20f, 40f, 20f);

    public LandUseReportTable(ResourceBundle resourceBundle, float yStart, PDDocument document, PDPage page, PDType0Font font, PageProvider pageProvider) throws IOException {
        super(resourceBundle, yStart, document, page, font, pageProvider);
    }

    public void setData(
            Map<String, Measure> measures,
            Map<String, Collection<LandUseView>> landuses
    ) throws IOException {
        List<List<String>> data = new ArrayList<>();

        data.add(new ArrayList<>(headers));

        for (Map.Entry<String, Measure> measure : measures.entrySet()) {
            for (LandUseView landuse : landuses.get(measure.getKey())) {
                data.add(new ArrayList<>(
                        Arrays.asList(
                                measure.getValue().getName(),
                                measure.getValue().getType(),
                                landuse.getName(),
                                String.format("%.1f", landuse.getShapeIntersectionArea())
                        )
                ));
            }
        }

        this.prepareData(data, colWidths);
    }

}
