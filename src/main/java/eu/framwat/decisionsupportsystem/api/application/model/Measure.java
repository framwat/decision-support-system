package eu.framwat.decisionsupportsystem.api.application.model;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import org.locationtech.jts.geom.Geometry;

@Builder
@Getter
@EqualsAndHashCode
public class Measure {
    private Geometry geometry;
    private String depth;
    private String height;
    private String length;
    private String name;
    private String surface;
    private String type;
}
