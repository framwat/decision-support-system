package eu.framwat.decisionsupportsystem.api.application.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "monthprecipitation")
public class MonthlyPrecipitation {
    @Id
    @Column(name = "rid")
    private Integer id;
}