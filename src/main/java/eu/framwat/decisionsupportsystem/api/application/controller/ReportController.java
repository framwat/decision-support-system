package eu.framwat.decisionsupportsystem.api.application.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import eu.framwat.decisionsupportsystem.api.application.service.ReportGenerator;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Map;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
@CrossOrigin(origins = "*", maxAge = 3600)
public class ReportController {

    private final ReportGenerator reportGenerator;

    private final MeasureMapper measureMapper;

    @PostMapping(path = "/report")
    public ResponseEntity<ReportStatusResponse> generate(
            @RequestBody @Valid Map<String, @Valid MeasureRequest> requestMeasures,
            @RequestParam(name = "locale") String locale
    ) throws IOException {

        UUID uuid = UUID.randomUUID();

        reportGenerator.generate(uuid, measureMapper.map(requestMeasures), locale);

        ReportStatusResponse response = ReportStatusResponse.builder()
                .uuid(uuid)
                .status(ReportGenerator.ReportStatus.Status.NEW.name())
                .build();

        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @GetMapping(path = "/report/status/{uuid}")
    public ResponseEntity<ReportStatusResponse> getStatus(@PathVariable("uuid") String uuidString) {

        UUID uuid = UUID.fromString(uuidString);
        ReportGenerator.ReportStatus reportStatus = reportGenerator.getStatusByUUID(uuid);

        ReportStatusResponse response = ReportStatusResponse.builder()
                .uuid(uuid)
                .status(reportStatus.getStatus().name())
                .build();

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping(
            path = "/report/download/{uuid}",
            produces = MediaType.APPLICATION_PDF_VALUE
    )
    public @ResponseBody byte[] downloadReportFile(@PathVariable("uuid") String uuid, HttpServletResponse response) throws IOException {
        response.addHeader("Content-Disposition", "attachment; filename=\"report.pdf\"");

        return (new FileInputStream(
                reportGenerator.getFileByUUID(
                        UUID.fromString(uuid)
                ))).readAllBytes();
    }
}
