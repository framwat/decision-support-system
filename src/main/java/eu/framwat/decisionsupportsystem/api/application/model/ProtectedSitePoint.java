package eu.framwat.decisionsupportsystem.api.application.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import org.locationtech.jts.geom.Geometry;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "pssitep")
@Getter
@EqualsAndHashCode
public class ProtectedSitePoint {
    @Id
    @Column(name = "objectid")
    private Integer id;

    @Column(name = "localname")
    private String localName;

    @Column(name = "protclass")
    private String protectionClass;

    @Column(name = "shape")
    private Geometry geometry;
}
