package eu.framwat.decisionsupportsystem.api.application.service.report.table;

import be.quodlibet.boxable.page.PageProvider;
import eu.framwat.decisionsupportsystem.api.application.model.*;
import eu.framwat.decisionsupportsystem.api.application.service.report.ReportTable;
import lombok.EqualsAndHashCode;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.font.PDType0Font;

import java.io.IOException;
import java.util.*;

@EqualsAndHashCode(callSuper = true)
public class RiskOfInvestmentsTable extends ReportTable {
    private final static List<String> headers = Arrays.asList("nswrm_name_column", "nswrm_type_column", "topic_column", "name_column", "area_column");
    private final static List<Float> colWidths = Arrays.asList(15f, 15f, 31f, 31f, 8f);

    public RiskOfInvestmentsTable(ResourceBundle resourceBundle, float yStart, PDDocument document, PDPage page, PDType0Font font, PageProvider pageProvider) throws IOException {
        super(resourceBundle, yStart, document, page, font, pageProvider);
    }

    public void setData(
            Map<String, Measure> measures,
            Map<String, Collection<ProtectedSiteAreaView>> protectedSiteAreas,
            Map<String, Collection<ProtectedSitePointView>> protectedSitePoints
    ) throws IOException {
        List<List<String>> data = new ArrayList<>();

        data.add(new ArrayList<>(headers));

        for (Map.Entry<String, Measure> measure : measures.entrySet()) {
            for (ProtectedSiteAreaView protectedSiteArea : protectedSiteAreas.get(measure.getKey())) {
                data.add(new ArrayList<>(
                        Arrays.asList(
                                measure.getValue().getName(),
                                measure.getValue().getType(),
                                protectedSiteArea.getProtectionClass(),
                                protectedSiteArea.getLocalName(),
                                String.format("%.1f", protectedSiteArea.getShapeIntersectionArea())
                        )
                ));
            }
        }

        for (Map.Entry<String, Measure> measure : measures.entrySet()) {
            for (ProtectedSitePointView protectedSitePoint : protectedSitePoints.get(measure.getKey())) {
                data.add(new ArrayList<>(
                        Arrays.asList(
                                measure.getValue().getName(),
                                measure.getValue().getType(),
                                protectedSitePoint.getProtectionClass(),
                                protectedSitePoint.getLocalName(),
                                "-"
                        )));
            }
        }

        this.prepareData(data, colWidths);
    }
}