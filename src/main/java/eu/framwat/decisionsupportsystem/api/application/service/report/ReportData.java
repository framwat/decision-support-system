package eu.framwat.decisionsupportsystem.api.application.service.report;

import eu.framwat.decisionsupportsystem.api.application.model.*;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.util.Collection;
import java.util.Map;

@Builder
@Getter
@EqualsAndHashCode
public class ReportData {
    private Map<String, Collection<AdministrationUnit>> administrationUnits;
    private Map<String, Collection<ParcelView>> parcels;
    private Map<String, Measure> measures;
    private Map<String, Collection<Double>> monthlyPrecipitation;
    private Map<String, Collection<Double>> monthlyAirTemp;
    private Map<String, Collection<Double>> monthlyRunOff;
    private Map<String, Collection<ProtectedSiteAreaView>> protectedSiteAreas;
    private Map<String, Collection<ProtectedSitePointView>> protectedSitePoints;
    private Map<String, Collection<LandUseView>> landuses;
    private Map<String, Collection<PotentialForWaterRetention>> potentialForWaterRetentions;
}
