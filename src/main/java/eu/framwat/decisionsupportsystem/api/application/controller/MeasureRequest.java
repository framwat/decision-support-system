package eu.framwat.decisionsupportsystem.api.application.controller;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.wololo.geojson.Feature;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@Valid
public class MeasureRequest {

    @NotNull
    private Feature geojson;
    private String depth;
    private String height;
    private String length;

    @NotBlank
    private String name;
    private String surface;

    @NotBlank
    private String type;
}
