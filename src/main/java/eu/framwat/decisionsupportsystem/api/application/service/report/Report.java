package eu.framwat.decisionsupportsystem.api.application.service.report;

import be.quodlibet.boxable.page.DefaultPageProvider;

import be.quodlibet.boxable.page.PageProvider;
import be.quodlibet.boxable.utils.PDStreamUtils;
import be.quodlibet.boxable.utils.PageContentStreamOptimized;
import eu.framwat.decisionsupportsystem.api.application.service.report.table.*;
import lombok.EqualsAndHashCode;
import lombok.extern.slf4j.Slf4j;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.font.PDType0Font;
import org.springframework.core.io.ClassPathResource;

import java.awt.*;
import java.io.IOException;
import java.util.ResourceBundle;

@Slf4j
@EqualsAndHashCode(callSuper = true)
public class Report extends PDDocument {
    public PDType0Font font;

    private final PDRectangle size = new PDRectangle(PDRectangle.A4.getHeight(), PDRectangle.A4.getWidth());

    private ReportData reportData;

    private DefaultPageProvider pageProvider;

    public Report() throws IOException {
        super();

        font = PDType0Font.load(this, new ClassPathResource("fonts/OpenSans-Regular.ttf").getInputStream());

        pageProvider = new DefaultPageProvider(this, size);
    }

    public void setData(ReportData reportData) {
        this.reportData = reportData;
    }

    public void draw(String locale) throws IOException {
        ResourceBundle i18nBundle = ResourceBundle.getBundle("i18n" + "_" + locale);

        PDPage page = pageProvider.createPage();

        float yStart = page.getMediaBox().getHeight() - (2 * 10);

        yStart = drawHeader(getCurrentPage(), i18nBundle.getString("report_title"), yStart);

        yStart = drawTableHeader(getCurrentPage(), i18nBundle.getString("introduction_header"), yStart);

        yStart = drawParagraphText(getCurrentPage(), i18nBundle.getString("introduction"), yStart);

        yStart = drawTableHeader(getCurrentPage(), i18nBundle.getString("administration_units_header"), yStart);

        final AdministrationUnitReportTable administrationUnitReportTable = new AdministrationUnitReportTable(i18nBundle, yStart, this, getCurrentPage(), font, pageProvider);
        administrationUnitReportTable.setData(reportData.getMeasures(), reportData.getAdministrationUnits());
        yStart = administrationUnitReportTable.draw();
        yStart = ifTooLowStartNewPage(yStart - 20, pageProvider);

        yStart = drawTableHeader(getCurrentPage(), i18nBundle.getString("parcels_header"), yStart);

        final ParcelReportTable parcelReportTable = new ParcelReportTable(i18nBundle, yStart, this, getCurrentPage(), font, pageProvider);
        parcelReportTable.setData(reportData.getMeasures(), reportData.getParcels());
        yStart = parcelReportTable.draw();
        yStart = ifTooLowStartNewPage(yStart - 20, pageProvider);

        yStart = drawTableHeader(getCurrentPage(), i18nBundle.getString("precipitation_header"), yStart);

        final MonthlyValuesReportTable precipitationTable = new MonthlyValuesReportTable(i18nBundle, yStart, this, getCurrentPage(), font, pageProvider);
        precipitationTable.setData(reportData.getMeasures(), reportData.getMonthlyPrecipitation(), true);
        yStart = precipitationTable.draw();
        yStart = ifTooLowStartNewPage(yStart - 20, pageProvider);

        yStart = drawTableHeader(getCurrentPage(), i18nBundle.getString("air_temperature_header"), yStart);

        final MonthlyValuesReportTable airTempTable = new MonthlyValuesReportTable(i18nBundle, yStart, this, getCurrentPage(), font, pageProvider);
        airTempTable.setData(reportData.getMeasures(), reportData.getMonthlyAirTemp(), false);
        yStart = airTempTable.draw();
        yStart = ifTooLowStartNewPage(yStart - 20, pageProvider);

        yStart = drawTableHeader(getCurrentPage(), i18nBundle.getString("run_off_header"), yStart);

        final MonthlyValuesReportTable runOffTable = new MonthlyValuesReportTable(i18nBundle, yStart, this, getCurrentPage(), font, pageProvider);
        runOffTable.setData(reportData.getMeasures(), reportData.getMonthlyRunOff(), true);
        yStart = runOffTable.draw();
        yStart = ifTooLowStartNewPage(yStart - 20, pageProvider);

        yStart = drawTableHeader(getCurrentPage(), i18nBundle.getString("risk_of_investment_header"), yStart);

        final RiskOfInvestmentsTable riskOfInvestments = new RiskOfInvestmentsTable(i18nBundle, yStart, this, getCurrentPage(), font, pageProvider);
        riskOfInvestments.setData(reportData.getMeasures(), reportData.getProtectedSiteAreas(), reportData.getProtectedSitePoints());
        yStart = riskOfInvestments.draw();
        yStart = ifTooLowStartNewPage(yStart - 20, pageProvider);

        yStart = drawParagraphText(getCurrentPage(), i18nBundle.getString("class_of_valorisation"), yStart);

        yStart = drawTableHeader(getCurrentPage(), i18nBundle.getString("class_of_valorisation_header"), yStart);

        final PotentialForWaterRetentionReportTable potentialForWaterRetention = new PotentialForWaterRetentionReportTable(i18nBundle, yStart, this, getCurrentPage(), font, pageProvider);
        potentialForWaterRetention.setData(reportData.getMeasures(), reportData.getPotentialForWaterRetentions());
        yStart = potentialForWaterRetention.draw();
        yStart = ifTooLowStartNewPage(yStart - 20, pageProvider);

        final PotentialForWaterRetention2ReportTable potentialForWaterRetention2 = new PotentialForWaterRetention2ReportTable(i18nBundle, yStart, this, getCurrentPage(), font, pageProvider);
        potentialForWaterRetention2.setData(reportData.getMeasures(), reportData.getPotentialForWaterRetentions());
        yStart = potentialForWaterRetention2.draw();
        yStart = ifTooLowStartNewPage(yStart - 20, pageProvider);

        yStart = drawTableHeader(getCurrentPage(), i18nBundle.getString("land_cover_occupation_header"), yStart);

        final LandUseReportTable landUseTable = new LandUseReportTable(i18nBundle, yStart, this, getCurrentPage(), font, pageProvider);
        landUseTable.setData(reportData.getMeasures(), reportData.getLanduses());
        yStart = landUseTable.draw();

    }

    private PDPage getCurrentPage() {
        return getPage(this.getNumberOfPages() - 1);
    }

    private float ifTooLowStartNewPage(float yStart, PageProvider pageProvider) {
        if (yStart < 70) {
            pageProvider.createPage();

            return size.getHeight() - (2 * 10);
        }

        return yStart;
    }

    private float drawTableHeader(PDPage page, String header, float yStart) throws IOException {

        PDPageContentStream contentStream = new PDPageContentStream(this, page, PDPageContentStream.AppendMode.APPEND, true, true);
        PageContentStreamOptimized optimizedContentStream = new PageContentStreamOptimized(contentStream);
        PDStreamUtils.write(optimizedContentStream, header, font, 12, 20, yStart, Color.BLACK);

        contentStream.close();

        return yStart - 20;
    }

    private float drawHeader(PDPage page, String header, float yStart) throws IOException {

        PDPageContentStream contentStream = new PDPageContentStream(this, page, PDPageContentStream.AppendMode.APPEND, true, true);
        PageContentStreamOptimized optimizedContentStream = new PageContentStreamOptimized(contentStream);
        PDStreamUtils.write(optimizedContentStream, header, font, 18, 20, yStart, Color.BLACK);
        contentStream.close();

        return yStart - 30;
    }

    private float drawParagraphText(PDPage page, String text, float yStart) throws IOException {

        PDPageContentStream contentStream = new PDPageContentStream(this, page, PDPageContentStream.AppendMode.APPEND, true, true);
        PageContentStreamOptimized optimizedContentStream = new PageContentStreamOptimized(contentStream);

        for (String entry : text.split("\n")) {
            PDStreamUtils.write(optimizedContentStream, entry, font, 10, 20, yStart, Color.BLACK);

            yStart -= 13;
        }

        contentStream.close();

        return yStart - 20;
    }
}
