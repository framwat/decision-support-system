package eu.framwat.decisionsupportsystem.api.application.service.report.table;

import be.quodlibet.boxable.page.PageProvider;
import eu.framwat.decisionsupportsystem.api.application.model.Measure;
import eu.framwat.decisionsupportsystem.api.application.model.PotentialForWaterRetention;
import eu.framwat.decisionsupportsystem.api.application.service.report.ReportTable;
import lombok.EqualsAndHashCode;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.font.PDType0Font;

import java.io.IOException;
import java.util.*;

@EqualsAndHashCode(callSuper = true)
public class PotentialForWaterRetentionReportTable extends ReportTable {
    private final static List<String> headers = Arrays.asList(
            "nswrm_name_column",
            "nswrm_type_column",
            "Goal: Flood",
            "Goal: Drought",
            "Goal: General",
            "Goal: Sediment",
            "Goal: Quality",
            "FloodRiskAreaRatio",
            "LakeRatio",
            "LakeCatchRatio",
            "LandSlope",
            "FlowMinAvgRatio",
            "FlowMaxAvgRatio",
            "FlowMinMaxRatio",
            "FlowVarRatio_m",
            "WaterYieldMinFlow",
            "WaterYieldAvgFlow",
            "ForestRatio",
            "GRR" //17
    );
    private final static List<Float> colWidths = Arrays.asList(
            20f, 29f,
            3f, 3f, 3f, 3f, 3f, 3f, 3f, 3f,
            3f, 3f, 3f, 3f, 3f, 3f, 3f, 3f,
            3f
    );

    public PotentialForWaterRetentionReportTable(ResourceBundle resourceBundle, float yStart, PDDocument document, PDPage page, PDType0Font font, PageProvider pageProvider) throws IOException {
        super(resourceBundle, yStart, document, page, font, pageProvider);
    }

    public void setData(Map<String, Measure> measures, Map<String, Collection<PotentialForWaterRetention>> potentialCollection) throws IOException {
        List<List<String>> data = new ArrayList<>();

        data.add(new ArrayList<>(headers));

        for (Map.Entry<String, Measure> measure : measures.entrySet()) {
            for (PotentialForWaterRetention potential : potentialCollection.get(measure.getKey())) {
                data.add(new ArrayList<>(
                        Arrays.asList(
                                measure.getValue().getName(),
                                measure.getValue().getType(),
                                formatField(potential.getCflood()),
                                formatField(potential.getCdrought()),
                                formatField(potential.getCgeneral()),
                                formatField(potential.getCsediment()),
                                formatField(potential.getCwaterquality()),
                                formatField(potential.getCfloodriskarearatio()),
                                formatField(potential.getClakeratio()),
                                formatField(potential.getClakecatchratio()),
                                formatField(potential.getClandslope()),
                                formatField(potential.getCflowminavgratio()),
                                formatField(potential.getCflowmaxavgratio()),
                                formatField(potential.getCflowminmaxratio()),
                                formatField(potential.getCflowvarratio_m()),
                                formatField(potential.getCwateryieldminflow()),
                                formatField(potential.getCwateryieldavgflow()),
                                formatField(potential.getCforestratio()),
                                formatField(potential.getCgrr())
                        )));
            }
        }

        this.prepareData(data, colWidths);
    }

    private String formatField(Double value) {
        return value == null ? "" : String.format("%.0f", value);
    }
}
