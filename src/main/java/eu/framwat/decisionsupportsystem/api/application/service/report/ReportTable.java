package eu.framwat.decisionsupportsystem.api.application.service.report;

import be.quodlibet.boxable.BaseTable;
import be.quodlibet.boxable.page.PageProvider;
import eu.framwat.decisionsupportsystem.api.application.service.report.table.FixedWidthDataTable;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.font.PDType0Font;

import java.io.IOException;
import java.util.List;
import java.util.ResourceBundle;

public abstract class ReportTable extends BaseTable {
    private final static float margin = 10;
    private final static float bottomMargin = 70;
    private PDPage page;
    private PDType0Font font;
    private ResourceBundle resourceBundle;

    public ReportTable(ResourceBundle resourceBundle,
                       float yStart,
                       PDDocument document,
                       PDPage page,
                       PDType0Font font,
                       PageProvider pageProvider) throws IOException {
        super(
                yStart,
                page.getMediaBox().getHeight() - (2 * margin),
                0,
                bottomMargin,
                page.getMediaBox().getWidth() - (2 * margin),
                margin,
                document,
                page,
                true,
                true,
                pageProvider
        );

        this.resourceBundle = resourceBundle;
        this.page = page;
        this.font = font;
    }

    protected void prepareData(List<List<String>> data, List<Float> colWidths) throws IOException {
        List<String> headers = data.get(0);

        for (int i=0; i<headers.size(); i++) {
            headers.set(i, resourceBundle.containsKey(headers.get(i)) ? resourceBundle.getString(headers.get(i)) : headers.get(i));
        }

        data.set(0, headers);

        FixedWidthDataTable t = new FixedWidthDataTable(this, page);
        t.setFont(font);

        t.drawDataTable(data, FixedWidthDataTable.HASHEADER, colWidths);
    }
}
