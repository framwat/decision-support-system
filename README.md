# Decision Support System #

### Dev environment ###

All packages should be installed using `yarn` https://yarnpkg.com/lang/en/docs/install/

Install web project dependencies:

    yarn install

Run Angular project we need Angular CLI:

    yarn global add @angular/cli
    
Running project:
Angular `yarn start` (alias: `ng serve`)

Running backend application: `./gradlew bootRun`

### Building ###

Angular: `yarn buildProd` (alias: `ng build --prod`)
Spring boot: `/gradlew build` it creates war file in `build/libs/` which contains both Angular and java files.

### Running ###

    docker run -ePOSTGIS_HOST=levis-framwat.sggw.pl -d -p 8080:8080 registry.gitlab.com/framwat/decision-support-system:v0.1.40