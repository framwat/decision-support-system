FROM openjdk:11-jre-slim
VOLUME /tmp
COPY build/libs/dss-api-0.0.1-SNAPSHOT.war app.war
ENTRYPOINT exec java $JAVA_OPTS -jar app.war $JAVA_ARGS
EXPOSE 8080
